      (function($) {

          $(document).ready(function() {
              window.pluginfs = false;
              $('#main-slideshow-article-plug-in').flexslider({
                  selector: ".xmslides > li ",
                  animation: "slide",
                  slideshow: false,
                  animationLoop: false,
                  controlNav: false,
                  directionNav: false,
                  useCSS: true,
                  after: function(slider) {
                      window.pluginfs = true;
                      setTimeout(function() {
                          $("#s2").css({
                              'height': $("#s1").height(),
                              'z-inde': 0
                          });
                            $("#views-slideshow-bxslider-1 ").css({
                              'height': $("#s1").height() -25,
                              'z-inde': 0
                          });
                             $("#main-slideshow-article-plug-in .skin-default").css({
                          'height': $("#s1").height()-25,
                      });
                          $("#views-slideshow-bxslider-1 .bx-wrapper").css({
                              'height': $("#s1").height() -25,
                              'z-index': 0
                          });


                           $("#views-slideshow-bxslider-1 .bx-wrapper").focus();
                      $("#views-slideshow-bxslider-1 .bx-wrapper .bx-prev").focus();
                      $("#views-slideshow-bxslider-1 .bx-wrapper .bx-next").focus();

                      }, 100);
                  },
                  start: function(slider) {
                      window.pluginfs = true;
                      setTimeout(function() {
                          $("#s2").css({
                              'height': $("#s1").height()
                          });

                          $("#views-slideshow-bxslider-1 .bx-wrapper").css({
                              'height': $("#s1").height() -25
                          });
                           $("#main-slideshow-article-plug-in  .skin-default").css({
                          'height': $("#s1").height()-25,
                      });
                      }, 250);
                  }
              });

              $('.scrollTo').on('click', function(event) {
                  target = $(this).attr('data-target');
                  console.log(target);
                  var scrollYPos = $(target).offset().top - 60;
                  event.preventDefault();
                  TweenLite.to(window, 2, {
                      scrollTo: {
                          y: scrollYPos,
                          x: 0
                      },
                      ease: Power4.easeOut
                  })
              })

              $(window).resize(function() {
                  $("#s2").css({
                      'height': $("#s1").height()
                  });
                  $("#views-slideshow-bxslider-1 .bx-wrapper").css({
                      'height': $("#s1").height()-25,
                  });
                    $("#main-slideshow-article-plug-in .skin-default").css({
                          'height': $("#s1").height()-25,
                      });
                  setTimeout(function() {
                      $("#s2").css({
                          'height': $("#s1").height()
                      });
                       $("#views-slideshow-bxslider-1 ").css({
                          'height': $("#s1").height()-25,
                      });
                        $("#main-slideshow-article-plug-in  .skin-default").css({
                          'height': $("#s1").height()-25,
                      });
                      $("#views-slideshow-bxslider-1 .bx-wrapper").css({
                          'height': $("#s1").height()-25,
                      });


                      $("#views-slideshow-bxslider-1 .bx-wrapper").focus();
                      $("#views-slideshow-bxslider-1 .bx-wrapper .bx-prev").focus();
                      $("#views-slideshow-bxslider-1 .bx-wrapper .bx-next").focus();
                  }, 300);
              })

          });

          $('.mprev').on('click', function() {

              $('#main-slideshow-article-plug-in').flexslider(0);

          })

          window.productprevslide = function() {
              if (window.pluginfs == true) {
                  $('#main-slideshow-article-plug-in').flexslider(0);
                  // window.pauseVideos();
                  return false;
              };
          };

          $('.mnext').on('click', function() {
              $('#main-slideshow-article-plug-in').flexslider(1);

              setTimeout(function() {
                  $("#s2").css({
                      'height': $("#s1").height()
                  });
                   $("#main-slideshow-article-plug-in  .skin-default").css({
                          'height': $("#s1").height()-25,
                      });

                  $("#views-slideshow-bxslider-1 .bx-wrapper").css({
                      'height': $("#s1").height() - 25
                  });

                  $("#views-slideshow-bxslider-1 .bx-wrapper").focus();
                  $("#views-slideshow-bxslider-1 .bx-wrapper .bx-prev").focus();
                  $("#views-slideshow-bxslider-1 .bx-wrapper .bx-next").focus();
              }, 100);


              return false;
          })



          window.pauseVideos = function pauseVideos(current) {
              // if state == 'hide', hide. Else: show video
              sliden = 0;
              $('#main-slideshow-article-plug-in .video-left iframe').each(function() {
                  if (!(current == sliden)) {
                      var video = $(this).attr("src");
                      $(this).attr("src", "");
                      $(this).attr("src", video);
                  };
                  sliden = sliden + 1;
              })
          }
      })(jQuery);
