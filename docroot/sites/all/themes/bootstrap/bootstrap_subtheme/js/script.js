jQuery(document).ready(function() {
  $ = jQuery;

  if ($('body').hasClass('front')) {
    front = true;
  } else {
    front = false;
  }
  var height = $(window).height();
  if (height > 540) {
    height = 540;
  }
  theight = height + 60;



$(".navicon-wrapper").click(function(){
  $("a.navicon-button").toggleClass("open");
  $("#megamenu").toggleClass("mobile");
  $("body").toggleClass("mobile-menu");
});


function winResized(id) {
    var $image=$(id);
    if ($image.size()) {
        var ww=$(window).width();
        var wh=$(window).height();
        var iw=$image.width();
        var ih=$image.height();
        var f=Math.max(ww/iw,wh/ih);
        var iwr=Math.round(iw*f);
        var ihr=Math.round(ih*f);
        $image.css({
            'position':'fixed',
            'width': iwr,
            'height': ihr,
            'left': (Math.round(ww-iwr)/2)+'px',
            'top': (Math.round(wh-ihr)/2)+'px'
        });
    }
}

/*
jQuery(document).ready(function() {
jQuery('.section-arrow').click(function(){
        jQuery('#landing').slideUp(2000);

        });
});
setTimeout(function() {
     jQuery('#landing').slideUp(1700);
 }, 3700);
};
*/

});
