<?php

/**
 * @file
 * template.php
 */
const  BUNDLE_PRICE_URL = 'https://www2.sonnox.com/pub/plug-shop/callback/productPrices.php';


function bootstrap_subtheme_url_outbound_alter(&$path, &$options, $original_path)
{
    $alias = drupal_get_path_alias($original_path);
    $url   = parse_url($alias);

    if (isset($url['fragment'])) {
        // set path without the fragment
        $path = $url['path'];

        // prevent URL from re-aliasing
        $options['alias'] = true;

        // set fragment
        $options['fragment'] = $url['fragment'];
    }

}//end bootstrap_subtheme_url_outbound_alter()


function bootstrap_subtheme_preprocess_views_view_table(&$vars)
{
    $vars['classes_array'][] = 'table table-striped';

}//end bootstrap_subtheme_preprocess_views_view_table()


function bootstrap_subtheme_preprocess_page(&$variables)
{



    $status = drupal_get_http_header('status');
    // dpm($status);
    if ($status == '403 Forbidden') {
        // $variables['theme_hook_suggestions'][] = 'page__403';
    }

    if ($status == '404 Not Found') {
        // $variables['theme_hook_suggestions'][] = 'page__404';
    }

    if (drupal_is_front_page()) {
        if (!isset($_COOKIE['landing'])) {
            setcookie('landing', 1);
            $variables['landing'] = 1;
        } else {
            setcookie('landing', 2);
            $variables['landing'] = 0;
        }

        $search              = isset($_GET['search']) ? $_GET['search'] : '';
        $variables['search'] = $search;
    }

    global $base_url;

    drupal_add_js(array('pathToTheme' => '/web'), 'setting');
    drupal_add_js(array('baseUrl' => $base_url), 'setting');
    // Drupal.settings.fulldomain
    // drupal_add_js(array('fulldomain' => $base_url), 'setting');

}//end bootstrap_subtheme_preprocess_page()


/**
 * Gets JSON data from a View rendered in the JSON data document style.
 *
 * This is useful for when working with a JSON view in code.
 *
 * @param  $name
 *   The name of the view.
 * @param  $display_id
 *   The display of the view to use.
 * @param  $args
 *   The arguments to pass to the view.
 * @param  $raw
 *   If TRUE, the JSON data is returned as a string.  Otherwise, an object
 *   representation is returned.
 * @return
 *   The JSON data in the form of an object or a string or NULL otherwise.
 */
function views_json_noheader_get($name, $display_id='default', $args=array(), $raw=false, $exsposed=null)
{
    $view = views_get_view($name);
    if (!is_object($view)) {
        return null;
    }

    // dpm($view);
    $view->display['default']->display_options['style_options']['using_views_api_mode']   = 1;
    $view->display[$display_id]->display_options['style_options']['using_views_api_mode'] = 1;

    if ($exsposed != null) {
        foreach ($exsposed as $key => $value) {
            $view->exposed_input[$key] = $value;
        };
    };

    $preview    = $view->preview($display_id, $args);
    $start_pos  = strpos($preview, '{');
    $finish_pos = strrpos($preview, '}');
    $length     = ($finish_pos - $start_pos + 1);
    $json       = trim(substr($preview, $start_pos, $length));

    if ($raw) {
        return $json;
    }

    return json_decode($json);

}//end views_json_noheader_get()


function bootstrap_subtheme_preprocess_node(&$vars)
{
    /*
        require_once 'sites/all/themes/bootstrap/bootstrap_subtheme/Mobile_Detect.php';
        $detect = new Mobile_Detect;
        $vars['is_mobile'] = $detect->isMobile();
    dpm($vars['is_mobile']);*/

    // create a template suggestion based  when the view mode is full  or default
    if (($vars['view_mode'] == 'full') || ($vars['view_mode'] == 'default')) {
        if ($vars['node']->type == 'article') {
            if (isset($vars['node']->field_category[LANGUAGE_NONE])) {
                $category_name                    = strtolower($vars['node']->field_category[LANGUAGE_NONE][0]['taxonomy_term']->name);
                $category_name                    = str_replace('-', '', $category_name);
                $vars['theme_hook_suggestions'][] = 'node__'.$vars['node']->type.'__'.$category_name;
            }
        };

        // update template variables  if field_category exist
        if (isset($vars['node']->field_category[LANGUAGE_NONE])) {
            // update template variables  when the taxonomy term value is bundles
            if ($vars['node']->field_category[LANGUAGE_NONE][0]['taxonomy_term']->name == 'Bundles') {
                preprocess_bundle_page($vars);
            }; //end if

            // Update template variables  when the taxonomy term value is Plug-in
            if (isset($vars['node']->field_category)) {
                if ($vars['node']->field_category[LANGUAGE_NONE][0]['taxonomy_term']->name == 'Plugin') {
                    preprocess_plugin_page($vars);
                } //end if
            }; //end if
        }; //end if
    }//end if

    // Create a template based on the  view mode
    if (isset($vars['view_mode'])) {
        $vars['theme_hook_suggestions'][] = 'node__'.$vars['node']->type.'__'.$vars['view_mode'];
        $vars['theme_hook_suggestions'][] = 'node__'.$vars['node']->nid.'__'.$vars['view_mode'];
    }

    // Create a template based on the url path  part
    $url      = str_replace('-', '', $vars['node_url']);
    $urlParts = explode('/', $url);
    unset($urlParts[0]);

    if ($urlParts[1] !== false) {
        $out = array();
        $sug = 'node';

        foreach ($urlParts as $val) {
            $sug  .= '__'.$val;
            $out[] = $sug;
        };
        // dpm($out);
        $vars['theme_hook_suggestions'] = array_merge($vars['theme_hook_suggestions'], $out);
    };

    // create a template base on field_text_style for article_text
    if ($vars['node']->type == 'article_text') {
        // dpm($vars['node']);
        $style = '';
        if (isset($vars['node']->field_text_style[LANGUAGE_NONE])) {
            $style = $vars['node']->field_text_style[LANGUAGE_NONE][0]['value'];
        } else {
            // $style = $vars['node']->field_text_style[0]['value'];
        }

        $style = str_replace('-', '', $style);
        $style = str_replace(' ', '', $style);
        $style = strtolower($style);
        $vars['theme_hook_suggestions'][] = 'node__'.$vars['node']->type.'__'.$style;
        if ($vars['view_mode']) {
            $vars['theme_hook_suggestions'][] = 'node__'.$vars['node']->type.'__'.$vars['view_mode'].'__'.$style;
        }
    };

    // dpm($vars['node']->type);
    if ($vars['node']->type == 'article_image') {
        // dpm($vars['node']);
        if (isset($vars['node']->field_image_style[LANGUAGE_NONE])) {
            $style = $vars['node']->field_image_style[LANGUAGE_NONE][0]['value'];
            $style = str_replace('-', '', $style);
            $style = str_replace(' ', '', $style);
            $style = strtolower($style);
            $vars['theme_hook_suggestions'][] = 'node__'.$vars['node']->type.'__'.$style;
            if ($vars['view_mode']) {
                $vars['theme_hook_suggestions'][] = 'node__'.$vars['node']->type.'__'.$vars['view_mode'].'__'.$style;
            };
        };
    };
    // dpm($vars['theme_hook_suggestions']);
    // call a function based on the hook theme  yourmodulename_preprocess_node_theme_hook
    foreach ($vars['theme_hook_suggestions'] as $suggestion) {
        $function = implode('__', array(__FUNCTION__, $suggestion));
        // dpm($function);
        if ((function_exists($function)) && ($function != __FUNCTION__)) {
            $function($vars);
        }
    };
    // dpm($vars['theme_hook_suggestions']);

}//end bootstrap_subtheme_preprocess_node()


function bootstrap_subtheme_preprocess_node__node__store(&$vars)
{
    $vars['section'] = 'BUY/TRY';
    // dpm($vars);
}//end bootstrap_subtheme_preprocess_node__node__store()


function bootstrap_subtheme_preprocess_node__node__demos(&$vars)
{
    $vars['section'] = 'BUY/TRY';

}//end bootstrap_subtheme_preprocess_node__node__demos()


function bootstrap_subtheme_preprocess_node__node__upgrades(&$vars)
{
    $vars['section'] = 'BUY/TRY';

}//end bootstrap_subtheme_preprocess_node__node__upgrades()


function bootstrap_subtheme_preprocess_node__node__dealers(&$vars)
{
    $vars['section'] = 'BUY/TRY';

}//end bootstrap_subtheme_preprocess_node__node__dealers()


function bootstrap_subtheme_preprocess_node__node__eductation(&$vars)
{
    $vars['section'] = 'BUY/TRY';

}//end bootstrap_subtheme_preprocess_node__node__eductation()


function bootstrap_subtheme_preprocess_node__node__support__venueinstallers(&$vars)
{
    // dpm($vars);
    $field_entities = $vars['node']->field_entities[LANGUAGE_NONE];
    if (is_array($field_entities)) {
        foreach ($field_entities as $key => $value) {
            $node = node_load($value['target_id']);
            if ($node->type == 'article_text') {
            };
            if ($node->type == 'article_image') {
            }; //end if
            $vars['entities'][] = $node;
        };
    }

}//end bootstrap_subtheme_preprocess_node__node__support__venueinstallers()


function bootstrap_subtheme_preprocess_node__node__bundles__custombundles(&$vars)
{
    $bundles = array();
    $query   = db_select('node', 'n');
    $query->fields('n', array('nid', 'type'));
    $query->condition('type', 'product_bundle', '=');
    $result = $query->execute()->fetchAll();
    // dpm($result);
    $bundles = array();
    foreach ($result as $value) {
        $n = node_load($value->nid);
        if ($n->title != 'Custom Bundles') {
            $logo      = file_create_url($n->field_bundle_logo[LANGUAGE_NONE][0]['uri']);
            $path      = drupal_get_path_alias('node/'.$n->field_article_entity_reference[LANGUAGE_NONE][0]['target_id']);
            $bundles[] = array(
                          'logo' => $logo,
                          'path' => $path,
                         );
        }

        // dpm($n);
    }

    $vars['bundles'] = $bundles;

}//end bootstrap_subtheme_preprocess_node__node__bundles__custombundles()


function bootstrap_subtheme_preprocess_node__node__article__artists(&$vars)
{
    // dpm(&$vars);
    $json = views_json_noheader_get('artists_list', 'json_artists_list', array($vars['nid']), true, null);
    $json = drupal_json_decode($json);

    $related = views_json_noheader_get('related_product_by_tag', 'related_product', array($vars['nid']), true, null);
    $related = drupal_json_decode($related);

    // dpm($json);
    drupal_add_js(
        array(
         'sonnox' => array(
                      'artistslist' => array(
                                        'artists' => $json,
                                        'related' => $related,
                                       ),
                     ),
        ),
        'setting'
    );

}//end bootstrap_subtheme_preprocess_node__node__article__artists()


function bootstrap_subtheme_preprocess_node__node__article__spotlight(&$vars)
{
    // dpm(&$vars);
    $json = views_json_noheader_get('spotlight_list', 'json_spotlight_list', array($vars['nid']), true, null);
    $json = drupal_json_decode($json);

    $related = views_json_noheader_get('related_product_by_tag', 'related_product', array($vars['nid']), true, null);
    $related = drupal_json_decode($related);

    // dpm($json);
    drupal_add_js(
        array(
         'sonnox' => array(
                      'spotlightlist' => array(
                                          'spotlight' => $json,
                                          'related'   => $related,
                                         ),
                     ),
        ),
        'setting'
    );

}//end bootstrap_subtheme_preprocess_node__node__article__spotlight()


function bootstrap_subtheme_preprocess_node__node__article__tips(&$vars)
{
    // dpm(&$vars);
    $json = views_json_noheader_get('tips_list', 'json_tips_list', array($vars['nid']), true, null);
    $json = drupal_json_decode($json);

    $related = views_json_noheader_get('related_product_by_tag', 'related_product', array($vars['nid']), true, null);
    $related = drupal_json_decode($related);

    // dpm($json);
    drupal_add_js(
        array(
         'sonnox' => array(
                      'tipslist' => array(
                                     'tips'    => $json,
                                     'related' => $related,
                                    ),
                     ),
        ),
        'setting'
    );

}//end bootstrap_subtheme_preprocess_node__node__article__tips()


function bootstrap_subtheme_preprocess_node__node__article__news(&$vars)
{
    // dpm(&$vars);
    $json = views_json_noheader_get('news_list', 'json_news_list', array($vars['nid']), true, null);
    $json = drupal_json_decode($json);

    $related = views_json_noheader_get('related_product_by_tag', 'related_product', array($vars['nid']), true, null);
    $related = drupal_json_decode($related);

    // dpm($related);
    drupal_add_js(
        array(
         'sonnox' => array(
                      'newslist' => array(
                                     'news'    => $json,
                                     'related' => $related,
                                    ),
                     ),
        ),
        'setting'
    );

}//end bootstrap_subtheme_preprocess_node__node__article__news()


function bootstrap_subtheme_preprocess_node__node__home(&$vars)
{
    // dpm('preprocess video');
    $search = isset($_GET['search']) ? $_GET['search'] : '';

    $json = views_json_noheader_get('sonnox_articles_homepage', 'json_articles', null, true, array('query' => $search));
    // dpm("test".$json);
    $json = drupal_json_decode($json);

    $json_articles_filters = views_json_noheader_get('homepage_articles_filters', 'json_filters', null, true);
    // dpm("test".$json);
    $articles_filters = drupal_json_decode($json_articles_filters);
    // dpm(articles_filters);
    $terms = array();
    foreach ($articles_filters['terms'] as $term) {
        $terms[] = array(
                    'tid'  => $term['term']['tid'],
                    'name' => $term['term']['name'],
                   );
    }

    // load slides
    $json_slides = views_json_noheader_get('homepage_slideshow', 'homepage_slideshow_json', null, true, array('query' => $search));
    // dpm("test".$json);
    $slides = $articles_filters = drupal_json_decode($json_slides);

    drupal_add_js(
        array(
         'sonnox' => array(
                      'articleslist'    => array(
                                            'tiles' => $json['nodes'],
                                            'pager' => $json['pager'],
                                           ),
                      'slides'          => $slides,
                      'articlesfilters' => array('terms' => $terms),
                     ),
        ),
        'setting'
    );

}//end bootstrap_subtheme_preprocess_node__node__home()


function bootstrap_subtheme_preprocess_node__node__products(&$vars)
{
    // dpm('preprocess video');
    $search = isset($_GET['search']) ? $_GET['search'] : '';

    $json = views_json_noheader_get('products_list', 'products_json', null, true, array('query' => $search));
    // dpm("test".$json);
    $json = drupal_json_decode($json);

    drupal_add_js(
        array(
         'sonnox' => array('productslist' => $json),
        ),
        'setting'
    );

}//end bootstrap_subtheme_preprocess_node__node__products()


function bootstrap_subtheme_preprocess_node__node__about__team(&$vars)
{
    $json = views_json_noheader_get('people', 'json', null, true);
    // dpm("test".$json);
    $json = drupal_json_decode($json);

    drupal_add_js(
        array(
         'sonnox' => array(
                      'people' => array(
                                   'list'  => $json['nodes'],
                                   'pager' => $json['pager'],
                                  ),
                     ),
        ),
        'setting'
    );

}//end bootstrap_subtheme_preprocess_node__node__about__team()


function bootstrap_subtheme_preprocess_node__node__about__partners(&$vars)
{
    $json = views_json_noheader_get('partnerslist', 'json', null, true);
    // dpm("test".$json);
    $json = drupal_json_decode($json);

    drupal_add_js(
        array(
         'sonnox' => array(
                      'partners' => array(
                                     'list'  => $json['nodes'],
                                     'pager' => $json['pager'],
                                    ),
                     ),
        ),
        'setting'
    );

}//end bootstrap_subtheme_preprocess_node__node__about__partners()


function bootstrap_subtheme_preprocess_node__node__support(&$vars)
{
    $json = views_json_noheader_get('faq_short_list', 'json', null, true);
    // dpm("test".$json);
    $json = drupal_json_decode($json);

    $spjson = views_json_noheader_get('support_news', 'json', null, true);
    $spjson = drupal_json_decode($spjson);

    $vjson = views_json_noheader_get('how_to_videos', 'json', null, true);
    $vjson = drupal_json_decode($vjson);

    drupal_add_js(
        array(
         'sonnox' => array(
                      'faqshortlist' => array(
                                         'faqs'  => $json['nodes'],
                                         'pager' => $json['pager'],
                                        ),
                      'supportnews'  => array(
                                         'list'  => $spjson['nodes'],
                                         'pager' => $spjson['pager'],
                                        ),
                      'howtovideos'  => array(
                                         'list'  => $vjson['nodes'],
                                         'pager' => $vjson['pager'],
                                        ),
                     ),
        ),
        'setting'
    );

}//end bootstrap_subtheme_preprocess_node__node__support()


function bootstrap_subtheme_preprocess_node__node__support__knowledgebase(&$vars)
{
    $json = views_json_noheader_get('faq_group_by_terms', 'json', null, true);
    // dpm("test".$json);
    $json = drupal_json_decode($json);

    drupal_add_js(
        array(
         'sonnox' => array(
                      'faqlist' => array(
                                    'list'  => $json['nodes'],
                                    'pager' => $json['pager'],
                                   ),
                     ),
        ),
        'setting'
    );

}//end bootstrap_subtheme_preprocess_node__node__support__knowledgebase()


function bootstrap_subtheme_preprocess_node__node__learn(&$vars)
{
    // dpm('preprocess video');
    $search = isset($_GET['search']) ? $_GET['search'] : '';

    $json = views_json_noheader_get('sonnox_learn_articles', 'json_learnarticles', null, true, array('query' => $search));
    // dpm("test".$json);
    $json = drupal_json_decode($json);

    $json_spotlight_filters = views_json_noheader_get('articles_filters', 'json_filters', null, true);
    // dpm("test".$json);
    $spotlight_filters = drupal_json_decode($json_spotlight_filters);
    // dpm($videos_filters);
    $terms = array();
    foreach ($spotlight_filters['terms'] as $term) {
        $terms[] = array(
                    'tid'  => $term['term']['tid'],
                    'name' => $term['term']['name'],
                   );
    }

    drupal_add_js(
        array(
         'sonnox' => array(
                      'learnarticlesresult'  => array(
                                                 'tiles' => $json['nodes'],
                                                 'pager' => $json['pager'],
                                                ),
                      'learnarticlesfilters' => array('terms' => $terms),
                     ),
        ),
        'setting'
    );

}//end bootstrap_subtheme_preprocess_node__node__learn()


function bootstrap_subtheme_preprocess_node__node__learn__tips(&$vars)
{
    // dpm('preprocess video');
    $search = isset($_GET['search']) ? $_GET['search'] : '';

    $json = views_json_noheader_get('sonnox_learn_tips', 'json_tips', null, true, array('query' => $search));
    // dpm("test".$json);
    $json = drupal_json_decode($json);

    $json_tips_filters = views_json_noheader_get('tips_filters', 'json_filters', null, true);
    // dpm("test".$json);
    $tips_filters = drupal_json_decode($json_tips_filters);
    // dpm($videos_filters);
     $terms = array();
    foreach ($tips_filters['terms'] as $term) {
        $terms[] = array(
                    'tid'  => $term['term']['tid'],
                    'name' => $term['term']['name'],
                   );
    }

    drupal_add_js(
        array(
         'sonnox' => array(
                      'tipsresult'  => array(
                                        'tiles' => $json['nodes'],
                                        'pager' => $json['pager'],
                                       ),
                      'tipsfilters' => array('terms' => $terms),
                     ),
        ),
        'setting'
    );

}//end bootstrap_subtheme_preprocess_node__node__learn__tips()


function bootstrap_subtheme_preprocess_node__node__learn__news(&$vars)
{
    // dpm('preprocess video');
    $search = isset($_GET['search']) ? $_GET['search'] : '';

    $json = views_json_noheader_get('sonnox_learn_news', 'json_news', null, true, array('query' => $search));
    // dpm("test".$json);
    $json = drupal_json_decode($json);

    $json_news_filters = views_json_noheader_get('news_filters', 'json_filters', null, true);
    // dpm("test".$json);
    $news_filters = drupal_json_decode($json_news_filters);
    // dpm($videos_filters);
     $terms = array();
    foreach ($news_filters['terms'] as $term) {
        $terms[] = array(
                    'tid'  => $term['term']['tid'],
                    'name' => $term['term']['name'],
                   );
    }

    drupal_add_js(
        array(
         'sonnox' => array(
                      'newsresult'  => array(
                                        'tiles' => $json['nodes'],
                                        'pager' => $json['pager'],
                                       ),
                      'newsfilters' => array('terms' => $terms),
                     ),
        ),
        'setting'
    );

}//end bootstrap_subtheme_preprocess_node__node__learn__news()


function bootstrap_subtheme_preprocess_node__node__learn__spotlight(&$vars)
{
    // dpm('preprocess video');
    $search = isset($_GET['search']) ? $_GET['search'] : '';

    $json = views_json_noheader_get('sonnox_learn_spotlight', 'json_spotlight', null, true, array('query' => $search));
    // dpm("test".$json);
    $json = drupal_json_decode($json);

    $json_spotlight_filters = views_json_noheader_get('spotlight_filters', 'json_filters', null, true);
    // dpm("test".$json);
    $spotlight_filters = drupal_json_decode($json_spotlight_filters);
    // dpm($videos_filters);
     $terms = array();
    foreach ($spotlight_filters['terms'] as $term) {
        $terms[] = array(
                    'tid'  => $term['term']['tid'],
                    'name' => $term['term']['name'],
                   );
    }

    drupal_add_js(
        array(
         'sonnox' => array(
                      'spotlightresult'  => array(
                                             'tiles' => $json['nodes'],
                                             'pager' => $json['pager'],
                                            ),
                      'spotlightfilters' => array('terms' => $terms),
                     ),
        ),
        'setting'
    );

}//end bootstrap_subtheme_preprocess_node__node__learn__spotlight()


function bootstrap_subtheme_preprocess_node__node__learn__videos(&$vars)
{
    // dpm('preprocess video');
    $search = isset($_GET['search']) ? $_GET['search'] : '';

    $json = views_json_noheader_get('sonnox_learn_videos', 'json_videos', null, true, array('query' => $search));
    // dpm("test".$json);
    $json = drupal_json_decode($json);

    $json_videos_filters = views_json_noheader_get('videos_filters', 'json_filters', null, true);
    // dpm("test".$json);
    $videos_filters = drupal_json_decode($json_videos_filters);
    // dpm($videos_filters);
     $terms = array();
    foreach ($videos_filters['terms'] as $term) {
        $terms[] = array(
                    'tid'  => $term['term']['tid'],
                    'name' => $term['term']['name'],
                   );
    }

    drupal_add_js(
        array(
         'sonnox' => array(
                      'videosresult'  => array(
                                          'tiles' => $json['nodes'],
                                          'pager' => $json['pager'],
                                         ),
                      'videosfilters' => array('terms' => $terms),
                     ),
        ),
        'setting'
    );

}//end bootstrap_subtheme_preprocess_node__node__learn__videos()


function bootstrap_subtheme_preprocess_node__node__learn__artists(&$vars)
{
    $search = isset($_GET['search']) ? $_GET['search'] : '';

    $json = views_json_noheader_get('sonnox_learn_artists', 'json_artists', null, true, array('query' => $search));

    $json = drupal_json_decode($json);

    $json_artists_filters = views_json_noheader_get('artists_filters', 'json_filters', null, true);
    // dpm("test".$json);
    $artists_filters = drupal_json_decode($json_artists_filters);
    // dpm($videos_filters);
    $terms = array();
    foreach ($artists_filters['terms'] as $term) {
        $terms[] = array(
                    'tid'  => $term['term']['tid'],
                    'name' => $term['term']['name'],
                   );
    }

    // dpm("test".$terms);
    drupal_add_js(
        array(
         'sonnox' => array(
                      'artistsresult'  => array(
                                           'tiles' => $json['nodes'],
                                           'pager' => $json['pager'],
                                          ),
                      'artistsfilters' => array('terms' => $terms),
                     ),
        ),
        'setting'
    );

}//end bootstrap_subtheme_preprocess_node__node__learn__artists()


function bootstrap_subtheme_preprocess_views_views_json_style_simple(&$vars)
{
    global $pager_total, $pager_page_array, $pager_total_items, $pager_limits;
    $vars['rows']['pager'] = array(
                              'pages' => $pager_total[0],
                              'page'  => $pager_page_array[0],
                              'count' => intval($pager_total_items[0]),
                              'limit' => intval($pager_limits[0]),
                             );

}//end bootstrap_subtheme_preprocess_views_views_json_style_simple()


function preprocess_bundle_page(&$vars)
{
    // dpm($vars['node']);
    $field_entities = $vars['node']->field_entities[LANGUAGE_NONE];
    $pos            = 1;
    $color          = '';
    if (is_array($field_entities)) {
        foreach ($field_entities as $key => $value) {
            $_n = node_load($value['target_id']);
            if ($_n->type == 'article_text') {
                $nv = node_view($_n);

                $node_ref = array('node_ref' => $nv);

                if (isset($_n->field_text_style[LANGUAGE_NONE])) {
                    if ($_n->field_text_style[LANGUAGE_NONE][0]['value'] == 'col-sm-5') {
                        $node_ref['nid'] = $_n->nid;
                        // dpm($node_ref);
                        $section['items'][] = theme('text_container_col_sm_5', array('node_ref' => $node_ref, 'nid' => $_n->nid));
                        $entity_content     = '';
                        $section['n']       = ($section['n'] + 1);
                    };
                    if ($_n->field_text_style[LANGUAGE_NONE][0]['value'] == 'default') {
                        $entity_content = theme('text_container_full_width', array('node_ref' => $node_ref));
                        closesection($vars, $section);
                    };
                    if ($_n->field_text_style[LANGUAGE_NONE][0]['value'] == 'center') {
                        $entity_content = theme('text_container_center', array('node_ref' => $node_ref));
                        closesection($vars, $section);
                    };
                    if ($_n->field_text_style[LANGUAGE_NONE][0]['value'] == 'centerfull') {
                        $entity_content = theme('text_container_centerfull', array('node_ref' => $node_ref));
                        closesection($vars, $section);
                    };
                }                ;
            }            ;
            if ($_n->type == 'article_image') {
                $nv       = node_view($_n);
                $node_ref = array('node_ref' => $nv);

                if ($_n->field_image_style[LANGUAGE_NONE][0]['value'] == 'left') {
                    $entity_content = theme('image_container_full_left', array('node_ref' => $node_ref, 'nid' => $_n->nid));
                };
                if ($_n->field_image_style[LANGUAGE_NONE][0]['value'] == 'right') {
                    $entity_content = theme('image_container_full_right', array('node_ref' => $node_ref, 'nid' => $_n->nid));
                };
                if ($_n->field_image_style[LANGUAGE_NONE][0]['value'] == 'center') {
                    $entity_content = theme('image_container_full_center', array('node_ref' => $node_ref, 'nid' => $_n->nid));
                };
            }; //end if

            if ($_n->type == 'product_bundle') {
                // dpm($_n);
                $prices = array();
                if (isset($_n->field_product_code[LANGUAGE_NONE])) {
                    foreach ($_n->field_product_code[LANGUAGE_NONE] as $item) {
                        $field       = entity_load('field_collection_item', array($item['value']));
                        $productcode = reset($field)->field_code[LANGUAGE_NONE][0]['safe_value'];
                        // dpm($productcode);
                        $json = get_bundle_price($productcode);
                        // dpm($json);
                        $price_obj = $json;
                        $price_obj = reset($price_obj);
                        // dpm($price_obj);
                        $prices[$productcode]['GBP']   = $price_obj['Price'];
                        $prices[$productcode]['EUR']   = $price_obj['Price_EUR'];
                        $prices[$productcode]['USD']   = $price_obj['Price_USD'];
                        $prices[$productcode]['title'] = reset($field)->field_title[LANGUAGE_NONE][0]['safe_value'];

                        /*
                            $prices[$productcode]['GBP'] = reset($field)->field_title[LANGUAGE_NONE][0]['safe_value'];
                            $prices[$productcode]['USD'] = reset($field)->field_title[LANGUAGE_NONE][0]['safe_value'];
                            $prices[$productcode]['EUR'] = reset($field)->field_title[LANGUAGE_NONE][0]['safe_value'];
                            */
                        // dpm($field);
                    }//end foreach
                }                ;
                $banner_sentence = '';
                if (isset($_n->field_top_banner_sentence[LANGUAGE_NONE])) {
                    $banner_sentence = $_n->field_top_banner_sentence[LANGUAGE_NONE][0]['safe_value'];
                }

                $vars['sentence'] = $banner_sentence;
                $color            = $_n->field_color[LANGUAGE_NONE][0]['rgb'];
                $nv               = node_view($_n);
                $node_ref         = array('node_ref' => $nv);
                $plugins_data     = load_bundle_plugins_list($node_ref);
                $formats          = load_bundle_compatibility($node_ref);
                $entity_content   = theme('product_bundle_container_full_width', array('node_ref' => $node_ref, 'formats' => $formats, 'plugins' => $plugins_data, 'prices' => $prices));
            }            ;

            $vars['entities'][] = $entity_content;
            $pos               += 1;
        }//end foreach
    }//end if
    $vars['color'] = $color;

}//end preprocess_bundle_page()


function get_bundle_price($productcode)
{
    $url = 'https://www2.sonnox.com/pub/plug-shop/callback/productPrices.php';
    // 'https://www2.sonnox.com/pub/plug-shop/callback/productPrice.php?code=PTXRSTG5'
    // PTXRSTG5
    $cid    = 'productcode'.$productcode;
    $result = cache_get($cid, 'cache');
    // print_r($result);
    $expire = true;
    if (isset($result)) {
        // dpm('expire');
        // dpm($result->expire);
        // dpm(is_null($result->expire));
        if (!is_null($result->expire)) {
            $expire = time() > $result->expire;
        };
    }

    // dpm($expire);
    // dpm('time');
    // dpm(time() );
    if ((!isset($result)) || $expire) {
        // print_r('not cached');
        $data = http_build_query(array('codes' => $productcode));

        // use key 'http' even if you send the request to https://...
        $options = array(
                    'http' => array(
// 'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
        'method' => 'GET',
        // 'content' => http_build_query($data),
                              ),
                   );

        $context = stream_context_create($options);

        // $result = json_decode(file_get_contents($url.'?'.$data, false, $context), true);
        $service_url = BUNDLE_PRICE_URL.'?'.$data;
        // dpm($service_url);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $service_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        curl_close($ch);

        // execute the session
        // $curl_response = curl_exec($curl);
        // dpm($output);
        // finish off the session
        // curl_close($curl);
        $result = json_decode($output, true);

        cache_set($cid, $result, 'cache', strtotime('midnight tomorrow'));
    } else {
        // print_r('cached');
        $result = $result->data;
         // dpm('in cache');
    }//end if
    // $result = file_get_contents($url, false, $context);
    // $url = 'https://develop:Asteroid1999@www2.sonnox.com/pub/util/subscribe.php?' . 'name='. $name . '&' . 'email=' . $email ;
    // echo $url;
    return $result;

}//end get_bundle_price()


function preprocess_plugin_page(&$vars)
{
    $product_nid      = null;
    $vars['entities'] = array();
    $vars['awards']   = array();
    drupal_add_js(drupal_get_path('theme', 'bootstrap_subtheme').'/js/sonnox_plugin_page.js', array('scope' => 'footer'));
    $vars['banner_url'][0] = file_create_url($vars['field_image'][0]['uri']);

    if (isset($vars['field_image'][1])) {
        $vars['banner_url'][1] = file_create_url($vars['field_image'][1]['uri']);
    } else {
        $vars['banner_url'][1] = $vars['banner_url'][0];
    };
    // load video slideshow
    $vars['submenu'] = plugin_submenu($vars);

    $field_entities      = $vars['field_entities'];
    $pos                 = 1;
    $color               = '';
    $section             = array(
                            'items' => array(),
                            'n'     => 0,
                           );
    $system_requirements = false;
    foreach ($field_entities as $key => $value) {
        $entity_content = '';
        $_n             = node_load($value['target_id']);

        if ($_n->type == 'article_text') {
            $nv       = node_view($_n);
            $node_ref = array('node_ref' => $nv);
            // dpm($_n);
            if (isset($_n->field_text_style[LANGUAGE_NONE])) {
                if ($_n->field_text_style[LANGUAGE_NONE][0]['value'] == 'col-sm-5') {
                    $node_ref['nid'] = $_n->nid;
                    // dpm($node_ref);
                    $section['items'][] = theme('text_container_col_sm_5', array('node_ref' => $node_ref, 'nid' => $_n->nid));
                    $entity_content     = '';
                    $section['n']       = ($section['n'] + 1);
                };
                if ($_n->field_text_style[LANGUAGE_NONE][0]['value'] == 'default') {
                    $entity_content = theme('text_container_full_width', array('node_ref' => $node_ref));
                    closesection($vars, $section);
                };
                if ($_n->field_text_style[LANGUAGE_NONE][0]['value'] == 'center') {
                    $entity_content = theme('text_container_center', array('node_ref' => $node_ref));
                    closesection($vars, $section);
                };
                if ($_n->field_text_style[LANGUAGE_NONE][0]['value'] == 'centerfull') {
                    $entity_content = theme('text_container_centerfull', array('node_ref' => $node_ref));
                    closesection($vars, $section);
                };
            }//end if
;
        }//end if
        if ($_n->type == 'article_image') {
            if (isset($_n->field_image_style[LANGUAGE_NONE])) {
                $nv       = node_view($_n);
                $node_ref = array('node_ref' => $nv);

                if ($_n->field_image_style[LANGUAGE_NONE][0]['value'] == 'left') {
                    $entity_content = theme('image_container_full_left', array('node_ref' => $node_ref, 'nid' => $_n->nid));
                    closesection($vars, $section);
                };
                if ($_n->field_image_style[LANGUAGE_NONE][0]['value'] == 'right') {
                    $entity_content = theme('image_container_full_right', array('node_ref' => $node_ref, 'nid' => $_n->nid));
                    closesection($vars, $section);
                };
                if ($_n->field_image_style[LANGUAGE_NONE][0]['value'] == 'center') {
                    $entity_content = theme('image_container_full_center', array('node_ref' => $node_ref, 'nid' => $_n->nid));
                    closesection($vars, $section);
                };
                if ($_n->field_image_style[LANGUAGE_NONE][0]['value'] == 'right-picture') {
                    $entity_content = theme('image_container_image_right', array('node_ref' => $node_ref, 'nid' => $_n->nid));
                    closesection($vars, $section);
                };
                if ($_n->field_image_style[LANGUAGE_NONE][0]['value'] == 'left-picture') {
                    $entity_content = theme('image_container_image_left', array('node_ref' => $node_ref, 'nid' => $_n->nid));
                    closesection($vars, $section);
                };
                if ($_n->field_image_style[LANGUAGE_NONE][0]['value'] == 'col-sm-5') {
                    $node_ref['nid']    = $_n->nid;
                    $section['items'][] = theme('image_container_col_sm_5', array('node_ref' => $node_ref, 'nid' => $_n->nid));
                    $entity_content     = '';
                    $section['n']       = ($section['n'] + 1);
                };
            }//end if
;
        }//end if
        if ($_n->type == 'compatibility') {
            // dpm($_n);
            $formats = array();
            foreach ($_n->field_formats_entity_reference[LANGUAGE_NONE] as $item) {
                $item_node     = node_load($item['target_id']);
                $item_view     = node_view($item_node, 'content');
                $item_rendered = drupal_render($item_view);
                $formats[]     = $item_rendered;
            };
            if (isset($_n->field_sub_title_text[LANGUAGE_NONE][0]['value'])) {
                $subtitle = $_n->field_sub_title_text[LANGUAGE_NONE][0]['value'];
            } else {
                $subtitle = $_n->title;
            };
            $entity_content = theme('compatibility_container', array('formats' => $formats, 'title' => $subtitle, 'nid' => $_n->nid));
            closesection($vars, $section);
        }//end if

        if ($_n->type == 'system_requirements') {
            $entity_content      = render_system_requirements($_n);
            $system_requirements = true;
            closesection($vars, $section);
            // dpm($entity_content);
        };

        if ($_n->type == 'specification') {
            $entity_content = render_specification($_n);
            if ($system_requirements == false) {
                $entity_content = '</div></div>'.$entity_content;
            }

            closesection($vars, $section);
            // dpm($entity_content);
        };

        // bundle_container
        if ($_n->type == 'product_plug_in') {
            $product_node   = $_n;
            $entity_content = render_product_plugin_bundles($_n);

            $vars['video_slide']      = views_embed_view('video_slideshow_of_plugin_page', 'plugin_videos', $product_node->nid);
            $vars['video_thumbnails'] = views_embed_view('video_slideshow_of_plugin_page', 'plugin_videos_thumbnail', $product_node->nid);

            $json = views_json_noheader_get('plugin_videos', 'json', array($product_node->nid), true);
            // dpm("test".$json);
            $json = drupal_json_decode($json);

            drupal_add_js(
                array(
                 'sonnox' => array(
                              'videos' => array(
                                           'list'  => $json['nodes'],
                                           'pager' => $json['pager'],
                                          ),
                             ),
                ),
                'setting'
            );
            closesection($vars, $section);
        }//end if

        $vars['entities'][] = $entity_content;
    }//end foreach

    closesection($vars, $section);
    if (isset($product_node)) {
        $vars['entities'][] = render_product_reviews($product_node);
        $vars['entities'][] = render_product_awards($product_node);
        $vars['awards']     = render_icon_awards($product_node);
    }

}//end preprocess_plugin_page()


function render_icon_awards($node_pp)
{
    $awards = array();
    $title  = $node_pp->title;
    if (isset($node_pp->field_awards[LANGUAGE_NONE])) {
        foreach ($node_pp->field_awards[LANGUAGE_NONE] as $key => $value) {
            $n                = node_load($value['target_id']);
            $logo             = image_style_url('retina', $n->field_awards_image[LANGUAGE_NONE][0]['uri']);
            $image_dimentions = getimagesize($n->field_awards_image[LANGUAGE_NONE][0]['uri']);
            // $path =drupal_get_path_alias('node/'.$n->field_article_entity_reference[LANGUAGE_NONE][0]['target_id']);
            // dpm($image_dimentions);
            // $image_dimentions['division'] = $image_dimentions[0] / $image_dimentions[1];
            $width        = $image_dimentions[1];
            $height       = $image_dimentions[0];
            $width_small  = (($height / $width) * 63);
            $height_small = 63;
            $proportion   = (($width_small / $height_small) * 100);
            $awards[]     = array(
                             'logo'          => $logo,
                             'original_size' => $image_dimentions,
                             'size'          => array(
                                                 'width'  => $width_small,
                                                 'height' => 63,
                                                ),
                             'proportion'    => $proportion,
                            );
            // dpm($n);
        }//end foreach
    }//end if

    if (sizeof($awards) > 0) {
        return $awards;
    } else {
        return $awards;
    }

}//end render_icon_awards()


function render_product_reviews($product_node)
{
    // if($product_nid == null ) return '';
    $product_nid = $product_node->nid;

    $bg_target_id = field_get_items('node', $product_node, 'field_review_background');
    $bg_node      = node_load($bg_target_id[LANGUAGE_NONE][0]['target_id']);
    $nids         = '';
    $reviews      = array();
    if (isset($product_node->field_reviews[LANGUAGE_NONE])) {
        foreach ($product_node->field_reviews[LANGUAGE_NONE] as $key => $value) {
            $n = node_load($value['target_id']);
            // dpm($n);
            $logo = image_style_url('retina', $n->field_review_logo[LANGUAGE_NONE][0]['uri']);
            // $path =drupal_get_path_alias('node/'.$n->field_article_entity_reference[LANGUAGE_NONE][0]['target_id']);
            $body     = field_get_items('node', $n, 'body');
            $body_val = $body[0]['value'];
            $logo     = field_get_items('node', $n, 'field_review_logo');
            // dpm($logo);
            $logo_url = image_style_url('original', $logo[0]['uri']);
            $url      = drupal_get_path_alias('node/'.$n->nid);
            // $var->field_review_url[LANGUAGE_NONE][0]['value'];
            // $var->field_review_file[LANGUAGE_NONE][0]['uri']
            // $var->field_review_url[LANGUAGE_NONE][0]['value']
            if (isset($n->field_review_file[LANGUAGE_NONE])) {
                $fileurl = file_create_url($n->field_review_file[LANGUAGE_NONE][0]['uri']);
                $fileurl = parse_url($fileurl);
                $url     = $fileurl['path'];
            }

            if (isset($n->field_review_url[LANGUAGE_NONE])) {
                $url = $n->field_review_url[LANGUAGE_NONE][0]['value'];
            }

            $reviews[] = array(
                          'logo' => $logo_url,
                          'body' => $body_val,
                          'url'  => $url,
                         );
            // dpm($n);
        }//end foreach
    }//end if

    $background[0] = image_style_url('original', $bg_node['field_article_image']['#items'][0]['uri']);
    $background[1] = image_style_url('original', $bg_node['field_article_image']['#items'][0]['uri']);
    // -----------------------
    /*
        $view = views_get_view('product_reviews');
        // Repeating query, ensure the View is set to cache the results.
        $view->preview('default', array($product_nid));

        $nids = '';
        $reviews = array();
        foreach ($view->result as $row) {
        $nid = $row->nid;
        $review = node_load($nid);
        $body = field_get_items('node', $review, 'body');
        $body_val = $body[0]['value'];
        $logo = field_get_items('node', $review, 'field_review_logo');
        //  dpm($logo);
        $logo_url = image_style_url('retina', $logo[0]['uri']);
        $url = drupal_get_path_alias('node/' . $nid);
        $reviews[] = array('logo' => $logo_url, 'body' => $body_val, 'url' => $url);
    }*/

    if (sizeof($reviews) > 0) {
        return theme('reviews_container', array('reviews' => $reviews, 'background' => $background, 'nid' => $product_nid));
    } else {
        return '';
    }

}//end render_product_reviews()


function closesection(&$vars, &$section)
{
    if ($section['n'] >= 1) {
        $section['n'] = 0;
        // dpm($section);
        $vars['entities'][] = theme('section_full_width', array('items' => $section['items']));
    }

}//end closesection()



;


function render_product_awards($node_pp)
{
        $awards = array();
    $title      = $node_pp->title;
    if (isset($node_pp->field_awards[LANGUAGE_NONE])) {
        foreach ($node_pp->field_awards[LANGUAGE_NONE] as $key => $value) {
            $n                = node_load($value['target_id']);
            $logo             = image_style_url('retina', $n->field_awards_image[LANGUAGE_NONE][0]['uri']);
            $image_dimentions = getimagesize($n->field_awards_image[LANGUAGE_NONE][0]['uri']);
            // $path =drupal_get_path_alias('node/'.$n->field_article_entity_reference[LANGUAGE_NONE][0]['target_id']);
            // dpm($image_dimentions);
            // $image_dimentions['division'] = $image_dimentions[0] / $image_dimentions[1];
            $width        = $image_dimentions[1];
            $height       = $image_dimentions[0];
            $width_small  = (($height / $width) * 63);
            $height_small = 63;
            $proportion   = (($width_small / $height_small) * 100);
            $awards[]     = array(
                             'logo'          => $logo,
                             'original_size' => $image_dimentions,
                             'size'          => array(
                                                 'width'  => $width_small,
                                                 'height' => 63,
                                                ),
                             'proportion'    => $proportion,
                            );
            // dpm($n);
        }//end foreach
    }//end if

    if (sizeof($awards) > 0) {
        // watchdog('template', 'message <pre>@imagedimension</pre>', array('@imagedimension' => var_export($awards, true). ' test '));
        return theme('awards_container', array('awards' => $awards, 'nid' => $node_pp->nid));
    } else {
        return '';
    }

}//end render_product_awards()


function render_product_plugin_bundles($node_pp)
{
    // dpm($node_pp);
    $bundles = array();
    $title   = $node_pp->title;
    if (isset($node_pp->field_bundle_list_entity_referen[LANGUAGE_NONE])) {
        foreach ($node_pp->field_bundle_list_entity_referen[LANGUAGE_NONE] as $key => $value) {
            $n         = node_load($value['target_id']);
            $logo      = file_create_url($n->field_bundle_logo[LANGUAGE_NONE][0]['uri']);
            $path      = drupal_get_path_alias('node/'.$n->field_article_entity_reference[LANGUAGE_NONE][0]['target_id']);
            $bundles[] = array(
                          'logo' => $logo,
                          'path' => $path,
                         );
            // dpm($n);
        }
    }

    if (sizeof($bundles) > 0) {
        return theme('bundle_container', array('bundles' => $bundles, 'title' => $title, 'nid' => $node_pp->nid));
    } else {
        return '';
    }

}//end render_product_plugin_bundles()


function render_specification($node_spec)
{
    $items = array();
    // dpm($node_spec);
    $_subtitle = '';
    if (isset($node_spec->field_sub_title_text[LANGUAGE_NONE])) {
        $_subtitle = $node_spec->field_sub_title_text[LANGUAGE_NONE][0]['value'];
    };

    $objects = array();
    foreach ($node_spec->field_specification_content[LANGUAGE_NONE] as $key => $value) {
        $n = node_load($value['target_id']);
        if (isset($n->field_sub_title_text[LANGUAGE_NONE][0]['value'])) {
            $subtitle = $n->field_sub_title_text[LANGUAGE_NONE][0]['value'];
        } else {
            $subtitle = $n->title;
        };
        $content_tab                 = node_load($n->nid);
        $objects[$n->nid]['title']   = $subtitle;
        $objects[$n->nid]['content'] = node_view($content_tab, 'responsive_tab');
    };

    if (sizeof($objects) > 0) {
        $tabs = theme('responsivetabs', array('title' => '$title', 'id' => $node_spec->nid, 'items' => $objects));
    };
    return theme('specification_container', array('tabs' => $tabs, 'subtitle' => $_subtitle, 'nid' => $node_spec->nid));

}//end render_specification()



;


function render_system_requirements($node_requirements)
{
    $items = array();
    // dpm($node_requirements);
    $modaltitle = '';
    if (isset($node_requirements->field_sub_title_text[LANGUAGE_NONE])) {
        $modaltitle = $node_requirements->field_sub_title_text[LANGUAGE_NONE][0]['value'];
    };
    // dpm($modaltitle);
    // dpm($node_requirements->field_sub_title_text[LANGUAGE_NONE][0]['value']);
    foreach ($node_requirements->field_system_requirements_conten[LANGUAGE_NONE] as $key => $value) {
        $n = node_load($value['target_id']);
        if ((isset($n->field_promote_to_submenu[LANGUAGE_NONE])) && ($n->field_promote_to_submenu[LANGUAGE_NONE][0]['value'] == 1)) {
            if (isset($n->field_sub_title_text[LANGUAGE_NONE][0]['value'])) {
                $subtitle = $n->field_sub_title_text[LANGUAGE_NONE][0]['value'];
            } else {
                $subtitle = $n->title;
            };

            if (isset($n->field_show_subtitle[LANGUAGE_NONE][0]['value'])) {
                if ($n->field_show_subtitle[LANGUAGE_NONE][0]['value'] == 1) {
                    $showsubtitle = true;
                } else {
                    $showsubtitle = false;
                };
            } else {
                $showsubtitle = false;
            };
            $items[$n->nid]['title']   = $subtitle;
            $items[$n->nid]['content'] = node_view($n, 'system_requirements');
        } else {
            $content_outside[$n->nid] = node_view($n, 'system_requirements');
            // dpm($content_outside);
        }//end if
;
    }//end foreach
    $items_rendered = theme('responsivetabs', array('subtitle' => $modaltitle, 'id' => $node_requirements->nid, 'items' => $items));

    return theme('system_requirements_container', array('items' => $items_rendered, 'title' => $modaltitle, 'content_outside' => $content_outside, 'nid' => $node_requirements->nid));

}//end render_system_requirements()


function plugin_submenu($vars)
{
    $submenu     = '';
    $source_node = node_load($vars['nid']);
    foreach ($vars['field_entities'] as $key => $value) {
        $n = node_load($value['target_id']);
        if (isset($n->field_promote_to_submenu[LANGUAGE_NONE])) {
            if ($n->field_promote_to_submenu[LANGUAGE_NONE][0]['value'] == 1) {
                // file link
                if ($n->type == 'article_file') {
                    $submenu .= '<div class=" scrollTo mnext" ng-click="showvideos = true;" data-target="#plugin-topsection" >VIDEOS</div>';
                    $nfile    = node_load($n->nid);
                    $ifile    = field_get_items('node', $source_node, 'field_file');
                    // dpm($nfile);
                    $file_field_rendered = field_view_value('node', $nfile, 'field_file', $nfile->field_file[LANGUAGE_NONE][0], 'direct_download');
                    $file_field_rendered['#items'][0]['description'] = strtoupper($file_field_rendered['#items'][0]['description']);
                    $file_field_rendered['#items'][0]['description'] = $file_field_rendered['#items'][0]['description'].'&nbsp<i class="fa fa-download"></i>';
                    $rfile    = render($file_field_rendered);
                    $submenu .= $rfile;
                } else {
                    if (isset($n->field_sub_title_text[LANGUAGE_NONE][0]['value'])) {
                        $subtitle = $n->field_sub_title_text[LANGUAGE_NONE][0]['value'];
                    } else {
                        $subtitle = $n->title;
                    };
                    $submenu .= "<div href='#node-".$n->nid."' class='scrollTo' data-target='#node-".$n->nid."' >".strtoupper($subtitle).'</div>';
                };
            }//end if
;
        }//end if
    }//end foreach
    return $submenu;

}//end plugin_submenu()


function load_bundle_plugins_list($n)
{
    $plugins      = $n['node_ref']['field_plug_in_list_entity_refere']['#items'];
    $plugins_data = array();
    // $ids = array_values($plugins);
    // print_r(array_values(array_column($plugins, 'target_id')));
    $ids   = array_values(array_column($plugins, 'target_id'));
    $nodes = node_load_multiple($ids);
    foreach ($nodes as $key => $value) {
        $np = $value;
        // node_load($value['target_id']);
        // $npv = node_view ($np);
        // dpm($np->title);
        // dpm($np->field_short_title[LANGUAGE_NONE][0]['value']);
        $imageuri       = $np->field_square_image[LANGUAGE_NONE][0]['uri'];
        $title          = $np->field_short_title[LANGUAGE_NONE][0]['value'];
        $article_id     = $np->field_article_entity_reference[LANGUAGE_NONE][0]['target_id'];
        $article_url    = drupal_get_path_alias('node/'.$article_id);
        $plugins_data[] = array(
                           'title' => $title,
                           'image' => $imageuri,
                           'url'   => $article_url,
                          );
    };
    return $plugins_data;

}//end load_bundle_plugins_list()


function load_bundle_compatibility($n)
{
    // dpm($n);
    $compatibility = node_load($n['node_ref']['field_compatitbility']['#items'][0]['target_id']);
    $formats       = array();
    // dpm($compatibility);
    if (isset($compatibility->field_formats_entity_reference[LANGUAGE_NONE])) {
        $compatibility_list = $compatibility->field_formats_entity_reference[LANGUAGE_NONE];

        if (is_array($compatibility_list)) {
            foreach ($compatibility_list as $key => $value) {
                $format_node  = node_load($value['target_id']);
                $format_title = $format_node->title;
                $formats[]    = array('title' => $format_title);
            }
        }
    }

    return $formats;

}//end load_bundle_compatibility()


function get_compatibility_format($n)
{
    // dpm($n);
    $compatibility = node_load($n);
    // dpm($compatibility);
    $compatibility_list = $compatibility->field_formats_entity_reference[LANGUAGE_NONE];
    $formats            = array();
    if (is_array($compatibility_list)) {
        foreach ($compatibility_list as $key => $value) {
            $format_node  = node_load($value['target_id']);
            $format_title = $format_node->title;
            $formats[]    = array('title' => $format_title);
        }
    }

    return json_decode($formats);

}//end get_compatibility_format()


function get_submenu_tree($menu='primary-links', $title)
{
    $tree = menu_tree_all_data($menu);
    foreach ($tree as $branch) {
        if ($branch['link']['title'] == $title) {
            return $branch['below'];
        }
    }

    return array();

}//end get_submenu_tree()


function bootstrap_subtheme_preprocess_html(&$variables)
{
    // Exclude specified JavaScript files from theme.
    // add query after pathauto url
    // bring current url info
    $url   = request_uri();
    $query = drupal_get_query_parameters();
    // dpm('url'.$url);
    // dpm($query);
    // url seems to be: "path?field=value" - (encoded) "path%3Ffield%3Dvalue"
    if (strpos($url, '?') !== false and empty($query)) {
        // we'll have $url['path'] = 'path' and $url['query'] = 'field=value'
        $url = parse_url($url);
        // dpm($url);
        // make $query associative array
        parse_str($url['query'], $query);

        dpm($url);
        // go to where you were supposed to go from the beginning
        if ($url['path'] and is_array($query)) {
            drupal_goto($url['path'], array('query' => $query));
        }
    }

    /*
        $theme_path = drupal_get_path('theme', 'bootstrap_subtheme');

        drupal_add_css($theme_path . '/css/class_script.css', array(
        'type' => 'file',
        'every_page' => true,
        'media' => 'all',
        'preprocess' => true,
        'group' => CSS_THEME,
        'browsers' => array('IE' => true, '!IE' => true),
        'weight' => -1,
        ));

        drupal_add_css($theme_path . '/css/class_script.css');
    */

}//end bootstrap_subtheme_preprocess_html()


function bootstrap_subtheme_theme($existing, $type, $theme, $path)
{
    $hooks['user_login'] = array(
    // Forms always take the form argument.
                            'arguments'            => array('form' => null),
                            'render element'       => 'form',
                            'template'             => 'theme/user/user-login',
                            'preprocess functions' => array('bs_preprocess_user_login'),
                           );
    $hooks['user_register_form'] = array(
                                    'render element' => 'form',
                                    'template'       => 'theme/user/user-register',
                                   );

    $hooks['user_profile_form'] = array(
    // Forms always take the form argument.
                                   'arguments'      => array('form' => null),
                                   'render element' => 'form',
                                   'template'       => 'theme/user/user-profile-edit',
                                  );

    $hooks['image_container_full_left']  = array(
                                            'arguments' => array('node_ref' => null),
                                            'template'  => 'theme/components/image-container-full-left',
                                           );
    $hooks['image_container_full_right'] = array(
                                            'arguments' => array(
                                                            'node_ref' => array(),
                                                            'nid'      => null,
                                                           ),
                                            'template'  => 'theme/components/image-container-full-right',
                                           );
    $hooks['image_container_col_sm_5']   = array(
                                            'arguments' => array(
                                                            'node_ref' => array(),
                                                            'nid'      => null,
                                                           ),
                                            'template'  => 'theme/components/image-container--col-sm-5',
                                           );

    $hooks['image_container_image_left']  = array(
                                             'arguments' => array(
                                                             'node_ref' => array(),
                                                             'nid'      => null,
                                                            ),
                                             'template'  => 'theme/components/image-container-image-left',
                                            );
    $hooks['image_container_image_right'] = array(
                                             'arguments' => array(
                                                             'node_ref' => array(),
                                                             'nid'      => null,
                                                            ),
                                             'template'  => 'theme/components/image-container-image-right',
                                            );
    $hooks['image_container_full_center'] = array(
                                             'arguments' => array(
                                                             'node_ref' => array(),
                                                             'nid'      => null,
                                                            ),
                                             'template'  => 'theme/components/image-container-full-center',
                                            );
    $hooks['text_container_full_width']   = array(
                                             'arguments' => array(
                                                             'node_ref' => null,
                                                             'nid'      => null,
                                                            ),
                                             'template'  => 'theme/components/text-container-full-width',
                                            );
    $hooks['text_container_center']       = array(
                                             'arguments' => array(
                                                             'node_ref' => null,
                                                             'nid'      => null,
                                                            ),
                                             'template'  => 'theme/components/text-container-center',
                                            );
    $hooks['text_container_centerfull']   = array(
                                             'arguments' => array(
                                                             'node_ref' => null,
                                                             'nid'      => null,
                                                            ),
                                             'template'  => 'theme/components/text-container-centerfull',
                                            );

    $hooks['text_container_col_sm_5'] = array(
                                         'arguments' => array(
                                                         'node_ref' => null,
                                                         'nid'      => null,
                                                        ),
                                         'template'  => 'theme/components/text-container-col-sm-5',
                                        );

    $hooks['compatibility_container'] = array(
                                         'arguments' => array(
                                                         'formats' => null,
                                                         'title'   => null,
                                                         'nid'     => null,
                                                        ),
                                         'template'  => 'theme/components/compatibility-container',
                                        );

    $hooks['system_requirements_container'] = array(
                                               'arguments' => array(
                                                               'items'           => null,
                                                               'title'           => null,
                                                               'content_outside' => null,
                                                               'nid'             => null,
                                                              ),
                                               'template'  => 'theme/components/system-requirements-container',
                                              );

    $hooks['specification_container'] = array(
                                         'arguments' => array(
                                                         'tabs'     => null,
                                                         'subtitle' => null,
                                                         'nid'      => null,
                                                        ),
                                         'template'  => 'theme/components/specification-container',
                                        );

    $hooks['bundle_container'] = array(
                                  'arguments' => array(
                                                  'bundles' => null,
                                                  'title'   => null,
                                                  'nid'     => null,
                                                 ),
                                  'template'  => 'theme/components/bundle-container',
                                 );

    $hooks['awards_container'] = array(
                                  'arguments' => array(
                                                  'awards' => null,
                                                  'nid'    => null,
                                                 ),
                                  'template'  => 'theme/components/awards-container',
                                 );

    $hooks['reviews_container'] = array(
                                   'arguments' => array(
                                                   'reviews'    => null,
                                                   'background' => null,
                                                   'nid'        => null,
                                                  ),
                                   'template'  => 'theme/components/reviews-container',
                                  );

    $hooks['section_full_width'] = array(
                                    'arguments' => array('items' => null),
                                    'template'  => 'theme/components/section-full-width',
                                   );

    $hooks['product_bundle_container_full_width'] = array(
                                                     'template'  => 'theme/product-bundle-container-full-width',
                                                     'arguments' => array(
                                                                     'node_ref' => null,
                                                                     'formats'  => null,
                                                                     'plugins'  => null,
                                                                    ),
                                                    );

    $hooks['node__learn__videos'] = array(
                                     'arguments' => array(),
                                    );

    return $hooks;

}//end bootstrap_subtheme_theme()


function bootstrap_subtheme_preprocess_user_register(&$variables)
{
    $variables['form'] = drupal_build_form('user_register_form', user_register_form(array()));

}//end bootstrap_subtheme_preprocess_user_register()


function bootstrap_subtheme_article_node_form_alter(&$form, &$form_state, $form_id)
{
    $form['summary']['#maxlength'] = 40;
    dpm($form);
    $form['#prefix'] = "<div class='page taxonomy filter'>";
    $form['#suffix'] = '</div>';

}//end bootstrap_subtheme_article_node_form_alter()


// bootstrap_form_alter(array &$form, array &$form_state = array(), $form_id = NULL) {
function bootstrap_subtheme_form_alter(array &$form, array &$form_state=array(), $form_id=null)
{
    switch ($form_id) {
        case 'user_register_form':
            // dpm($form);
            unset($form['profile_main']);
            $form['account']['name']['#title']       = t('Username:');
            $form['account']['name']['#description'] = t('');
            $form['account']['mail']['#title']       = t('Email Address:');
            $form['account']['mail']['#description'] = t('');
            $form['actions']['submit']['#value']     = t('Register');
            $form['#attributes']['class'][]          = 'max-with-400';
            $form['actions']['submit']['#attributes']['class'][] = 'btn-success';
        break;

        case 'user_pass':
            $form['actions']['submit']['#attributes']['class'][] = 'btn-success';
            $break;
        case 'user_login_form':
        case 'user_login_block':
        case 'user_login':
            $form['name']              = array(
                                          '#type'        => 'textfield',
                                          '#title'       => t('Username:'),
                                          '#description' => t(''),
                                          '#maxlength'   => USERNAME_MAX_LENGTH,
                                          '#size'        => 15,
                                          '#required'    => true,
                                          '#attributes'  => array('placeholder' => t('Username')),
                                         );
            $form['pass']              = array(
                                          '#type'        => 'password',
                                          '#title'       => t('Password:'),
                                          '#description' => t(''),
                                          '#maxlength'   => 60,
                                          '#size'        => 15,
                                          '#required'    => true,
                                          '#attributes'  => array('placeholder' => t('Password')),
                                         );
            $form['actions']['submit'] = array(
                                          '#type'       => 'submit',
                                          '#value'      => t('Log In'),
                                          '#attributes' => array('class' => array('btn-success')),
                                         );
            $items = array();
            // Create Account & Forgotten Password
        break;
    }//end switch

    if ($form_id == 'views_exposed_form' && $form['#id'] == 'views-exposed-form-taxonomy-term-page') {
        $form['#prefix'] = "<div class='page taxonomy filter'>";
        $form['#suffix'] = '</div>';
    };

    if ($form_id == 'views_exposed_form' && $form['#id'] == 'views-exposed-form-series-teaser-list-page') {
        $form['#prefix'] = "<div class='page taxonomy filter'>";
        $form['#suffix'] = '</div>';
    };

    // dpm($form['#id']);
    if ($form_id == 'views_exposed_form' && $form['#id'] == 'views-exposed-form-user-bookmarks-default') {
        $form['#prefix'] = "<div class='page taxonomy filter'>";
        $form['#suffix'] = '</div>';
    }

    if ($form_id == 'views_exposed_form' && $form['#id'] == 'views-exposed-form-user-favorites-default') {
        $form['#prefix'] = "<div class='page taxonomy filter'>";
        $form['#suffix'] = '</div>';
    }

    if ($form_id == 'views_exposed_form' && $form['#id'] == 'views-exposed-form-competitions-teaser-list-page') {
        $form['#prefix'] = "<div class='page taxonomy filter'>";
        $form['#suffix'] = '</div>';
    }

    if ($form_id == 'views_exposed_form' && $form['#id'] == 'views-exposed-form-competitions-product-teaser-list-page') {
        $form['#prefix'] = "<div class='page taxonomy filter'>";
        $form['#suffix'] = '</div>';
    }

    if ($form_id == 'views_exposed_form' && $form['#id'] == 'views-exposed-form-contest-content-view-default') {
        $form['#prefix'] = "<div class='page taxonomy filter'>";
        $form['#suffix'] = '</div>';
    }

}//end bootstrap_subtheme_form_alter()


function bootstrap_subtheme_preprocess_flag(&$vars)
{
    $state = ($vars['action'] == 'flag' ? 'off' : 'on');

    if ($vars['flag_name_css'] == 'bookmarks') {
        $flag = flag_get_flag('favorites');
        // dpm($vars);
        $count = $flag->get_count($vars['entity_id']);

        if ($state == 'off') {
            $vars['link_text'] = '<div class="flag-wrapper"><div class="flag-area-bookmarks"><i class="fa fa-star"></i> <div class="flag-container">          <div class="loader-wrapper"><span class="flag-loader loader loader-bars"><span></span></span></div><div class="flag-bookmark flag-link">bookmark</div></div></div>';
        } else {
            $vars['link_text'] = '<div class="flag-wrapper active"><div class="flag-area-bookmarks"><i class="fa fa-star"></i> <div class="flag-container"> <div class="loader-wrapper"><span class=" flag-loader loader loader-bars"><span></span></span></div><div class="flag-bookmark flag-link"><div class="link-container">Unbookmark</div></div></div></div>';
        }
    }

    if ($vars['flag_name_css'] == 'favorites') {
        $flag = flag_get_flag('favorites');
        // dpm($vars);
        $count = $flag->get_count($vars['entity_id']);

        $like_number = "<div class='flag-like-number'><i class='fa fa-thumbs-o-up'></i>".$count.'</div>';
        if ($state == 'off') {
            $vars['link_text'] = $like_number.'<div class="flag-wrapper"> <div class="flag-container">          <div class="loader-wrapper"><span class="flag-loader loader loader-bars"><span></span></span></div><div class="flag-link">like</div></div></div>';
        } else {
            $vars['link_text'] = $like_number.'<div class="flag-wrapper"><div class="flag-container"> <div class="loader-wrapper"><span class=" flag-loader loader loader-bars"><span></span></span></div><div class="flag-link"><div class="link-container">unlike</div></div></div></div>';
        }
    }

}//end bootstrap_subtheme_preprocess_flag()


/**
 * @file
 * menu-tree.func.php
 */


/**
 * Overrides theme_menu_tree().
 */
function bootstrap_subtheme_menu_tree(&$variables)
{
    // dpm($variables);
    return '<ul class="menu nav">'.$variables['tree'].'</ul>';

}//end bootstrap_subtheme_menu_tree()


/**
 * Bootstrap theme wrapper function for the primary menu links.
 */
function bootstrap_subtheme_menu_tree__primary(&$variables)
{
    return '<ul class="menu nav navbar-nav">'.$variables['tree'].'</ul>';

}//end bootstrap_subtheme_menu_tree__primary()


/**
 * Bootstrap theme wrapper function for the secondary menu links.
 */
function bootstrap_subtheme_menu_tree__secondary(&$variables)
{
    return '<ul class="menu nav navbar-nav secondary">'.$variables['tree'].'</ul>';

}//end bootstrap_subtheme_menu_tree__secondary()


function bootstrap_subtheme_menu_tree__menu_footer_menu(&$variables)
{
    return '<ul class="list-unstyled footer-menu paddint-top-lg">'.$variables['tree'].'</ul>';

}//end bootstrap_subtheme_menu_tree__menu_footer_menu()


function bootstrap_subtheme_menu_tree__menu_social_menu(&$variables)
{
    return '<ul class="list-unstyled social-menu">'.$variables['tree'].'</ul>';

}//end bootstrap_subtheme_menu_tree__menu_social_menu()


/**
 * Overrides theme_menu_link().
 */
function bootstrap_subtheme_menu_link(array $variables)
{
    $element  = $variables['element'];
    $sub_menu = '';
    // dpm($variables);
    if ($element['#original_link']['has_children'] == 1) {
        // Prevent dropdown functions from being added to management menu so it
        // does not affect the navbar module.
        if (($element['#original_link']['menu_name'] == 'management') && (module_exists('navbar'))) {
            $sub_menu = drupal_render($element['#below']);
        }

        // elseif ((!empty($element['#original_link']['depth'])) && ($element['#original_link']['depth'] == 1)) {
        // Add our own wrapper.
        unset($element['#below']['#theme_wrappers']);

        $sub_menu = '<ul class="">'.drupal_render($element['#below']).'</ul>';
        // Generate as standard dropdown.
        // $element['#title'] .= ' <span class="caret"></span>';
        // $element['#attributes']['class'][] = 'dropdown';
        $element['#localized_options']['html'] = true;

        // Set dropdown trigger element to # to prevent inadvertant page loading
        // when a submenu link is clicked.
        // $element['#localized_options']['attributes']['data-target'] = '#';
        // $element['#localized_options']['attributes']['class'][] = 'dropdown-toggle';
        // $element['#localized_options']['attributes']['data-toggle'] = 'dropdown';
        // }
    }//end if

    // On primary navigation menu, class 'active' is not set on active menu item.
    // @see https://drupal.org/node/1896674
    if (($element['#href'] == $_GET['q'] || ($element['#href'] == '<front>' && drupal_is_front_page())) && (empty($element['#localized_options']['language']))) {
        $element['#attributes']['class'][] = 'active';
    }

    $output = l($element['#title'], $element['#href'], $element['#localized_options']);
    return '<li'.drupal_attributes($element['#attributes']).'>'.$output.$sub_menu."</li>\n";

}//end bootstrap_subtheme_menu_link()


function bootstrap_subtheme_block_view_alter(&$data, $block)
{

}//end bootstrap_subtheme_block_view_alter()


function bootstrap_subtheme_preprocess_image(&$variables)
{
    // dpm($variables);
    foreach (array('width', 'height') as $key) {
        unset($variables[$key]);
    }

}//end bootstrap_subtheme_preprocess_image()


function bootstrap_subtheme_views_query_alter(&$view, &$query)
{
    // Find all combine fields and make them case insensitive.
    dpm('collate');
    foreach ($query->where as $group_key => $group) {
        foreach ($group['conditions'] as $key => $condition) {
            if (preg_match('/:views_combine/', $condition['field'])) {
                $query->where[$group_key]['conditions'][$key]['field'] = $condition['field'].' COLLATE utf8_general_ci';
            }
        }
    }

}//end bootstrap_subtheme_views_query_alter()


/**
 * Implements template_preprocess_file_entity
 */


function bootstrap_subtheme_preprocess_file_entity(&$vars)
{
    $width = null;
    // dpm($vars);
    // Get image width from override or file metadata settings.
    if (!empty($vars['override']['attributes']['width'])) {
        $width = $vars['override']['attributes']['width'];
    } else if (!empty($vars['metadata']['width'])) {
        $width = $vars['metadata']['width'];
    }

    // Add width to wrapper div, to limit space of caption (title and copyright).
    if ($width) {
        if (empty($vars['attributes_array']['style'])) {
            $vars['attributes_array']['style'] = 'width: '.$width.'px;';
        } else {
            $vars['attributes_array']['style'] .= ' width: '.$width.'px;';
        }
    }

    // unset($vars['elements']);
    // Add file style attributes to wrapper div too.
    if (!empty($vars['override']['attributes']['style'])) {
        if (empty($vars['attributes_array']['style'])) {
            $vars['attributes_array']['style'] = $vars['override']['attributes']['style'];
            // $vars['elements']['#attributes']['style'] = $vars['override']['attributes']['style'];
        } else {
            $vars['attributes_array']['style'] .= $vars['override']['attributes']['style'];
            // $vars['elements']['#attributes']['style'] = $vars['override']['attributes']['style'];
            // $vars['elements']['#attributes']['style'] = $vars['override']['attributes']['style'];
        }
    }

}//end bootstrap_subtheme_preprocess_file_entity()


// https://www.drupal.org/node/2194821
// hook_media_wysiwyg_token_to_markup_alter
// http://www.58bits.com/blog/2013/03/06/how-to-add-captions-to-images-in-drupal


function bootstrap_subtheme_media_wysiwyg_token_to_markup_alter(&$element, $tag_info, $settings)
{
    // dpm($element);
    // print_r($element);
    // $element['content']['#attributes']['style'] = $element['content']['file']['#item']['attributes']['style'];

}//end bootstrap_subtheme_media_wysiwyg_token_to_markup_alter()


/**
 * Implements hook_file_view_alter() from File Entity.
 */
function bootstrap_subtheme_file_view_alter(&$build, $type)
{
    // dpm($build);
    if ($build['#bundle'] === 'image') {
        unset($build['links']);
    }

}//end bootstrap_subtheme_file_view_alter()


function bootstrap_subtheme_file_url_alter(&$uri)
{
    global $user;

    // User 1 will always see the local file in this example.
    if ($user->uid == 1) {
        return;
    }

    $cdn1           = '/web';
    $cdn2           = '/web';
    $cdn_extensions = array(
                       'css',
                       'js',
                       'gif',
                       'jpg',
                       'jpeg',
                       'png',
                       'svg',
                      );

    // Most CDNs don't support private file transfers without a lot of hassle,
    // so don't support this in the common case.
    $schemes = array('public');

    $scheme = file_uri_scheme($uri);

    // Only serve shipped files and public created files from the CDN.
    if (!$scheme || in_array($scheme, $schemes)) {
        // Shipped files.
        $wrapper = file_stream_wrapper_get_instance_by_scheme($scheme);
        if (is_object($wrapper)) {
            $path = $wrapper->getDirectoryPath().'/'.file_uri_target($uri);
            // Clean up Windows paths.
            $path = str_replace('\\', '/', $path);

            // Serve files with one of the CDN extensions from CDN 1, all others from
            // CDN 2.
            $pathinfo = pathinfo($path);
            // if (isset($pathinfo['extension']) && in_array($pathinfo['extension'], $cdn_extensions)) {
            // $uri = $cdn1 . '/' . $path;
            // }
            // else {
            // docroot/sites/all/themes/bootstrap/bootstrap_subtheme
            $uri = str_replace('sites/all/themes/bootstrap/bootstrap_subtheme', 'web', $uri);
            // $uri = $cdn2 . '/' . $path;
            // }
        };
    }//end if

}//end bootstrap_subtheme_file_url_alter()


/*
    function demo_css_alter(&$css) {
    $path = drupal_get_path('module', 'node') . '/node.css';
    unset($css[$path]);
    }
*/


function bootstrap_subtheme_css_alter(&$css)
{
    // $path = str_replace('sites/all/themes/bootstrap/bootstrap_subtheme', 'web', $css[$path]);
    // ÅÅ $css[$path]
    $newCss = array();
    foreach ($css as $key => $value) {
        unset($css[$key]);
        $path          = str_replace('sites/all/themes/bootstrap/bootstrap_subtheme', 'web', $key);
        $newCss[$path] = $value;
    }

    $css = $newCss;

}//end bootstrap_subtheme_css_alter()


function bootstrap_subtheme_js_alter(&$js)
{
    // $path = str_replace('sites/all/themes/bootstrap/bootstrap_subtheme', 'web', $css[$path]);
    // ÅÅ $css[$path]
    $newCss = array();
    foreach ($js as $key => $value) {
        unset($js[$key]);
        $path          = str_replace('sites/all/themes/bootstrap/bootstrap_subtheme', 'web', $key);
        $newCss[$path] = $value;
    }

    $js = $newCss;

}//end bootstrap_subtheme_js_alter()
