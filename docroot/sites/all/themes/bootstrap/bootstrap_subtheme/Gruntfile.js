'use strict';

module.exports = function(grunt) {

    grunt.initConfig({

        csssplit: {
            your_target: {
                src: ['css/theme.css'],
                dest: 'css'
            },
        },
        watch: {
            options: {
                livereload: true
            },
            sass: {
                files: ['sass/{,**/}*.{scss,sass}'],
                tasks: ['compass:dev', 'cssmin', 'split'],
                options: {
                    livereload: false
                }
            },
            images: {
                files: ['images/**']
            },
            css: {
                files: ['css/{,**/}*.css']
            },
            js: {
                files: ['app/src/{,**/}*.js'],
                tasks: ['html2js', 'uglify', 'jshint']
            },
            html: {
                files: ['app/src/**/*.html'],
                //tasks: ['html2js', 'uglify', 'htmlangular']
                tasks: ['html2js', 'uglify']
            }
        },

        compass: {
            options: {
                config: 'config.rb',
                bundleExec: true,
                force: true,
                trace: true
            },
            dev: {
                options: {
                    environment: 'development'
                }
            },
            dist: {
                options: {
                    environment: 'production'
                }
            }
        },
        bless: {
            css: {
                options: {},
                files: {
                    'css/themeie.css': 'css/theme.css'
                }
            }
        },

        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1,
                compatibility: '-selectors.ie7Hack'
            },
            target: {
                files: {
                    'css/themeo.css': ['css/theme.css']
                }
            }
        },

        jshint: {
            options: {
                jshintrc: '.jshintrc'
            },
            all: [
                'app/src/{,**/}*.js',
                '!app/src/vendors/**/*.js'
            ]
        },

        uglify: {
            dist: {
                options: {
                    mangle: false,
                    compress: {
                        drop_console: true
                    },
                    sourceMap: false
                },
                files: {
                    'app/dist/app.min.js': [
                        // Templates
                        'tmp/templates.js',
                        // sonnox.page module
                        'app/src/page/app.js',
                        'app/src/page/**/*.js',
                        // sonnox.footer module
                        'app/src/footer/app.js',
                        'app/src/footer/**/*.js',
                        // sonnox.articles module
                        'app/src/articles/app.js',
                        'app/src/articles/**/*.js',
                        // sonnox.learnarticles module
                        'app/src/learnall/app.js',
                        'app/src/learnall/**/*.js',
                        // sonnox.common module
                        'app/src/common/app.js',
                        'app/src/common/**/*.js',
                        // sonnox.products module
                        'app/src/products/app.js',
                        'app/src/products/**/*.js',
                        // sonnox.learnsection module
                        'app/src/learnsection/app.js',
                        'app/src/learnsection/**/*.js',
                        'app/src/learnartists/app.js',
                        'app/src/learnartists/**/*.js',
                        'app/src/learntips/app.js',
                        'app/src/learntips/**/*.js',
                        'app/src/learnspotlight/app.js',
                        'app/src/learnspotlight/**/*.js',
                        'app/src/learnnews/app.js',
                        'app/src/learnnews/**/*.js',
                        'app/src/support/app.js',
                        'app/src/support/**/*.js',
                        'app/src/plugin/app.js',
                        'app/src/plugin/**/*.js',
                        'app/src/about/app.js',
                        'app/src/about/**/*.js',

                        // sonnox module (main app)
                        'app/src/app.js'
                    ]
                }
            },
            js_dev: {
                options: {
                    preserveComments: 'all',
                    beautify: {
                        width: 80,
                        beautify: true
                    } // beautify
                }, // options
                files: {
                    'js/bscript.js': [
                        'bower_components/bootstrap/js/affix.js',
                        'bower_components/bootstrap/js/alert.js',
                        'bower_components/bootstrap/js/button.js',
                        'bower_components/bootstrap/js/carousel.js',
                        'bower_components/bootstrap/js/collapse.js',
                        'bower_components/bootstrap/js/dropdown.js',
                        'bower_components/bootstrap/js/modal.js',
                        'bower_components/bootstrap/js/tooltip.js',
                        'bower_components/bootstrap/js/popover.js',
                        'bower_components/bootstrap/js/scrollspy.js',
                        'bower_components/bootstrap/js/tab.js',
                        'bower_components/bootstrap/js/transition.js',
                        'assets/js/*.js'
                    ]
                } // files
            }, // js_dev
            js_prod: {
                files: {
                    'js/bscript.js': [
                        'bower_components/bootstrap/js/affix.js',
                        'bower_components/bootstrap/js/alert.js',
                        'bower_components/bootstrap/js/button.js',
                        'bower_components/bootstrap/js/carousel.js',
                        'bower_components/bootstrap/js/collapse.js',
                        'bower_components/bootstrap/js/dropdown.js',
                        'bower_components/bootstrap/js/modal.js',
                        'bower_components/bootstrap/js/tooltip.js',
                        'bower_components/bootstrap/js/popover.js',
                        'bower_components/bootstrap/js/scrollspy.js',
                        'bower_components/bootstrap/js/tab.js',
                        'bower_components/bootstrap/js/transition.js',
                        'assets/js/*.js'
                    ]
                } // files
            } // js_prod
        },

        html2js: {
            options: {
                module: 'sonnox-templates',
                singleModule: true,
                htmlmin: {
                    collapseBooleanAttributes: true,
                    collapseWhitespace: true,
                    removeAttributeQuotes: true,
                    removeComments: true,
                    removeEmptyAttributes: true,
                    removeRedundantAttributes: true,
                    removeScriptTypeAttributes: true,
                    removeStyleLinkTypeAttributes: true
                },
                rename: function(moduleName) {
                    return moduleName.replace('../app/src/', '');
                }
            },
            main: {
                src: ['app/src/**/*.html'],
                dest: 'tmp/templates.js'
            }
        },
        htmlangular: {
            options: {
                tmplext: 'html',
                customattrs: [
                    'bx-slider',
                    'edit-spotlight*',
                    'expand-more',
                    'gallery',
                    'locale-string',
                    'location-field*',
                    'notify-when-repeat-finished',
                    'results-list',
                    'rtl',
                    'team',
                    'use-field-language',
                    'btn*',
                    'iso*',
                    'ok-*',
                    'opt-kind',
                    'sortable*',
                    'tooltip*',
                    'typeahead*',
                    'ckeditor',
                    'slick-slider',
                    'prevent-default',
                    'compare-to',
                    'slick-slider-dynamic*'
                ],
                customtags: [
                    'pagination',
                    'video-embed',
                    'from-to-hours',
                    'switch-language-button',
                    'switch-language-popup',
                    'switch-language-note',
                    'current-language-code-label',
                    'datetimepicker'
                ],
                relaxerror: [
                    'A select element with a required attribute and without a multiple attribute, and whose size is 1, must have a child option element.',
                    'Element img is missing required attribute src.',
                    'Empty heading.'
                ]
            },
            files: {
                src: ['app/src/**/*.html'],
            },
        }
    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-csssplit');
    grunt.loadNpmTasks('grunt-bless');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-html2js');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-html-angular-validate');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    grunt.registerTask('build', [
        'compass:dist',
        'jshint',
        'htmlangular',
        'html2js',
        'uglify', 'uglify:js_dev'
    ]);

    grunt.registerTask('split', [
        'bless'
    ]);

};
