  <?php
  if (isset($field_image[2])) {
      $path = image_style_url('original', $field_image[1]['uri']);
      $path2 = image_style_url('original', $field_image[2]['uri']);
} else {
        $path = image_style_url('original', $field_image[0]['uri']);
        $path2 = image_style_url('original', $field_image[1]['uri']);
    }
?>
  <div  class="promopage">

    <div  class="promo-header " >
      <div class="banner-page-title center text-center color-white">
        <H2 style="color:<?php echo $field_text_color[LANGUAGE_NONE][0]['rgb']; ?>!important;font-size:1.5e"><?php echo $title; ?></H2>
        </div>
        <div class="hidden-xs background-image position-absolute  full " style="max-width:100%;background-image:url(<?php echo $path; ?>);" ></div>
     <div  class="background-image position-absolute full hidden-sm hidden-md hidden-lg"  style="max-width:100%;background-image:url(<?php echo $path2; ?>);" ></div>
      </div>

    </div>
  <div class=" promo-content background-white" >
    <div class="container">
     <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 margin-top-lg margin-bottom-md padding-2em" >
        <?php echo $body[0]['value']; ?>
    </div>
  </div>

</div>


