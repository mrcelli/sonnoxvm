<div class="padding-100-100 background-white"   id="node-<?php echo $nid;?>">
    <div class="container">
        <div class="row content" >
            <div class="col-xs-10 col-xs-offset-1 text-xs-center col-sm-10 col-sm-offset-1  text-sm-center col-md-5 col-md-offset-1 text-md-left offsetside-text">
                <h2><?php print $node->field_sub_title_text[LANGUAGE_NONE][0]['value'];?></h2>
               <div ><?php print $node->body['und'][0]['value'];?></div>
            </div>
            <div class="col-xs-12 col-sm-6 side-image">
                    <img src="<?php  echo image_style_url('original', $node->field_article_image[LANGUAGE_NONE][0]['uri']);?>">
            </div>
        </div>
    </div>
</div>
