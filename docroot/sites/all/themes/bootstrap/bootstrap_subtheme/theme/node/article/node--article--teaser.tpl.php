<div class="page teaser">
<div class="sonnox-posts">
<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div class="content_panel teaser">
      <div class="container_left"><?php   print  str_replace('width="450" height="280"', '', render($content['field_image']));?></div>
      <div class="container_right">
          <header><hgroup>
        <h3><?php

        print render($content['field_category']);?></h3>
        <?php print render($title_prefix); ?>
    <?php if (!$page) : ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php endif; ?>
<?php print render($title_suffix); ?>
  </header></hgroup>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      hide($content['field_tags']);
      print render($content);
    ?>
    </div>
  </div>

</article>

</div>
</div>

