<?php

$image_uri = file_build_uri($content['field_parallex_image']['#items'][0]['filename']);
$derivative_uri = image_style_path('parallex_content_display', $image_uri);
$path = image_style_url('parallex_content_display', $content['field_parallex_image']['#items'][0]['uri']);
$success = file_exists($derivative_uri) || image_style_create_derivative('parallex_content_display', $image_uri, $derivative_uri);
$img_path = $path; //file_create_url($derivative_uri);

$vars = get_defined_vars();
// dpm($vars);

// parallex subtitle
if (isset($node->field_sub_title_text[LANGUAGE_NONE][0]['value'])) {$subtitle = $node->field_sub_title_text[LANGUAGE_NONE][0]['value'];
} else { $subtitle = $node->title;
}
;
//dpm($node);
if (isset($node->field_show_subtitle[LANGUAGE_NONE][0]['value'])) {
    if ($node->field_show_subtitle[LANGUAGE_NONE][0]['value'] == 1) {
        $nodeshowsubtitle = true;
    } else {
        $nodeshowsubtitle = false;
    }

} else {
    $nodeshowsubtitle = false;
}
// parallex subtitle end
// dpm ($nodeshowsubtitle);

if (isset($vars['field_captions_list'])) {
    $array_captions = array();
    foreach ($vars['field_captions_list'][LANGUAGE_NONE] as $key => $value) {
        //   dpm($value['target_id']);
        $n = node_load($value['target_id']);
        // dpm($n);

        if (isset($n->field_show_subtitle[LANGUAGE_NONE][0]['value'])) {
            if ($n->field_show_subtitle[LANGUAGE_NONE][0]['value'] == 1) {
                $showsubtitle = true;
            } else {
                $showsubtitle = false;
            }

        } else {
            $showsubtitle = false;
        }
        //  if(isset($n->field_sub_title_text[LANGUAGE_NONE][0]['value'])){$subtitle = $n->field_sub_title_text[LANGUAGE_NONE][0]['value'];}else {$subtitle = $n->title;};
        $c['title'] = $n->title;
        $c['body'] = $n->body[LANGUAGE_NONE][0]['value'];
        $c['showsubtitle'] = $showsubtitle;
        $array_captions[] = $c;
    }
}

?>
<style type="text/css">
    #node-<?php print $node->nid;?> {
    background: url(<?php echo $img_path; ?>) 50% 0 no-repeat fixed;height:400px;
    background-size: cover;
 margin: 0 auto;
   margin-bottom:60px;
}
</style>
   <h3></h3>
<div  id="node-<?php print $node->nid;?>" class="<?php print $classes;?>  clearfix"<?php print $attributes;?> >

    <?php if ($nodeshowsubtitle == 1) {?> <div class="parallex-subtitle"> <?php print $subtitle;?></div> <?php 
    }
    ;?>
<div class="parallex-background">
    <?php ?>


  <div class="parallex-wrapper" style="width:900px;height: 100%;">



<?php if (empty($array_captions)) {?>
    <div class="parallex-title">
        <?php // print render($content['field_category']);?>
        <h1><?php print $variables['title'];?></h1>
    </div>
     <div class="parallex-body">

        <?php print render($content['body']);?>
    </div>
  </div>
<?php } else {
    ?>
 <div class="mflexslider" style="width:100%;height:100%;"> <ul class="slides"><?php
    foreach ($array_captions as $key => $value) {
        $ctitle = $value['title'];
        $cbody = $value['body'];
        $showsubtitle = $value['showsubtitle'];
        // dpm($key);
        ?>
        <li  >
    <div style="display:block;vertical-align:middle;margin:auto;height:100%;display: table;">
        <div style="vertical-align: middle;margin: auto 0;display: table-cell;">
         <div class="parallex-body">

           <h4><?php print $cbody;?></h4>
     </div>
        <?php if ($showsubtitle == true) {?>
     <div class="parallex-title">

        <h2><?php print $ctitle;?><h2>
    </div>
        <?php }
    ;?>
     </div>
     </div>
        </li>
    <?php }
    ;?>
</ul></div>
<?php }
;?>


</div>

</div>
</div>
 <script type="text/javascript">
   (function($){
         $(document).ready(function(){
//$("#node-<?php print $node->nid;?>").parallax("50%", 0.1);

<?php if (empty($array_captions)) {?>
$("#node-<?php print $node->nid;?> .parallex-title").fitText(1, { minFontSize: "17px", maxFontSize: "45px" });
<?php } else {?>
    $("#node-<?php print $node->nid;?> .parallex-title").fitText(1, { minFontSize: "14px", maxFontSize: "25px" });
<?php }
;?>

parallex_height = $('#node-<?php print $node->nid;?>').height();

$('.mflexslider li').each(function() {$(this).height(parallex_height);})
//parallex_height

  $('#node-<?php print $node->nid;?> .mflexslider').flexslider({
    selector :  ".slides > li ",
   slideshow: true,
   slideshowSpeed: 7000,
    animation: "slide",
    animationLoop :true,
    controlNav:false,
    directionNav:false,
    useCSS: false,
    direction : "vertical"
  });


})

     })(jQuery);
</script>
