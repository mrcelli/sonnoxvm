<?php

$path = image_style_url('original', $content['field_image']['#items'][0]['uri']);

?>
<div ng-controller="supportCtr"  >

<div  class="first-section" style="position:relative;">
  <div class="banner-page-title center text-center color-white">
    <h1>SUPPORT</h1>
    <H3><?php $title = ($title == 'Support') ? 'Home' : $title;
    echo $title;?></H3>
  </div>
  <div   class=" clearfix"  >
    <img src="<?php echo $path; ?>" style="width:100%;"/>
  </div>
</div>


<div   class=" support  plug-in" >


<div   class="pre-submenu"  in-view-options="{offsetBottom: -25}"  in-view="$inview  && notinview($event, $inview)"  ></div>
<div id="sub-menu" class="sub-menu support archors topviewportchecker visible "  in-view-options="{offsetBottom: -50}" in-view=" inview($event, $inview)">
 <div class="container " style="padding-top:0px;">
     <div class="col-sm-12" >
          <div class=" " style="padding-top:0px;  text-align: left;" >
            <div class="hidden-xs  hidden-sm display-inline ">
                    <?php
                    $submenu = get_submenu_tree('main-menu', 'Support');
                    $selectlist = array();
                    $router_item = menu_get_item();
                    foreach ($submenu as $key => $listitem) {
                        //if($listitem['link']['title'] == 'Browse') {$submenu[$key]['link']['title'] = 'All';}
                        $selectlist[$key]['title'] = $listitem['link']['title'];
                        $selectlist[$key]['href'] = '/' . drupal_get_path_alias($listitem['link']['href']);
                        $selectlist[$key]['selected'] = $router_item['tab_root_href'] == $listitem['link']['href'] ? ' ng-selected="true" ' : '';
                        if ($selectlist[$key]['selected'] !== '') {$selected = "/" . drupal_get_path_alias($listitem['link']['href']);
                        }
                    }
                    ;

                    $submenu_rendered = menu_tree_output($submenu);
                    print render($submenu_rendered);

?>
            </div><!-- hidden-xs-->
              <div class="col-xs-12 hidden-md hidden-lg display-inline" >
                    <select class="form-control" name="page_select" ng-init="page_v = '<?php echo $selected; ?>'" ng-model="page_v" ng-change="page_change(page_v)"  >
                        <?php

                        // dpm($selectlist);
                        foreach ($selectlist as $item) {

                            echo '<option  value="' . $item['href'] . '"   ' . $item['selected'] . ' >' . $item['title'] . '</option>';
                        }
                        ;
?>
                     </select>
              </div>

            </div>
</div>
</div>

</div>


</div>


<div class="padding-100-100 background-grey-light ">
 <div class="container " >
   <div class="col-xs-12 text-center">
     <h2 class="font-sonnox font-size-xs-17px  "><?php echo $body[0]['value']; ?></h2>
   </div>

   <div class="col-xs-12 ">
    <div class="text-center margin-auto">
    <?php foreach ($entities as $key => $value) {?>
      <h3 class="display-inline-block cursor-pointer font-size-xs-16px  padding-1em onhover-background-white margin-bottom-0" ng-class="{'background-white' : show === <?php echo $key; ?>}" ng-click="show = <?php echo $key; ?>" >  <?php echo $value->field_sub_title_text[LANGUAGE_NONE][0]['value']; ?></h3>

    <?php }
;?>
 </div>
        <?php foreach ($entities as $key => $value) {?>
      <div ng-show="<?php echo $key; ?> === show" class="padding-top-lg padding-bottom-lg background-white">
        <div class="margin-auto text-alignt-left padding-05em " style="max-width:600px;" >
        <?php echo $value->body[LANGUAGE_NONE][0]['value']; //echo $value->[LANGUAGE_NONE][0]['value'];                              ?>
      </div>
      </div>
        <?php }
;?>
   </div>
 </div>
</div>


