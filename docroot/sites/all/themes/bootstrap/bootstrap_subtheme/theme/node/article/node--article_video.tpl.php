<?php
$vars = get_defined_vars();

?>
<div id="home-<?php print $node->nid; ?>" class="home-video" style="height: 760px;">
        <?php print render($content['field_article_video']); ?>
     </div>

<div class="page series">
<div class="sonnox-posts">
<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> series-node-body parallex clearfix"<?php print $attributes; ?>>


  <div class="content_panel">
       <h1><?php print $title; ?> </h1>
    <?php
    // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
    print render($content['field_tags']);
         print '<div class="text-container series-body" style="text-align:left;">'.render($content['body']).'</div>';
    ?>
  </div>

</article>
<?php
if ($display_submitted) :
    ;
?>
    <div class="submitted">

    <footer>
        <p>
            <?php print $submitted; ?>


        </p><p>
            Tags: <?php print render($content['field_tags']); ?>

       </p></footer>
<?php endif; ?>
</div>
</div>
</div>
