<?php

//$var = get_defined_vars();
//dpm($var);
//$items['id'] = $node->nid;
//$items['title'] = $node->title;
$items = array();
foreach ($field_system_requirements_conten as $key => $value) {
    $n = node_load($value['target_id']);
    // dpm($n);

    if ((isset($n->field_promote_to_submenu[LANGUAGE_NONE])) && ($n->field_promote_to_submenu[LANGUAGE_NONE][0]['value'] == 1)) {
        if (isset($n->field_sub_title_text[LANGUAGE_NONE][0]['value'])) {$subtitle = $n->field_sub_title_text[LANGUAGE_NONE][0]['value'];
        } else { $subtitle = $n->title;
        }
        ;
        if (isset($n->field_show_subtitle[LANGUAGE_NONE][0]['value'])) {
            if ($n->field_show_subtitle[LANGUAGE_NONE][0]['value'] == 1) {
                $showsubtitle = true;
            } else {
                $showsubtitle = false;
            }
        } else {
            $showsubtitle = false;
        }
        $items[$n->nid]['title'] = $subtitle;
        $items[$n->nid]['content'] = node_view($n, 'system_requirements');
    } else {
        $content_outside[$n->nid] = node_view($n, 'system_requirements');
    }
    ;
}
;

?>

<div class="text-container" >
<a type="" class="" data-toggle="modal" href="#myModal">
Minimum system requirements...
</a>
    </div>


<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php print $field_sub_title_text[LANGUAGE_NONE][0]['value'];?></h4>
      </div>
      <div class="modal-body text-left">
            <?php
            if (sizeof($items) > 1) {
                print theme('responsivetabs', array('title' => $node->title, 'id' => $node->nid, 'items' => $items));
            }
?>
            <?php

            foreach ($content_outside as $key => $value) {
                print render($value);
            }
?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

      </div>
    </div>
  </div>
</div>
