<?php

$vars = get_defined_vars();
$path = image_style_url('original', $field_image_top_banner[0]['uri']);

?>
<div id="products"  ng-controller="productsCtr" style="  " >
<div class="first-section" style="position:relative;">
  <div class="banner-page-title center text-center color-white">
    <h1>PRODUCTS</h1>

  </div>
  <div   class=" clearfix"  >
    <img src="<?php echo $path; ?>" style="width:100%;"/>
  </div>
</div>

<div   class=" plug-in"  >
<div class="pre-submenu"  in-view-options="{offsetBottom: -28}"  in-view="$inview  && notinview($event, $inview)"  ></div>
  <div id="sub-menu" class="sub-menu topviewportchecker  visible "  in-view-options="{offsetBottom: -30}" in-view="inview($event, $inview)">

     <div class="container " style="padding-top:0px;">
         <div class="col-sm-12 " >
              <div class=" " style="padding-top:0px;  text-align: left;" >
                <ul class="menu nav" style="">
                <li class="first leaf  " ng-class="{ 'active' : type == 'all'}" ng-click="type = 'all' "><div >All</div></li>
                <li class="leaf" ng-click="type = 'product_plug_in'"  ng-class="{ 'active' : type == 'product_plug_in'}"  ><div >Plug-ins</div></li>
                <li class="leaf" ng-click="type = 'product_bundle'" ng-class="{ 'active' : type == 'product_bundle'}" ><div   >Bundles</div></li>
                <!--li class="leaf" ng-click="type = 'product_custom_bundle' " ng-class="{ 'active' : type == 'product_custom_bundle'}" ><div  >Custom Bundles</div></li-->
                </ul>


              </div>

        </div>
    </div>
  </div>

  <div class="container">
    <div style="height:30px;"></div>
    <div class="col-md-9">
  <div class="display-block margin-bottom-md padding-right-1">
<div class=" flat-options margin-bottom-md " >
        <label class="flat-option line-height-08em " ng-model="templatetype" ng-class="{'active' : templatetype == 1}" btn-radio="1" ><i class="fa fa-list"></i></label>
        <label class="flat-option line-height-08em  " ng-model="templatetype" ng-class="{'active' : templatetype == 2}"  btn-radio="2" ><i class="fa fa-th-large"></i></label>

    </div>
                     <form class="navbar-form  margin-bottom-md">
                    <div class="input-group">
                      <input type="text" id="search-products" ng-model="search" value="" class="form-control" placeholder="Search Product...">
                      <span ng-class="{'opacity-1' : search.length > 0}" ng-click="search = ''" class="label"><span class="close-icon thick"></span></span>
                    </div><!-- /input-group -->
                  </form>
                </div>

    <products-list template-url="display" ></products-list>
    </div>
    <?php

    $block = views_embed_view('promotion_tiles', 'block');

?>
    <div class="col-md-3">
      <div id="products-right-column" class="products-right-column  promotion-visible"  >
            <?php print $block;?>
      </div>
    </div>

  </div>
</div>
</div>
