<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
 //dpm($content);
$vars =  get_defined_vars();


 //$grants = nap_get_grants($node->nid);
 //   dpm($grants);
    $lnode = node_load($node->nid);
  //  dpm($node);
    $has_grant = node_access('view', $lnode);
  // dpm($has_grant);
if($has_grant) {
    //  dpm('has grant');
    //$node_view = node_view(node_load($node_nid), 'teaser');
    // print render($node_view);

}else {
    //    dpm('has not  grant');
    // $node_view = node_view(node_load($nid), 'teaser');
    // print render($node_view);

};

//dpm($field_contest_image);
$image_uri = file_build_uri($uc_product_image[0]['filename']);
//$path = image_style_path('default', $filename);
//$image_style_url = image_style_url('original', $image_uri);
$style          = 'original';
$derivative_uri = image_style_path($style, $image_uri);
$success        = file_exists($derivative_uri) || image_style_create_derivative($style, $image_uri, $derivative_uri);
$image_style_url  = file_create_url($derivative_uri);;
//$image_style_url = theme('image_style', array('path' => $derivative_uri, 'style_name' => 'original', 'width' => '0' , 'height' => '0', 'test' => 1));
//$image = theme('image_style', array('style_name' => 'original', 'path' => $image_style_url));

//$file =   file_load ( $url );
//$contest_image_url = $image ;//file_create_url();
//dpm($image_style_url);

unset($uc_product_image);
unset($content['uc_product_image']);
?>
<div id="home-<?php print $node->nid; ?>" class="home-parallax " style="height: 760px;background: url(<?php echo $image_style_url;?>) fixed;background-size: cover;
background-position: center top;">
      <div class="home-text-wrapper series-parallex-text-wrapper">

         <div class="series-parallex-text">
        <?php  // print render($content['field_category']);?>
        <h1><?php print $title; ?> </h2>
    </div>





        </div><!-- END HOME TEXT WRAPPER -->
     </div>

<div class="page series product-display-default">
<div class="sonnox-posts">
<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> series-node-body parallex clearfix"<?php print $attributes; ?>>








  <div class="content_panel">
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      hide($content['field_tags']);
    if (user_is_logged_in()) {
        $like_link =  flag_create_link('bookmarks', $node->nid);
        // print  "<div class='flag-area-bookmarks-wrapper'>".$like_link."</div></div>";
    }
    //   print render($content);
         print '<div class="text-container series-body" style="text-align:left;">'.render($content).'</div>';



     // $like_number = "<div class='flag-like-number'><i class='fa fa-thumbs-o-up'></i>".$count."</div>";
      //$like_link = "";
    if (user_is_logged_in()) {
        $like_link =  flag_create_link('favorites', $node->nid);
        //    print "<div class='flag-area'>".$like_link."</div>";
    }
    ?>
  </div>

    <?php //print render($content['links']); ?>

    <?php //print render($content['comments']); ?>

</article>

</div>
</div>



</div>
