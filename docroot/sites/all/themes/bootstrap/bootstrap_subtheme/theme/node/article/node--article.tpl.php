<?php

$vars = get_defined_vars();

$image_uri = file_build_uri($content['field_image']['#items'][0]['filename']);

$style = 'original';
$derivative_uri = image_style_path($style, $image_uri);
$path = image_style_url('original', $content['field_image']['#items'][0]['uri']);
$success = file_exists($derivative_uri) || image_style_create_derivative($style, $image_uri, $derivative_uri);
$image_style_url = $path; //file_create_url($derivative_uri);
//dpm($node->nid);
//$image_style_url = '';
//print render($content['field_image']);
$bx_rendered = views_embed_view('video_slideshow_of_plugin_page', 'block', $node->nid);
$bx2_rendered = views_embed_view('video_slideshow_of_plugin_page', 'block_1', $node->nid);
hide($content['field_image']);
?>


<div id="home-<?php print $node->nid;?>" class="home-parallax" style="height: 760px;background: url(<?php echo $image_style_url; ?>) fixed;background-size: cover;
background-position: center top;">
      <div class="home-text-wrapper series-parallex-text-wrapper">
  <div class="parallex-background">
       <div class="parallex-wrapper">
         <div class="series-parallex-text">
        <?php // print render($content['field_category']);?>
        <h1><?php print $title;?> </h1>
    </div>
        </div><!-- END HOME TEXT WRAPPER -->
     </div>
      </div>
      </div>

<div class="page ">
<div class="sonnox-posts">

<article id="node-<?php print $node->nid;?>" class="<?php print $classes;?> series-node-body  clearfix"<?php print $attributes;?>>


  <div class="content_panel">
    <?php
    // We hide the comments and links now so that we can render them later.
    hide($content['comments']);
    hide($content['links']);
?><div class="text-container">
        <?php
        print render($content['field_category']);
        print render($content['body']);
        // /  render($content['body']);
?></div>
        <?php
        print render($content);

?>
  </div>

    <?php //print render($content['links']); ?>

    <?php //print render($content['comments']); ?>

</article>

</div>

