
<div data-ng-controller="pluginCtr"  class="plugin">
  <div id="plugin-topsection" class=" first-section position-relative banner" style="overflow:hidden;">
    <div class="plugin-cover full" >

     <div class="hidden-xs background-image position-absolute  full " style="max-width:100%;background-image:url(<?php echo $banner_url[0]; ?>);" ></div>
     <div  class="background-image position-absolute full hidden-sm hidden-md hidden-lg"  style="max-width:100%;background-image:url(<?php echo $banner_url[1]; ?>);" ></div>
<div class="container position-relative full-height" >
      <div class="awards-icons hidden-xs" >
        <?php
        foreach ($awards as $award) {
            print '<div class="background-image awards-div" style="background-image:url('.$award['logo'].');'.'width:'.$award['size']['width'].'px;" ></div>';

        };
?>
     </div>

     <div class="article-plugin-title  center "   style="position:absolute;width:100%;color:white;z-index:50;">
      <div class="col-xs-12 text-center" style="position:relative;">
        <h1 style="color:white;"><?php print strtoupper($title); ?> </h1>
        <div class="col-sm-12 hidden-xs">
          <div class="col-sm-6 text-right">
            <div class="ghost-transparent-lg hidden-xs " >
              <div class="try-green-button" data-ng-click="goto('buy')">BUY</div>

              <div class="try-red-button" data-ng-click="goto('demos')">TRY</div>
            </div>
          </div>


          <div class="col-sm-6 text-left">
            <div class="mnext ghost-white-lg " data-ng-click="showvideos = true;"><i  class="fa fa-chevron-right display-inline"></i>&nbsp;WATCH VIDEOS</div>
          </div>
        </div>
      </div>

      <div class="col-xs-12 text-center hidden-sm hidden-md hidden-lg">
       <div class="ghost-white-lg "  data-ng-click="showvideos = true;">

          <i class="fa fa-chevron-right display-inline"></i>&nbsp;WATCH VIDEOS

      </div>
    </div>
  </div>
        </div>
</div>
<div data-ng-class="{'video-toright': showvideos === true }" data-in-view=" videonotinview($event, $inview)" id="videos-wrapper"  class="background-black  plugin-videos  position-absolute text-center margin-auto full" >
 <div class="position-relative container text-center margin-auto full-height"  style="max-height: 100%;">



   <div class="vertical-center position-absolute video-back"   data-ng-click="videosleft()" ><i class="fa fa-chevron-left"></i></div>
   <input id="playvideo"  type="button" class="position-absolute" value="Play" onclick="window.youtubePlayer.playVideo();"></input>
  <div class="video-left z-index-10  full-height" id="video-left" style="">

    <!--iframe id="videoiframe" class="fluid-iframe"  style="height:100%;width:100%;" data-ng-src="{{videourl | trustAsResourceUrl}}" frameborder="0" allowfullscreen></iframe-->
    <youtube-video video-id="videourl" player-width="'100%'" player-height="'100%'" player="youtubePlayer" ></youtube-video>
  </div>
  <div class="video-right text-left position-relative z-index0" >
    <bxslidervideos-drv></bxslidervideos-drv>
    <div id="slider-prev" data-ng-click="gotToPrevSlide();"><div style="position:relative;"><i class="fa fa-chevron-up"><a class="bx-prev" href=""></a></i></div></div>
    <div id="slider-next" data-ng-click="goToNextSlide();"><div style="position:relative;"><i class="fa fa-chevron-down"><a class="bx-next" href=""></a></i></div></div>
  </div>
</div>


</div>
</div>
<div class="position-relative" style="width:100%;height:0px;">
<div id="mobile-close-video" class="mobile-close-video" data-ng-click="closevideomobile()">SHOW VIDEOS LIST</div>
  </div>

<div   class="pre-submenu"  in-view-options="{offsetBottom: -25}"   data-in-view="$inview  && notinview($event, $inview)"  ></div>
<div id="sub-menu" class="sub-menu   background-grey-soft  sub-menu-plugin-page topviewportchecker  visible" data-in-view-options="{offsetBottom: -110}" data-in-view=" inview($event, $inview)"  >
    <?php print $submenu; ?>
</div>

<div class="plug-in article-plugin" style=" background:white;">


  <div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>  container-full"<?php print $attributes; ?> style="paddidata-ng-top: 30px;">



    <?php
    foreach ($entities as $entity) {
        print $entity;
    }

?>


</div>
</div>
</div>

