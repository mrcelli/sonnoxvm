<?php
  $path = image_style_url('original', $field_image_top_banner[0]['uri']);

?>
  <div >
    <div  class="first-section" style="position:relative;">
      <div class="banner-page-title center text-center color-white">
        <h1>ABOUT</h1>
        <H3><?php echo $title; ?></H3>
      </div>
        <div   class=" clearfix"  >
          <img src="<?php echo $path; ?>" style="width:100%;"/>
        </div>
    </div>
    <div   class=" support  plug-in" >
      <div  class="pre-submenu"  in-view-options="{offsetBottom: -28}"  in-view="$inview  && notinview($event, $inview)"  ></div>
        <div id="sub-menu" class="sub-menu support archors topviewportchecker visible background-grey "  in-view-options="{offsetBottom: -30}" in-view=" inview($event, $inview)" >
         <div class="container " style="padding-top:0px;">
           <div class="col-sm-12" >
            <div class=" " style="padding-top:0px;  text-align: left;" >
              <div class="hidden-xs  hidden-sm display-inline ">
                <?php
                $submenu     = get_submenu_tree('main-menu', 'About');
                $selectlist  = array();
                $router_item = menu_get_item();
                foreach ($submenu as $key => $listitem) {
                    // if($listitem['link']['title'] == 'Browse') {$submenu[$key]['link']['title'] = 'All';}
                    $selectlist[$key]['title']    = $listitem['link']['title'];
                    $selectlist[$key]['href']     = '/'.drupal_get_path_alias($listitem['link']['href']);
                    $selectlist[$key]['selected'] = $router_item['tab_root_href'] == $listitem['link']['href'] ? ' ng-selected="true" ' : '';
                    if ($selectlist[$key]['selected'] !== '') {
                        $selected = '/'.drupal_get_path_alias($listitem['link']['href']);
                    }
                };

                $submenu_rendered = menu_tree_output($submenu);
                print render($submenu_rendered);
?>
              </div><!-- hidden-xs-->
              <div class="col-xs-12  hidden-md hidden-lg display-inline" >
                <select class="form-control" name="page_select" ng-init="page_value = '<?php echo $selected; ?>'" ng-model="page_value" ng-change="page_change(page_value)"  >
                    <?php
                    // dpm($selectlist);
                    foreach ($selectlist as $sitem) {
                        echo '<option  value="'.$sitem['href'].'"   '.$sitem['selected'].' >'.$sitem['title'].'</option>';
                    };
?>
              </select>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div class=" about about-partners background-white" >
    <div class="container">
     <div class="col-xs-12 col-sm-9 col-sm-offset-1 col-md-8 col-md-offset-2 margin-top-lg margin-bottom-md padding-2em" >



        <?php echo $body[0]['summary']; ?>

    </div>
  </div>
  <div class="background-image padding-100-100" style="min-height:450px;background-image:url(<?php echo image_style_url('original', $field_image_top_banner[1]['uri']); ?>);" ></div>
  <div class="container">
   <div class="col-xs-12 col-sm-9 col-sm-offset-1 col-md-8 col-md-offset-2 margin-top-md margin-bottom-md background-white padding-2em" style="margin-top:-200px;">


        <?php echo $body[0]['value']; ?>

  </div>
</div>
</div>
</div>

