<?php

if (isset($node->field_sub_title_text[LANGUAGE_NONE][0]['value'])) {$subtitle = $node->field_sub_title_text[LANGUAGE_NONE][0]['value'];
} else { $subtitle = $node->title;
}
;

?>

<div in-view-options="{offsetBottom: '0'}" ng-class="{'fadeInDown' : $inview == true  }"   in-view="$inview & cpinview  = $inview" iid="node-<?php echo $nid; ?>" class="text-container">
     <h3><?php print $subtitle;?></h3>
    <?php print render($content);
?></div>
