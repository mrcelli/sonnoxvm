<?php
$path = null;
if (isset($field_image_top_banner[0]['uri'])) {
    $path = image_style_url('original', $field_image_top_banner[0]['uri']);
}
;

?>
<?php if (isset($body[0]['value'])) {?>
<div  >
    <?php if (isset($field_image_top_banner[0]['uri'])) {?>
        <div  style="position:relative;">
            <div class="banner-page-title center text-center color-white">
                <h1><?php echo $title; ?></h1>
            </div>
            <div   class=" clearfix"  >
                <img src="<?php echo $path; ?>" style="width:100%;"/>
            </div>
        </div>

    <?php }
    ;?>

    <div class="container-fluid background-white">
        <div class="col-xs-12 col-sm-9 col-sm-offset-1 col-md-8 col-md-offset-2 margin-top-lg margin-bottom-md padding-2em" >
            <?php if (!isset($field_image_top_banner[0]['uri'])) {?>
                <h1><?php echo $title; ?></h1>
            <?php }
            ;?>
            <?php echo $body[0]['value']; ?>

        </div>
    </div>
</div>

<?php }
;?>