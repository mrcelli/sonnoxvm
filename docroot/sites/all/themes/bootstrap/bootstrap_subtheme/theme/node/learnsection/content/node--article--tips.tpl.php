<?php

$vars = get_defined_vars();
//dpm($vars);

//$path = image_style_url('original', $field_image_top_banner[0]['uri']);
?>
 <div  class="first-section " ></div>
<div   class=" learn learn-tips plug-in" ng-controller="learnPagetips"  >
<div   class="pre-submenu"  in-view-options="{offsetBottom: -50}"  in-view="$inview  && notinview($event, $inview)"  ></div>
  <div id="sub-menu" class="sub-menu learn-tips  archors topviewportchecker visible  background-grey-soft"  in-view-options="{offsetBottom: -28}" in-view=" inview($event, $inview)" >
   <div class="container " style="padding-top:0px;">

     <div class="col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0 " >
      <div class=" " style="padding-top:0px;  text-align: left;" >
        <div class="hidden-xs hidden-sm display-inline ">
            <?php
            $submenu = get_submenu_tree('main-menu', 'Learn');
            $selectlist = array();
            $router_item = menu_get_item();

            foreach ($submenu as $key => $listitem) {
                if ($listitem['link']['title'] == 'Browse') {$submenu[$key]['link']['title'] = 'All';
                }
                $selectlist[$key]['title'] = $listitem['link']['title'];
                $selectlist[$key]['href'] = '/' . drupal_get_path_alias($listitem['link']['href']);
                $selectlist[$key]['selected'] = $router_item['tab_root_href'] == $listitem['link']['href'] ? ' ng-selected="true" ' : '';
            }
            ;
            $submenu_rendered = menu_tree_output($submenu);
            print render($submenu_rendered);
?>
        </div><!-- hidden-xs-->
        <div class="col-xs-6 sumbmenu-select  hidden-xs  hidden-md hidden-lg display-inline" >
          <select class="form-control" ng-model="page_value" ng-change="page_change()"  style="max-width:270px;">
            <option value="">Learn sections</option>
            <?php
            foreach ($selectlist as $sitem) {
                echo '<option  value="' . $sitem['href'] . '"   ' . $sitem['selected'] . ' >' . $sitem['title'] . '</option>';
            }
            ;
?>
          </select>
        </div>
        <div class="display-inline float-right responsive-search-wrapper" ng-controller="searchBoxCtr">

         <form class="form-wrapper cf" novalidate="novalidate" action="/learn/tips" method="get">
          <input type="text" name="search"  class="form-control"  placeholder="Search here..."   required>
          <button type="submit" >
           <i ng-class="{'display-none ' : loading== true}"  class="fa fa-search"></i>
           <i  ng-class="{'display-none ' : loading!= true}" class="fa fa-spinner fa-pulse"></i>
         </button>
       </form>
     </div>

   </div>
 </div>
</div>
</div>

<div class="background-white padding-top-lg padding-bottom-lg " style="width:100%;">
<div class="container   " >
  <div class="col-xs-12 col-xs-offset-0 col-sm-10 col-sm-offset-1">
   <div class="col-xs-12 col-sm-12 col-md-8  background-white no-side-padding">
    <div class="col-sm-12 col-sm-offset-0">
      <div class="col-sm-12 col-sm-offset-0 padding-side-05  padding-bottom-lg ">
       <h1 > <?php print $title;?></h1>
        <?php print $body[0]['safe_value']?>
     </div>
   </div>

 </div>

 <div class="col-xs-12  col-sm-12 col-md-3 col-md-offset-1   padding-gutter " >
  <div class="col-xs-10 col-xs-offset-1 no-padding " >
    <?php

    $block = views_embed_view('promotion_tiles', 'block');
    print $block;
?>
  </div>
  <div ng-controller="tipsrelatedCtr"  ng-show="related.length" class="col-xs-10  text-center margin-top-md padding-bottom-md col-xs-offset-1 background-white">

   <h3 class="margin-bottom-md"> Related Products </h3>
   <artistrelated/>
 </div>
 <div ng-controller="tipsCtr" class="col-xs-10 text-center margin-top-md col-xs-offset-1 background-white">
   <h3 class="margin-bottom-md"> Other Tips </h3>
   <tipshortlist/>
 </div>

</div>
</div>
</div>
  </div>
    </div>

