<?php

$vars = get_defined_vars();
$var = get_defined_vars();

$path = image_style_url('original', $field_image_top_banner[0]['uri']);
$search = isset($_GET['search']) ? check_plain($_GET['search']) : '';
?>


<div   class="first-section" style="position:relative;">
  <div class="banner-page-title center text-center color-white">
    <h1>LEARN</h1>
    <H3><?php echo $title; ?></H3>
  </div>
  <div   class=" clearfix"  >
    <img src="<?php echo $path; ?>" style="width:100%;"/>
  </div>
</div>


<div   class=" learn learn-spotlight " ng-controller="learnPagespotlight"  >


<div   class="pre-submenu"  in-view-options="{offsetBottom: -28}"   in-view="$inview  && notinview($event, $inview)"  ></div>
      <div id="sub-menu" class="sub-menu learn-spotlight  archors topviewportchecker visible "  in-view-options="{offsetBottom: -30}" in-view=" inview($event, $inview)">
       <div class="container " style="padding-top:0px;">
           <div class="col-sm-12" >
                <div class=" " style="padding-top:0px;  text-align: left;" >
                  <div class="hidden-xs hidden-sm display-inline ">
                        <?php
                        $submenu = get_submenu_tree('main-menu', 'Learn');
                        $selectlist = array();
                        $router_item = menu_get_item();

                        foreach ($submenu as $key => $listitem) {
                            if ($listitem['link']['title'] == 'Browse') {$submenu[$key]['link']['title'] = 'All';
                            }
                            $selectlist[$key]['title'] = $listitem['link']['title'];
                            $selectlist[$key]['href'] = '/' . drupal_get_path_alias($listitem['link']['href']);
                            $selectlist[$key]['selected'] = $router_item['tab_root_href'] == $listitem['link']['href'] ? ' ng-selected="true" ' : '';

                        }
                        ;
                        $submenu_rendered = menu_tree_output($submenu);
                        print render($submenu_rendered);
                        // dpm($submenu_rendered);

?>
                  </div><!-- hidden-xs-->
                    <div class=" hidden-md  hidden-xs  hidden-lg display-inline" >
                          <select class="form-control" ng-model="page_value" ng-change="page_change()"  >
                            <?php

                            // dpm($selectlist);
                            foreach ($selectlist as $sitem) {

                                echo '<option  value="' . $sitem['href'] . '"   ' . $sitem['selected'] . ' >' . $sitem['title'] . '</option>';
                            }
                            ;
?>
                           </select>
                    </div>
                    <div class="display-inline float-right responsive-search-wrapper" ng-controller="searchBoxspotlightCtr">
                     <form class="form-wrapper cf  " novalidate="novalidate">
                        <input type="text" ng-model="q" name="search"  class="form-control"  value="test" ng-init="q='<?php echo $search ?>'" placeholder="Search here..." required>
                        <button type="submit" ng-click="search()">
                           <i ng-class="{'display-none ' : loading== true}"  class="fa fa-search"></i>
                            <i  ng-class="{'display-none ' : loading!= true}" class="fa fa-spinner fa-pulse"></i>
                        </button>
                    </form>
                    </div>
                  </div>
      </div>
      </div>

              </div>


      </div>



 <div class="container learn learn-spotlight" ng-controller="spotlighttilesListCtr">
   <div class="col-xs-12">
       <div class="display-block " >


    <filterlistspotlight/>
    </div>

    <spotlightlist/>

 </div>

