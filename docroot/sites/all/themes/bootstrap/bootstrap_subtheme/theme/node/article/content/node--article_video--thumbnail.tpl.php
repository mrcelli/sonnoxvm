<?php


$vars = get_defined_vars();
   // dpm($vars);
$image_path = file_create_url($vars['field_video_thumbnail'][0]['uri']);

?>
<div class="video-thumbnail">
<div class="video-thumbnail-left">
 <img typeof="foaf:Image" src="<?php echo $image_path; ?>" alt="">
</div>
<div class="video-thumbnail-right">
 <div class="title"><?php echo $vars['variables']['title']; ?></div>
   <div class="description"></div>
</div>
 </div>

