<div class="padding-100-100 margin-top-0 margin-bottom-0 background-grey-soft text-center" style="min-height:500px;">
<div class="container"  style="font-size:1.5em;line-height: 1.5em;">
<article id="node-<?php print $node->nid;?>" class="<?php print $classes;?> clearfix"<?php print $attributes;?>>

<?php
// Hide comments, tags, and links now so that we can render them later.
hide($content['comments']);
hide($content['links']);
hide($content['field_tags']);
//print render($content);
$path = current_path();
?>
<?php echo $body[0]['value']; ?>
  <div class="margin-1em padding-1em color-red"><?php echo $path; ?></div>
</article>
  </div>
  </div>
