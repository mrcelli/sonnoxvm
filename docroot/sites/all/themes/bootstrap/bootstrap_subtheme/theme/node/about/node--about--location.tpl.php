<?php
$path = image_style_url('original', $field_image_top_banner[0]['uri']);
//dpm($body);
?>
<div ng-controller="locationCtr"  >

  <div class="first-section"  style="position:relative;">
    <div class="banner-page-title center text-center color-white">
      <h1>ABOUT</h1>
      <H3><?php echo $title; ?></H3>
    </div>
    <div   class=" clearfix"  >
      <img src="<?php echo $path; ?>" style="width:100%;"/>
    </div>
  </div>
  <div   class=" support  plug-in" >
    <div   class="pre-submenu"  in-view-options="{offsetBottom: -28}"  in-view="$inview  && notinview($event, $inview)"  ></div>
    <div id="sub-menu" class="sub-menu support archors topviewportchecker visible "  in-view-options="{offsetBottom: -30}" in-view=" inview($event, $inview)" >
     <div class="container " style="padding-top:0px;">
       <div class="col-sm-12" >
        <div class=" " style="padding-top:0px;  text-align: left;" >
          <div class="hidden-xs   hidden-sm  display-inline ">
            <?php
            $submenu = get_submenu_tree('main-menu', 'About');
            $selectlist = array();
            $router_item = menu_get_item();
            foreach ($submenu as $key => $listitem) {
                //if($listitem['link']['title'] == 'Browse') {$submenu[$key]['link']['title'] = 'All';}
                $selectlist[$key]['title'] = $listitem['link']['title'];
                $selectlist[$key]['href'] = '/' . drupal_get_path_alias($listitem['link']['href']);
                $selectlist[$key]['selected'] = $router_item['tab_root_href'] == $listitem['link']['href'] ? ' ng-selected="true" ' : '';
                if ($selectlist[$key]['selected'] !== '') {$selected = "/" . drupal_get_path_alias($listitem['link']['href']);
                }
            }
            ;

            $submenu_rendered = menu_tree_output($submenu);
            print render($submenu_rendered);
            // dpm($submenu_rendered);

?>
          </div><!-- hidden-xs-->
          <div class="col-xs-12  hidden-md hidden-lg display-inline" >
           <select class="form-control" name="page_select" ng-init="page_value = '<?php echo $selected; ?>'" ng-model="page_value" ng-change="page_change(page_value)"  >
                <?php

                // dpm($selectlist);
                foreach ($selectlist as $sitem) {

                    echo '<option  value="' . $sitem['href'] . '"   ' . $sitem['selected'] . ' >' . $sitem['title'] . '</option>';
                }
                ;
?>
          </select>
        </div>
      </div>
    </div>
  </div>
</div>
</div>


<div class=" about about-location  position-relative" >
  <div class="background-cover padding-bottom-lg" style="background-image: url(<?php echo image_style_url('original', $field_image_top_banner[2]['uri']); ?>);">

   <div class=" padding-top-lg  margin-bottom-lg padding-bottom-lg">
    <div class="container">
    <div class="col-xs-12 text-center padding-top-lg padding-bottom-md">
         <div class="margin-top-lg ">
           <h4><?php echo $body[0]['value']; ?></h4>
         </div>
       </div>
     <div class="width-100 text-center display-inline-block padding-bottom-md" >
      <img class="max-width-100  display-inline-block" style="max-height:353px;vertical-align: top;border:2px solid white;" src="<?php echo image_style_url('original', $field_image_top_banner[1]['uri']); ?>">

      <div class="display-inline-block">
        <div class="" style="
        vertical-align: top;
        text-align: center;
        padding: 2em;
        max-width: 600px;
        margin: auto;
        margin-left: 0px;
        ">
        <div class="display-inline-block text-alignt-left font-size-2em" style="font-size: 30px;
        padding: em;
        height: 100%;
        vertical-align: top;
        padding: 0.5em"><i class="fa fa-map-marker"></i></div>
        <div class="display-inline-block text-alignt-left padding-1em padding-left-0 text-left" style="font-size:20px;">Sonnox Ltd.
          <br>1 Manor Barns,
          <br>Finstock,
          <br>Oxfordshire.
          <br>OX7 3DG
          <br>UK
          <br>
        </div>
      </div>
    </div>
  </div>

</div>
</div>
</div>

<location-drv/>
</div>
</div>

