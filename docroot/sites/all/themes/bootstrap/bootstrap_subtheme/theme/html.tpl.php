<!DOCTYPE html>
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="<?php print $language->language;?>" dir="<?php print $language->dir;?>"<?php print $rdf_namespaces;?> data-ng-app="sonnox"  ng-controller="pagectr"  xmlns:ng="http://angularjs.org" >
<!--<![endif]-->
<!--[if lte IE 9]>
<html class="old-ie" lang="<?php print $language->language;?>" dir="<?php print $language->dir;?>" <?php print $rdf_namespaces;?> data-ng-app="sonnox"  ng-controller="pagectr"  xmlns:ng="http://angularjs.org"  >
<![endif]-->
<head profile="<?php print $grddl_profile;?>">
 <meta http-equiv="X-UA-Compatible" content="IE=9;IE=10;IE=Edge,chrome=1"/>
<!--script type="text/javascript" src="https://getfirebug.com/firebug-lite.js"></script-->
  <meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no"/>
    <?php print $head;?>
  <title><?php print $head_title;?></title>
    <?php print $styles;?>
  <!-- HTML5 element support for IE6-8 -->
  <!--[if lte IE 9]>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
    <?php print $scripts;?>
</head>
<body class="<?php print $classes;?> images-loaded" <?php print $attributes;?>  images-loaded-events="imgLoadedEvents">
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content');?></a>
  </div>
    <?php print $page_top;?>
    <?php print $page;?>
    <?php print $page_bottom;?>
</body>
<!--[if lte IE 9]>
    <script src="web/js/jquery.placeholder.min.js"></script>
    <script type="text/javascript">
    $('input, textarea').placeholder();
    </script>
<![endif]-->
</html>
