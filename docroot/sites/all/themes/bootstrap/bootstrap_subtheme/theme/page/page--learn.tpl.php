<?php

drupal_add_js('sites/all/libraries/transit/jquery.transit.min.js');
$block = views_embed_view('aritcles_teaser_list_learn_section', 'aritcles_teaser_list_learn_section-block_all');
print render($block);

?>
<div id="site-container"  class="images-loaded" images-loaded-events="imgLoadedEvents">
<div class="page-overlay" >
<div class="center"><i class="fa color-white fa-4 fa-circle-o-notch fix fa-spin"></i></div>
<div class="center"><i class="fa color-white fa-2 fa-circle-o-notch fix fa-spin-reverse"></i></div>
<div class="center"><i class="fa color-white fa-1 fa-circle-o-notch fix fa-spin"></i></div>
</div>



<div class="nav-container global-nav" id="nav-container">
    <header role="banner" id="page-header" class=" ">

    <?php if (!empty($site_slogan)) : ?>
      <p class="lead"><?php print $site_slogan;?></p>
    <?php endif;?>

    <?php print render($page['header']);?>
  </header> <!-- /#page-header -->
</div><!-- /#nav-container -->

<div class="outer-page-container">
    <?php if ($page['homepage_slider']['#empty'] == "false") : ?>
  <div id="homepage-wrapper">
   <div id="homepage-slider">

        <?php print render($page['homepage_slider']);?>

    </div>

  <div class="section-arrow" style="opacity: 0.95; " ><div class=" icon fa fa-arrow-circle-down"></div></div>
   <!--div id="homepage-content-top">
        <?php if (!empty($page['homepage-content-top'])) : ?>
        <?php print render($page['homepage-content-top']);?>
        <?php endif;?>
   </div>
   <div id="homepage-content-bottom">
        <?php if (!empty($page['homepage-content-bottom'])) : ?>
        <?php print render($page['homepage-content-bottom']);?>
        <?php endif;?>
   </div-->
 </div> <!--homepage-wrapper -->
    <?php endif;?>

<div class="main-container full-container ">
  <div class="row">

    <?php /* region--sidebar.tpl.php */?>
    <?php if ($page['sidebar_first']) : ?>

        <?php print render($page['sidebar_first']);?>
    <?php endif;?>

    <?php /* region--content.tpl.php */?>
    <?php print render($page['content']);?>

<div class="container ">
    <?php /* region--sidebar.tpl.php */?>
    <?php if ($page['sidebar_second']) : ?>

        <?php print render($page['sidebar_second']);?>
    <?php endif;?>
 </div>
  </div>
</div>
<?php /* region--footer.tpl.php */?>
<footer class="footer full-container" style="min-height:300px;">
          <div class="col-sm-12 "  style="min-height:300px;"> <?php print render($page['footer-1']);?></div>
          <!--div class="col-sm-4 background-lightblue col-footer"  style="min-height:300px;"><?php print render($page['footer-2']);?></div>
          <div class="col-sm-4 background-darkblue col-footer"  style="min-height:300px;"><?php print render($page['footer-3']);?></div-->

 <div class='copyright'> © 2007 - 2014 Sonnox</div>
</footer>

 </div><!-- outerpage container  -->
 </div> <!-- site container  -->




