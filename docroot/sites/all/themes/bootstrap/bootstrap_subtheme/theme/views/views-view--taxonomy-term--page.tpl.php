<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
 $vars = get_defined_vars();
 //dpm($vars);
 $view = views_get_current_view();
// dpm($view->args[0]);
$tterm = taxonomy_term_load($view->args[0]);
 $ttid = $tterm->tid;
 //dpm($tterm);
 $btags = false;
if($tterm->vocabulary_machine_name == 'tags' ) {$btags=true;
};
         $url =  drupal_lookup_path('alias', 'taxonomy/term/'.$ttid);
     //    dpm($url);
 // term icon section
 
 
  $term_object =  taxonomy_term_load($ttid);
             // dpm($term_object);
              $term_icon = field_view_field('taxonomy_term', $term_object, 'field_category_icon');
              $term_icon['#label_display'] = 'hidden';
             //  dpm($term_icon);
              $term_icon_output = render($term_icon);
              $term_icon_output = '<a href="'.$url.'" class="title-icon" ><div class="term-icon">'.$term_icon_output.'</div></a>';
if($btags== false) {
       $class_tag = " vocabulary ";
    $tag_char = "";
} else {  $class_tag = " tag " ; $tag_char = "#";
};
     
 $ttitle = '<div class="page taxonomy filter bigcategory category-'.strtolower($tterm->name).$class_tag.'  "><div class="category-title big">'.$tag_char.$term_icon_output.l($tterm->name, $url).'</div></div>';
 
  $v_category = taxonomy_vocabulary_machine_name_load('category');
      
        $taxonomy_titles_content = '<div class="page category '.$class_tag.'">';
if ($terms = taxonomy_get_tree($v_category->vid)) {
            
    foreach ($terms as $term) {
        // Do something with $term->tid or $term->name
   
        $tid = $term->tid;
        if((($tid!=$ttid)&&($btags == false)) || (($btags == true))) {
            $term_object =  taxonomy_term_load($tid);
             // dpm($term_object);
              $term_icon = field_view_field('taxonomy_term', $term_object, 'field_category_icon');
              $term_icon['#label_display'] = 'hidden';
             //  dpm($term_icon);
              $term_icon_output = render($term_icon);
              $url =  drupal_lookup_path('alias', 'taxonomy/term/'.$tid);
               $term_icon_output = '<a href="'.$url.'" class="title-icon" ><div class="term-icon">'.$term_icon_output.'</div></a>';

            $taxonomy_titles_content .= '<div class="category-box taxonomy category-'.strtolower($term->name).'" ><div class="category-title">'.l($term->name, $url).$term_icon_output.'</div></div>';
         
        }
    }
}
        $taxonomy_titles_content .= '</div>';
?>
<div class="<?php print $classes; ?>">
    <?php print render($title_prefix); ?>
    <?php if ($title) : ?>
    <?php print $title; ?>
    <?php endif; ?>
    <?php print render($title_suffix); ?>
    <?php if ($header) : ?>
    <div class="view-header">
        <?php print $header; ?>
    </div>
    <?php endif; ?>
  
    <?php print $taxonomy_titles_content;?>
    <?php print $ttitle;?>
    <?php if ($exposed) : ?>
    <div class="view-filters">
        <?php print $exposed; ?>
    </div>
    <?php endif; ?>

    <?php if ($attachment_before) : ?>
    <div class="attachment attachment-before">
        <?php print $attachment_before; ?>
    </div>
    <?php endif; ?>

    <?php if ($rows) : ?>
    <div class="view-content">
        <?php print $rows; ?>
    </div>
    <?php elseif ($empty) : ?>
    <div class="view-empty">
        <?php print $empty; ?>
    </div>
    <?php endif; ?>

    <?php if ($pager) : ?>
    <?php print $pager; ?>
    <?php endif; ?>

    <?php if ($attachment_after) : ?>
    <div class="attachment attachment-after">
        <?php print $attachment_after; ?>
    </div>
    <?php endif; ?>

    <?php if ($more) : ?>
    <?php print $more; ?>
    <?php endif; ?>

    <?php if ($footer) : ?>
    <div class="view-footer">
        <?php print $footer; ?>
    </div>
    <?php endif; ?>

    <?php if ($feed_icon) : ?>
    <div class="feed-icon">
        <?php print $feed_icon; ?>
    </div>
    <?php endif; ?>

</div><?php /* class view */ ?>