<?php $rand = rand();?>
<div class="padding-100-100 background-white">
    <div class="container">
        <div class="row content" >

            <div  in-view-options="{offsetBottom: '0'}" ng-class="{'fadeInUp' : tccf_inview<?php echo $rand; ?>  }" ng-init="tccf_inview<?php echo $rand; ?>    = false" in-view="tccf_inview<?php echo $rand; ?>    = true"   class="col-xs-12 col-xs-offset-0 text-xs-center col-sm-offset-0 col-sm-12 text-center">
            <?php if (isset($node_ref['node_ref']['field_sub_title_text'])) {?>
             <h2><?php print $node_ref['node_ref']['field_sub_title_text'][0]['#markup'];?></h2>
            <?php }
;?>

               <div ><?php print $node_ref['node_ref']['body'][0]['#markup'];?></div>
            </div>

        </div>
    </div>
</div>
