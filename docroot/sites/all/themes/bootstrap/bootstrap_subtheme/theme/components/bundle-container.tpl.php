
<div class="padding-100-100 bundles-section background-grey-light  " id="node-<?php echo $nid; ?>">
    <div class="container" >
        <div class="row " >
          <div id="node-<?php echo $nid; ?>" class="col-xs-10 col-xs-offset-1 col-sm-12 col-sm-offset-0 plugin-bundles text-center margin-bottom-lg">

            <h2 class="text-center">BUNDLES</h2>
            <p>The <?php print $title;?> is also available in the following money-saving bundles.</p>
                <?php $i = 0;foreach ($bundles as $bundle) {?>
                 <div   in-view-options="{ debounce: <?php echo ($i * 100) + 200; ?>  , offsetTop: '-50' }" ng-class="{'fadeInUp' : bnd_inview<?php echo $i; ?>  }" ng-init="bnd_inview<?php echo $i; ?>   = false" in-view="bnd_inview<?php echo $i; ?>   = true"   class="logo-container opacity-n0 margin-bottom-lg" >
                 <a href="/<?php print $bundle['path'];?>"> <img src="<?php print $bundle['logo'];?>"></a>
                 </div>
                <?php $i = $i + 1;
                }
;?>
          </div>
    </div>
  </div>
</div>
