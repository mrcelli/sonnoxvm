<?php $rand = rand();?>
<?php
$var = get_defined_vars();
//dpm($var);
$tot_plugins = sizeof($plugins);
?>
<div class="padding-100-100 product-bundle-container-plugin background-black text-center"  >
    <div class="container">
        <div class="row content" >
            <div class="col-xs-12 col-sm-12 text">
              <h2><font style="font-size:1.5em;"><?php print $tot_plugins;?></font>&nbsp;PLUG-INS</h2>
            </div>
            <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 ">
                 <div class="text-center  " >
                <?php $i = 0;foreach ($plugins as $key => $value) {?>

                      <div  in-view-options="{ debounce: <?php echo ($i * 100) + 200; ?>  , offsetTop: '-50' }" ng-class="{'fadeInUp' : bnd_inview<?php echo $i; ?>  }" ng-init="bnd_inview<?php echo $i; ?>   = false" in-view="bnd_inview<?php echo $i; ?>   = true"  class=" opacity-n0 square-plugin margin-bottom-sm">
                        <h4><?php print $value['title'];?></h4>
                        <div class="square-image">
                          <a href="/<?php print $value['url'];?>">
                                <?='<img src="' . image_style_url('original', $value['image']) . '" />'?>
                          </a>
                        </div>
                      </div>
                <?php $i = $i + 1;
                }
;?>
                </div>
                </div>



        </div>
    </div>
</div>
<div class="padding-100-100 product-bundle-container-plugin background-grey text-center padding-bottom-50" >
    <div class="container">
        <div class="row content" in-view-options="{offsetBottom: '10'}"  ng-class="{'fadeInUp' : cpinview<?php echo $rand; ?>}" ng-init="cpinview<?php echo $rand; ?> = false" in-view="cpinview<?php echo $rand; ?> = true"   id="bundle-container"  >
            <div class="col-xs-10 col-xs-offset-1 col-sm-offset-3 col-sm-6 text-center padding-bottom-50">
               <h2><?php print $node_ref['node_ref']['field_promo_title_price']['#items'][0]['value'];?></h2>
               <div ><?php print $node_ref['node_ref']['field_promo_price_text']['#items'][0]['value'];?></div>
            </div>
            <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-lg-6 col-lg-offset-3 padding-bottom-50">
                 <div class="col-xs-12 padding-bottom-50">
                <?php foreach ($prices as $key => $value) {?>
                      <div class="product-price-box">
                        <h4  class="price-format-title"><?php print $value['title'];?></h4>
                        <div class="price-format-content">
                              <ul class="list-unstyled">
                                <li>£<?php print $value['GBP'];?> GBP</li>
                              <li> $<?php print $value['USD'];?> USD  </li>
                               <li>€<?php print $value['EUR'];?> EUR</li>
                               </ul>
                        </div>
                      </div>
                <?php }
;?>
                </div>
                <div class="col-xs-12 col-sm-12 note ">
                <?php
                if(isset($node_ref['node_ref']['field_note'])) { print $node_ref['node_ref']['field_note']['#items'][0]['value'];
                };?>
              </div>
             </div>

        </div>
    </div>
</div>

