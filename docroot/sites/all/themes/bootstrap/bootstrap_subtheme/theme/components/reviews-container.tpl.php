<div class="padding-100-100 reviews background-grey-dark" id="node-reviews">
    <div class="container" >
        <div   class="row " >
         <div class="col-xs-12 col-sm-12 ">
          <h2 class="text-center margin-bottom-lg">REVIEWS</h2>
        </div>

          <div   class="col-xs-12 col-sm-12 text-center" >

            <?php $side = 0;foreach ($reviews as $review) {
    ?>
                    <div  in-view-options="{debounce: <?php echo ($side * 200) + 100; ?>, offsetBottom: '200'}" ng-class="{'fadeInUp' : rc_inview<?php echo $side; ?>  }" ng-init="rc_inview<?php echo $side; ?>   = false" in-view="rc_inview<?php echo $side; ?>   = true"  class="opacity-n0 margin-bottom-lg review-thumbnail padding-1em margin-1em">
                            <div class="col-xs-12 col-sm-12 text-center position-relative ">
                            <div class="logo-container">
                              <div  class="review-logo margin-bottom-md background-image" style="background-image:url(<?php print $review['logo'];?>);"></div>
                                </div>
                            </div>
                             <div class="col-xs-12 col-sm-12 text-center ">
                                    <?php print $review['body'];?>
                                  <div class="display-block text-center">
                                  <a  target="_blank" href="<?php print $review['url'];?>" class="ghost-white-sm margin-auto">Read More..</a>
                                </div>
                                </div>

                     </div>
            <?php $side = $side + 1;
            }
?>
          </div>
        </div>
    </div>
</div>
