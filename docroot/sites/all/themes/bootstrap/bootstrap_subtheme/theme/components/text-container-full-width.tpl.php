<?php $rand = rand();?>

<div class="padding-100-100 article-text-full-width background-white">
    <div class="container" >
        <div class="row content" >
            <div  in-view-options="{offsetBottom: '0'}" ng-class="{'fadeInUp' : tcfw_inview<?php echo $rand; ?>  }" ng-init="tcfw_inview<?php echo $rand; ?>    = false" in-view="tcfw_inview<?php echo $rand; ?>    = true"    class="col-xs-10 col-xs-offset-1 text-xs-center text-sm-center col-sm-10 col-sm-offset-1 col-md-offset-2 col-md-5 text-md-left  text-left  ">
                <h2><?php print $node_ref['node_ref']['field_sub_title_text'][0]['#markup'];?></h2>
               <div ><?php print $node_ref['node_ref']['body'][0]['#markup'];?></div>
            </div>
        </div>
    </div>
</div>
