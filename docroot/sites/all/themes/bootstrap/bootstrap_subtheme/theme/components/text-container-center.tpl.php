<?php $rand = rand();?>
<div class="padding-100-100 background-white">
    <div class="container">
        <div class="row content" >

            <div  in-view-options="{offsetBottom: '0'}" ng-class="{'fadeInUp' : tcc_inview<?php echo $rand; ?>  }" ng-init="tcc_inview<?php echo $rand; ?>    = false" in-view="tcc_inview<?php echo $rand; ?>    = true"   class="col-xs-10 col-xs-offset-1 text-xs-center col-sm-offset-3 col-sm-6 text-center">
            <?php if (isset($node_ref['node_ref']['field_sub_title_text'])) {?>
             <h2><?php print $node_ref['node_ref']['field_sub_title_text'][0]['#markup'];?></h2>
            <?php }
;?>

               <div ><?php print $node_ref['node_ref']['body'][0]['#markup'];?></div>
            </div>

        </div>
    </div>
</div>
