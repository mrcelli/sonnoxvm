<?php $rand = rand();?>
<div class="padding-100-100 background-white"   id="node-<?php echo $nid; ?>">
<div class="bg-sm" style=" background-image: url( <?php echo image_style_url('original', $node_ref['node_ref']['field_article_image']['#items'][1]['uri']); ?>)"></div>
<div class="bg-xs" style=" background-image: url( <?php echo image_style_url('original', $node_ref['node_ref']['field_article_image']['#items'][2]['uri']); ?>)"></div>
    <div class="container"  >
        <div  in-view-options="{offsetBottom: '0'}" ng-class="{'fadeInUp' : icfil_inview<?php echo $rand; ?>  }" ng-init="icfil_inview<?php echo $rand; ?>   = false" in-view="icfil_inview<?php echo $rand; ?>   = true"   class=" opacity-n0  row content" >
            <div class="col-xs-12 col-sm-12 col-md-6  text-center offsetside-text">


        <div id="node-<?php echo $nid; ?>" class="image-container" data-toggle="modal" data-target="#node-<?php echo $nid; ?>Modal">
            <?php print '<img src="' . file_create_url($node_ref['node_ref']['field_article_image']['#items'][0]['uri']) . '" style="width: 100%;height: auto;max-width:300px;cursor:pointer;" />';?>
        </div>
        <!-- Modal -->

        <div class="modal fade" id="node-<?php echo $nid; ?>Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div id="node-<?php echo $nid; ?>" class="image-container" data-toggle="modal" data-target="#node-<?php echo $nid; ?>Modal" style="width: 100%;height: 100%;vertical-align: middle;display: table;margin: 0 auto;">
            <div style="display: table-cell;vertical-align: middle;margin: 0 auto;">
                <?php echo '<img src="' . file_create_url($node_ref['node_ref']['field_article_image']['#items'][0]['uri']) . '" style="width: 100%;height: auto;" />'; ?>
            </div>
          </div>
        </div>
       </div>
            <div class="col-xs-10 col-xs-offset-1 text-xs-center col-sm-10 col-sm-offset-1  text-sm-center col-md-5 col-md-offset-1 text-md-left offsetside-text">
                <h2><?php print $node_ref['node_ref']['field_sub_title_text'][0]['#markup'];?></h2>
               <div ><?php print $node_ref['node_ref']['body'][0]['#markup'];?></div>
            </div>
        </div>
    </div>
</div>
