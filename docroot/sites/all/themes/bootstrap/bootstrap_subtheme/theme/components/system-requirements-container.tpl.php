<div class="row">
<div  in-view-options="{offsetBottom: '0'}" ng-class="{'fadeInUp': srcinview  }"  in-view="srcinview = true"    ng-init="srcinview  = false"  class="   col-xs-12 text-center">
<a class="btn btn-default btn-lg" data-toggle="modal" href="#myModal">
<i class="fa fa-cog" style="margin-right: 0.5em;"></i>Minimum system requirements...
</a>

</div>

<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h2 class="modal-title" id="myModalLabel"><?php print $title;?></h2>
      </div>
      <div class="modal-body text-left">
            <?php

            print $items;

            if (sizeof($content_outside) > 0) {
                foreach ($content_outside as $key => $value) {
                    print render($value);
                }
                ;
            }
            ;

?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

      </div>
    </div>
  </div>
</div>
</div>


 </div> <!-- end container from compatibility section -->
 </div> <!-- end padding-100-100 from compatibility section -->
