<?php $rand = rand();?>
<div class="padding-100-100 background-cover"   id="node-<?php echo $nid; ?>">
<div class="bg-sm" style=" background-image: url( <?php echo image_style_url('original', $node_ref['node_ref']['field_article_image']['#items'][0]['uri']); ?>)"></div>
<div class="bg-xs" style=" background-image: url( <?php echo image_style_url('original', $node_ref['node_ref']['field_article_image']['#items'][1]['uri']); ?>)"></div>
    <div class="container">
        <div class="row content" >
            <div   in-view-options="{offsetBottom: '0'}" ng-class="{'fadeInUp' : icfl_inview<?php echo $rand; ?>  }" ng-init="icfl_inview<?php echo $rand; ?>   = false" in-view="icfl_inview<?php echo $rand; ?>   = true"   class=" opacity-n0  col-xs-10 col-xs-offset-1 text-xs-center col-sm-10 col-sm-offset-1  text-sm-center col-md-5 col-md-offset-1 text-md-left offsetside-text">
                <h2><?php print $node_ref['node_ref']['field_sub_title_text'][0]['#markup'];?></h2>
               <div ><?php print $node_ref['node_ref']['body'][0]['#markup'];?></div>
            </div>
             <div class="col-xs-12 col-sm-6 visible-xs hidden-sm hidden-md hidden-lg side-image">

            </div>
        </div>
    </div>
</div>
