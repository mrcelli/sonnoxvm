<?php

$height = $variables['metadata']['height'];

?>
<img class="media-image-right" data-fid="<?php echo $fid; ?>" style="max-height:<?php echo $height; ?>px;" title="<?php echo $title; ?>" alt="<?php echo $alt; ?>" src="<?php echo image_style_url($view_mode, $uri); ?>">
