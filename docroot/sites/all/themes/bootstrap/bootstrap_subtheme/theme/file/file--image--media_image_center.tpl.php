<?php

$height = $variables['metadata']['height'];

?>
<img class="media-image-center" style="max-height:<?php echo $height; ?>px;" data-fid="<?php echo $fid; ?>" title="<?php echo $title; ?>" alt="<?php echo $alt; ?>" src="<?php echo image_style_url($view_mode, $uri); ?>">
