<?php

global $base_url;

$logo_white_link = $base_url.'/sites/default/files/Sonnox-logo-white.svg';
$logo_jpg        = $base_url.'/sites/default/files/Sonnox-logo-white.svg';

// onerror="this.onerror=null; this.src='<?php print $page['logo'];
?>


      <div class="nav-container global-nav" id="nav-container">
   <header role="banner" id="page-header" class=" ">

      <div  class="position-absolute visible-xs visible-sm hidden-md  hidden-lg  navicon-box " >
        <div class="position-relative navicon-wrapper">
           <a class="navicon-button x">
              <div class="navicon"></div>
            </a>
        </div>
      </div>

    <?php
    if ($page['logo']) :;
?>
                <a class="logo  " href="<?php print $page['front_page']; ?>" title="<?php print t('Home'); ?>">
          <!--img src="
            <?php
          //  print $logo_white_link;
            // $page['logo'];
?>
" alt="<?php print t('Home'); ?>"  /-->
          <div class="logo-background backgroun-image" style="background-image:url(<?php print $logo_white_link; ?>);"></div>
        </a>
        <?php
        // }
    endif;
    ?>

    <?php
    if ($content_attributes) :;
?>
<div<?php print $content_attributes; ?>>
    <?php
    endif;
    ?>
    <?php print $content; ?>
    <?php
    if ($content_attributes) :;
?>

    </div><?php     endif; ?>



  </header> <!-- /#page-header -->
</div><!-- /#nav-container -->
