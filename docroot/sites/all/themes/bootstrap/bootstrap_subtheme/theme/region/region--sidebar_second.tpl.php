<?php if ($content) : ?>
  <aside<?php print $attributes;?> >
    <?php if ($content_attributes) : ?><div<?php print $content_attributes;?>><?php 
    endif;?>
    <?php print $content;?>
    <?php if ($content_attributes) : ?></div><?php 
    endif;?>
  </aside>
<?php endif;?>
