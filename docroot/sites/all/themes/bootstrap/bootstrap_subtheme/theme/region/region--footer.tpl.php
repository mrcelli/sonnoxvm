<?php


$var = get_defined_vars();
//dpm($var);
?>

<footer ng-controller="footerCtr" class="footer full-container container-fluid background-cover padding-top-lg " style="" >

 <div class="container ">
   <div class="col-xs-12  text-center ">
      <div class="display-inline-block  footer-inline display-xs-block min-width-300px  ">
        <a   target="_top" class="color-text  cursor-pointer" ng-click="shownewsletter()" >Subscribe To Newsletter&nbsp;<i class="fa fa-newspaper-o"></i></a>
      </div>
      <div class="display-xs-block footer-inline display-inline">
      <?php print render($var['elements']['menu_menu-social-menu']); ?>
    </div>
      <div class="display-xs-block footer-inline display-inline-block min-width-300px ">
        <a href="mailto:support@sonnox.com" target="_top" class="color-text"><i class="fa fa-envelope"></i>&nbsp;Contact Support</a>
      </div>

</div>
     <div class="col-xs-12 text-align-left">
      <?php print render($elements['menu_menu-footer-menu']); ?>
     </div>


  <div class='col-sm-12 col-xs-12 color-text text-center'>
        <div class="display-inline privacy display-xs-block " >
        <a class="display-inline color-text" href="/privacy-policy">Privacy Policy</a> / <a class="color-text " href="/terms-and-conditions">Terms & Conditions</a>
      </div>&nbsp;&nbsp;
    <div class='display-inline display-xs-block  color-text copyright '> © 2007 -
      <?php
      echo date('Y');
      $site_name = variable_get('site_name');
      echo '&nbsp';
      echo $site_name;
      echo ' ';
      ?>
       </div>

  </div>

</div>
</footer>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-72858008-2', 'auto');
  ga('send', 'pageview');

</script>
