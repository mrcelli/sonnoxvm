<?php if ($content) : ?>
  <div<?php print $attributes;?>>
    <?php if ($content_attributes) : ?><div<?php print $content_attributes;?>><?php 
    endif;?>
    <?php print render($page['page']['highlighted']);?>
    <?php //print render($page['breadcrumb']); ?>
    <a id="main-content"></a>
    <?php print render($page['title_prefix']);?>

    <?php if ($page['title']) : ?>
      <!--h1 class="page-header"><?php print $page['title'];?></h1-->
    <?php endif;?>

    <?php print render($page['title_suffix']);?>
    <?php print render($page['messages']);?>
    <?php if (($page['tabs']) && ($is_admin)) : ?>
    <div class="container">
    <?php print render($page['tabs']);?>
    </div>
    <?php endif;?>
    <?php print render($page['page']['help']);?>
    <?php print render($page['action_links']);?>
    <?php print $content;?>
    <?php if ($content_attributes) : ?></div><?php 
    endif;?>
  </div>
<?php endif;?>
