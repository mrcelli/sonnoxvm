<?php
  $head_title = 'mysite.com :: Site-offline';
  $logo = 'sites/all/files/customLogo.png';

  // If your theme is set to display the site name, uncomment this line and replace the value:
  // $site_name = 'Your Site Name';

  // If your theme is set to *not* display the site name, uncomment this line:
  unset($site_name);

  // If your theme is set to display the site slogan, uncomment this line and replace the value:
  //$site_slogan = 'My Site Slogan';

  // If your theme is set to *not* display the site slogan, uncomment this line:
  // unset($site_slogan);

  // Main message. Note HTML markup.
  $content = '<p>The site is currently not available due to technical problems. Please try again later. Thank you for your understanding.</p><hr /><p>If you are the maintainer of this site, please check your database settings.</p>';
?>
<div id="site-container" >


    <?php print render($page['header']); ?>



<div class="outer-page-container">

<div class="main-container full-container ">


    <?php
    if ($page['sidebar_first']) :;
?>

    <?php print render($page['sidebar_first']); ?>
    <?php     endif; ?>

        <?php print render($page['content']); ?>

<div class="container ">
    <?php
    if ($page['sidebar_second']) :
        ;
?>

    <?php print render($page['sidebar_second']); ?>
    <?php     endif; ?>
 </div>
  </div>

    <?php print render($page['footer']); ?>
 </div><!-- outerpage container  -->
 </div> <!-- site container  -->




