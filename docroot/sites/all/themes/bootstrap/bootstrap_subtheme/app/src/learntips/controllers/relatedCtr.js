(function () {
  angular
    .module('sonnox.learntips')
    .controller('tipsrelatedCtr', tipsrelatedCtr);


  tipsrelatedCtr.$inject = ['$scope',  'tipslist'];

  function tipsrelatedCtr($scope, tipslist) {
      $scope.related = tipslist.getrelated();
    }
})();
