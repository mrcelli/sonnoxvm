(function () {
  'use strict';
  // Fix for naughty jQuery plugins.
  window.$ = window.jQuery;
  angular.module('sonnox.learnsection', ['sonnox-templates', 'ui.bootstrap', 'pasvaz.bindonce', 'infinite-scroll']);
  //angular.module('infinite-scroll').value('THROTTLE_MILLISECONDS', 250);
}());
