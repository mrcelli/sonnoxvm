/* jshint ignore:start */
(function () {
  angular
    .module('sonnox.common')
    .filter('productSearch', productSearch);

  productSearch.$inject = ['$filter', '$log'];
  function productSearch($filter, $log) {
    return function (products, searchText) {
      //$log.debug(product);

      var filterMW = $filter('filterMW');
      //var searchRegx = new RegExp(searchText, 'i');
      if ((searchText === 'undefined') || (!searchText)) {
        return products;
      }

      //var productContent = product.node;

      var  filterResult = filterMW(products, ['title', 'body', 'Tags'], searchText);

      $log.debug('end');


      return filterResult;
    };
  }

}());
