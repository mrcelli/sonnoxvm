(function () {
  angular
    .module('sonnox.learntips')
    .controller('tipstilesListCtr', tipstilesListCtr);

  tipstilesListCtr.$inject = ['$scope', '$rootScope', '$http', '$log', 'searchtips'];

  function tipstilesListCtr($scope, $rootScope, $http, $log, searchtips) {
      $log.debug(searchtips);
      var result = searchtips.gettiles();
      $scope.terms = searchtips.getfilters();
      $log.debug($scope.terms);
      $scope.tiles =  result.tiles;
      $scope.totalpages = result.pager.pages;
      $scope.count = result.pager.count;
      $scope.currentpage = result.pager.page;
      $log.debug($scope);
      $rootScope.listscope = $scope;
      $scope.loading = false;

      $scope.termChanged = function () {
        $scope.termsvalues = $scope.termsselected.join(',');
        $log.debug($scope.termsvalues);
      };

      $scope.loadm = function () {
        if (searchtips.loading === true) { return; }
        if ($scope.totalpages === ($scope.currentpage + 1)) {
          searchtips.loading = false;
          $scope.loading = searchtips.loading;
          return;
        }
        $log.debug($scope.currentpage);
        $log.debug(searchtips.loading);
        $log.debug($scope.totalpages);
        searchtips.loading = true;
        $scope.loading = searchtips.loading;

        searchtips.set_page($scope.currentpage + 1);

        searchtips.loadmore().success(function (data) {
          $log.debug(data);
          $scope.tiles.push.apply($scope.tiles, data.nodes);
          $scope.totalpages = data.pager.pages;
          $scope.count = data.pager.count;
          $scope.currentpage = data.pager.page;
          $log.debug($scope.tiles);
          searchtips.loading = false;
          $scope.loading = searchtips.loading;
        }).error(function () {
          searchtips.loading = false;
          $scope.loading = searchtips.loading;
        });
      };



    }




}());
