(function () {
  'use strict';
  // Fix for naughty jQuery plugins.
  window.$ = window.jQuery;
  angular.module('sonnox.footer', ['sonnox-templates', 'ui.bootstrap']);
}());

