(function () {
  angular
    .module('sonnox.learnsection')
    .factory('artistslist', artistslist);

  artistslist.$inject = ['$log'];

  function artistslist($log) {

      return  {
        getlist : function () {
          $log.debug(Drupal.settings.sonnox.artistslist);
          return Drupal.settings.sonnox.artistslist.artists.nodes;
        },
        getrelated : function () {
          $log.debug(Drupal.settings.sonnox.artistslist.related);
          return Drupal.settings.sonnox.artistslist.related.nodes;
        }
      };

    }


})();
