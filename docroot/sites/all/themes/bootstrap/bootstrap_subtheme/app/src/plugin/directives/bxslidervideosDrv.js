(function () {
    angular
        .module('sonnox.plugin')
        .directive('bxslidervideosDrv', bxslidervideosDrv);
    bxslidervideosDrv.$inject = ['$log'];

    function bxslidervideosDrv($log) {
        return {
          restrict: 'E',
          replace: true,
          templateUrl: 'plugin/views/bxslider-videos.html',
          link: function (scope, elm) {
            elm.ready(function () {
              scope.bxSlider = $('.' + $(elm[0]).attr('class')).bxSlider({
                mode: 'vertical',
                nextSelector: '#plugint-topsection .bx-next',
                prevSelector: '#plugint-topsection .bx-prev',
                adaptiveHeight: true,
                nextText: '',
                prevText: '',
                keyboardEnabled: true,
                controls: true,
                infiniteLoop: false,
                pager: false,
                autoControls: false,
                touchEnabled: true,
                oneToOneTouch: true,
                preventDefaultSwipeY: true,
                swipeThreshold: 20
              });
              $log.debug(scope.bxSlider);

              $('ul#bx-plugin').parent().mousewheel(function (event, delta, deltaX, deltaY) {
                if (delta > 0) {
                  scope.bxSlider.goToPrevSlide();
                }
                if (deltaY < 0) {
                  scope.bxSlider.goToNextSlide();
                }
                event.stopPropagation();
                event.preventDefault();
              });


            });
          }
        };

      }




  }());
