(function () {
  angular
    .module('sonnox.learnspotlight')
    .controller('spotlightrelatedCtr', spotlightrelatedCtr);


  spotlightrelatedCtr.$inject = ['$scope',  'spotlightlist'];

  function spotlightrelatedCtr($scope, spotlightlist) {
      $scope.related = spotlightlist.getrelated();
    }
})();
