(function () {
  angular
    .module('sonnox.learnspotlight')
    .controller('searchBoxspotlightCtr', searchBoxspotlightCtr);

  searchBoxspotlightCtr.$inject = ['$scope', '$rootScope', '$http', '$log', 'searchspotlight'];

  function searchBoxspotlightCtr($scope, $rootScope, $http, $log, searchspotlight) {
      $scope.loading = searchspotlight.loading;
      $scope.$watch(function () { return searchspotlight.loading; },
              function (newValue) {
                $scope.loading = newValue;
              }
             );

      $scope.search = function () {
        if (searchspotlight.loading ===  true) { return; }
        searchspotlight.set_q($scope.q);
        searchspotlight.set_page(0);
        searchspotlight.loading = true;
        $rootScope.listscope.loading = searchspotlight.loading;
        searchspotlight.loadmore().success(function (data) {
          $rootScope.listscope.loading = searchspotlight.loading;
          $log.debug(data);
          $rootScope.listscope.tiles = data.nodes;
          $rootScope.listscope.totalpages = data.pager.pages;
          $rootScope.listscope.count = data.pager.count;
          $rootScope.listscope.currentpage = data.pager.page;
          $log.debug($scope.tiles);
          searchspotlight.loading = false;
          $rootScope.listscope.loading = searchspotlight.loading;
        }).error(function () { searchspotlight.loading = false; $rootScope.listscope.loading = searchspotlight.loading; });
      };
    }




}());
