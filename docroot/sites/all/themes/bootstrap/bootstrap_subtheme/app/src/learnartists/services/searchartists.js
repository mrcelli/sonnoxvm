(function () {
  angular
    .module('sonnox.learnartists')
    .factory('searchartists', searchartists);

  searchartists.$inject = ['$log', '$http'];

  function searchartists($log, $http) {
    var page;

    return  {
      loading : false,
      gettiles : function () {
                        return Drupal.settings.sonnox.artistsresult;
                      },
      getfilters: function () {
                        return Drupal.settings.sonnox.artistsfilters.terms;
                      },
      loadmore  : function () {
          $log.debug(page);
          //var termsquery = this.tearmlist.join('+');
          $log.debug(this.q);
          $log.debug(this.tearmlist);
          return $http({method: 'GET', url: '/json/artists?',   params: { page :  page, query : this.q, 'taxonomytid[]' : this.tearmlist} });

        },
      set_q : function (query) {
        this.q = query;
      },
      get_q : function () {
        return this.q;
      },
      set_page : function (p) {
        page = p;
      },
      get_page : function () {
        return page;
      },
      tearmlist : [],
      q : ''

    };

  }





}());
