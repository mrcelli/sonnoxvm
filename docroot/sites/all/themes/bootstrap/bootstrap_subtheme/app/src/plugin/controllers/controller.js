(function () {
  angular
    .module('sonnox.plugin')
    .controller('pluginCtr', pluginCtr);


  pluginCtr.$inject = ['$scope', 'relatedvideosrv', '$log', 'isMobile', '$timeout', 'youtubeEmbedUtils', '$window'];

  function pluginCtr($scope, relatedvideosrv, $log, isMobile) {
      $scope.isMobile = isMobile;
      $log.debug('isMobile');

      $scope.videos = relatedvideosrv.getlist();
      $log.debug($scope.videos);
      $scope.showvideos = false;
      $scope.videourl = $scope.videos[0].field_video_id;
      $scope.activevideo = 0;
      $scope.playerVars = {
          autoplay: 1,
          controls: 1,
          enablejsapi: 1,
          modestbranding: 1,
          rel: 0
        };
      $log.debug(Drupal.settings.baseUrl);
     // $scope.youtubePlayer = {};
      $scope.videosleft = function () {
        $('#videos-wrapper').addClass('video-toleft');
        $scope.youtubePlayer.stopVideo();
        $scope.showvideos = false;
      };
      $scope.loadvideo = function (field_video_id, index) {
          $scope.youtubePlayer.stopVideo();
          $scope.activevideo = index;

          if ($scope.videourl === field_video_id) {
            $scope.youtubePlayer.playVideo();
            $('#video-left').removeClass('mobile-notvisible');
            $('#mobile-close-video').removeClass('mobile-notvisible');
            $('#video-left').addClass('mobile-visible');
            $('#mobile-close-video').addClass('mobile-visible');
          }
          $scope.playerVars = {
            controls: 1,
            autoplay: 1,
            enablejsapi: 1,
            modestbranding: 1,
            rel: 0,
            origin: '"' + Drupal.settings.baseUrl + '"'
          };
          $scope.videourl = field_video_id;// 'https://www.youtube.com/embed/' + v + '?autoplay=1&enablejsapi=1&autohide=1&theme=light';

          $scope.$on('youtube.player.ready', function () {
            if (!isMobile.any()) {
              $scope.youtubePlayer.playVideo();
            }
            window.youtubePlayer = $scope.youtubePlayer;
            $('#video-left').removeClass('mobile-notvisible');
            $('#mobile-close-video').removeClass('mobile-notvisible');
            $('#video-left').addClass('mobile-visible');
            $('#mobile-close-video').addClass('mobile-visible');


            /*$('#video-left iframe').contents().find('.ytp-spinner-svg').each(function (elm) {
              $(elm).click();
              $log.debug(elm);
            });*/
          });

        };
      $scope.closevideomobile = function () {
        $scope.youtubePlayer.stopVideo();
        $('#video-left').addClass('mobile-notvisible');
        $('#mobile-close-video').addClass('mobile-notvisible');
        $('#video-left').removeClass('mobile-visible');

      };
      $scope.goToNextSlide = function () {
        $scope.bxSlider.goToNextSlide();
      };
      $scope.gotToPrevSlide = function () {
        $scope.bxSlider.goToPrevSlide();
      };
      $scope.videonotinview = function (event, status) {
        if (status === false) {
          $scope.youtubePlayer.stopVideo();
          $scope.closevideomobile();
          $scope.showvideos = false;
        }
      };

      //var slider = $("ul#bxslider").bxSlider();





    }


})();

