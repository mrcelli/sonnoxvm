(function () {
  angular
    .module('sonnox.learnartists')
    .controller('learnPageArtists', learnPageArtists);

  learnPageArtists.$inject = ['$scope', '$log'];

  function learnPageArtists($scope, $log) {
    $log.debug($scope);
    $scope.page_change = function () {
      $log.debug($scope.page_value);
      window.location.href =  $scope.page_value;
    };
  }

}());
