(function () {
  angular
    .module('sonnox.about')
    .factory('peoplesrv', peoplesrv);

  peoplesrv.$inject = ['$log'];

  function peoplesrv() {

      return  {
        getlist : function () {
          //$log.debug(Drupal.settings.sonnox.people);
          if (typeof(Drupal.settings.sonnox.people.list) !== 'undefined') {
            return Drupal.settings.sonnox.people.list;
          } else {
            return {};
          }
        }
      };

    }


})();

