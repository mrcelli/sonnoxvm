(function () {
  angular
    .module('sonnox.products')
    //.config(function ($locationProvider) {
    //  $locationProvider.html5Mode({enabled: true, requireBase: false}).hashPrefix('!');

    //})

    .controller('productsCtr', productsCtr);

  productsCtr.$inject = ['$scope', 'productslistsrv', '$timeout', '$rootElement', '$log'];

  function productsCtr($scope, productslistsrv, $timeout, $rootElement, $log) {
    function escapeRegExp(string) {
        return string.replace(/([.*+?\^=!:${}()|\[\]\/\\])/g, '\\$1');
      }
    $scope.shash = window.location.hash;
    //$log.debug('hash');
    //$log.debug(window.location);
    //$log.debug($scope.shash);
    if ($scope.shash === '#bundles') {
      $scope.type = 'product_bundle';
    } else if ($scope.shash === '#plugins') {
      $scope.type = 'product_plug_in';
    } else {
      $scope.type = 'all';
    }


    $scope.products  = productslistsrv.getlist();
    $scope.templatetype  = 1;
    $scope.radio = '1';
    $scope.display  = '';
    $scope.gettemplate = function (templatetype) {
                $log.debug(templatetype);
                $scope.templatetype = templatetype;
                if (templatetype !== 1) {
                  // data = $templateCache.get('products/views/products-list-square.html');
                  $scope.display = 'products/views/products-list-square.html';
                } else {
                  // data = $templateCache.get('products/views/products-list.html');
                  $scope.display = 'products/views/products-list.html';
                }
                //element.html(data);
                //$compile(element.contents())(scope);
              };
    $scope.gettemplate($scope.templatetype);
    $scope.$watch('display', function (newVal) {
        $scope.watchController = newVal ? newVal : 'undefined';
      });
    $scope.$watch('templatetype', function (val, oldval) {
        $log.debug(oldval);
        $scope.gettemplate(val);
      });

    $scope.filterBySearch = function (product) {
      var val = product.node.title;
      if (!$scope.search) { return true; }
      var regex = new RegExp('\\b' + escapeRegExp($scope.search), 'i');
      return regex.test(val);
    };
    $('#search-products').focus();
  }
}());
