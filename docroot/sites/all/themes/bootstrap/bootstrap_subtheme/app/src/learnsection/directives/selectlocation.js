(function () {
  angular
    .module('sonnox.learnsection')
    .directive('selectlocation', selectlocation);

  selectlocation.$inject = ['$log'];
 /**
  * change url on select
  */

  function selectlocation($log) {
    return {
      restrict: 'A',
      link: link
    };

    function link(scope, elm, attrs) {
      elm.on('change', function () {
        $log.debug(attrs);
        window.location.href =  attrs.value;
      });
    }
  }



}());
