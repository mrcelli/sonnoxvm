/* jshint ignore:start */
(function () {
  angular
    .module('sonnox.common')
    .filter('faqSearch', faqSearch);

  faqSearch.$inject = ['$filter', '$log'];
  function faqSearch($filter, $log) {
    return function (faq, searchText) {
      //$log.debug(product);

      var filterMW = $filter('filterMW');
      //var searchRegx = new RegExp(searchText, 'i');
      if ((searchText === 'undefined') || (!searchText)) {
        return faq;
      }

      //var productContent = product.node;

      var  filterResult = filterMW(faq, ['title', 'body'], searchText);

      $log.debug('end');


      return filterResult;
    };
  }

}());
