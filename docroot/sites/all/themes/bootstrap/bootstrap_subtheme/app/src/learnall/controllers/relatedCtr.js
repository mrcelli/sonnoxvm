(function () {
  angular
    .module('sonnox.learnarticles')
    .controller('learnarticlesrelatedCtr', learnarticlesrelatedCtr);


  learnarticlesrelatedCtr.$inject = ['$scope',  'learnarticleslist'];

  function learnarticlesrelatedCtr($scope, learnarticleslist) {
      $scope.related = learnarticleslist.getrelated();
    }
})();
