(function () {
  angular.module('sonnox.learnnews')
  .controller('tagsnewsFiltersCtr', tagsnewsFiltersCtr);

  tagsnewsFiltersCtr.$inject = ['$scope', '$rootScope', '$document', 'searchnews', '$log'];

  function tagsnewsFiltersCtr($scope, $rootScope, $document, searchnews, $log) {
    // add
    var termlist = [];
    searchnews.tearmlist = termlist;
    var comparator = angular.equals;

    function contains(arr, item, comparator) {
      if (angular.isArray(arr)) {
        for (var i = arr.length; i--;) {
          if (comparator(arr[i], item)) {
            return true;
          }
        }
      }
      return false;
    }

    function add(arr, item, comparator) {
      arr = angular.isArray(arr) ? arr : [];
      if (!contains(arr, item, comparator)) {
        arr.push(item);
      }
      return arr;
    }

    // remove
    function remove(arr, item, comparator) {
      if (angular.isArray(arr)) {
        for (var i = arr.length; i--;) {
          if (comparator(arr[i], item)) {
            arr.splice(i, 1);
            break;
          }
        }
      }
      return arr;
    }
    $rootScope.listscope.loading = searchnews.loading;
    $scope.loading = searchnews.loading;
    $scope.loadnews = function () {
        if (searchnews.loading ===  true) { return; }
       // searchnews.set_q($scope.q);
        searchnews.set_page(0);
        searchnews.loading = true;
        $rootScope.listscope.loading = searchnews.loading;

        searchnews.loadmore().success(function (data) {
          $log.debug(data);
          $rootScope.listscope.tiles = data.nodes;
          $rootScope.listscope.totalpages = data.pager.pages;
          $rootScope.listscope.count = data.pager.count;
          $rootScope.listscope.currentpage = data.pager.page;
          $log.debug($scope.tiles);
          searchnews.loading = false;
          $rootScope.listscope.loading = searchnews.loading;
          $scope.loading = $rootScope.listscope.loading;
        }).error(function () { searchnews.loading = false; $rootScope.listscope.loading = searchnews.loading; $scope.loading = $rootScope.listscope.loading; });
      };

    $scope.togglenewsterm = function (tid) {
      if (searchnews.loading === true) { return; }
      termlist = add(termlist, tid, comparator);
      var trm = $document[0].getElementById('term-' + tid);
      if (angular.element(trm).hasClass('active')) {
        termlist = remove(termlist, tid, comparator);
        angular.element(trm).removeClass('active');
      } else {
        termlist = add(termlist, tid, comparator);
        angular.element(trm).addClass('active');
      }
      $scope.loadnews();
      $log.debug(termlist);
    };

  }


})();
