(function () {
  angular
    .module('sonnox.learnspotlight')
    .controller('learnPagespotlight', learnPagespotlight);

  learnPagespotlight.$inject = ['$scope', '$log'];

  function learnPagespotlight($scope, $log) {
    $log.debug($scope);
    $scope.page_change = function () {
      $log.debug($scope.page_value);
      window.location.href =  $scope.page_value;
    };
  }

}());


