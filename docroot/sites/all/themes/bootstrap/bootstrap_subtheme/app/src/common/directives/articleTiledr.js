(function () {
    angular
        .module('sonnox.common')
        .directive('articletile', articletile);
    articletile.$inject = ['$modal', '$timeout', 'relatedproducts', 'youtubeEmbedUtils', '$log', 'isMobile'];

    function articletile($modal, $timeout, relatedproducts, youtubeEmbedUtils, $log) {

        return {
            restrict: 'E',
            templateUrl: articletiletemplate, //'common/views/article-tile.html',
            link: link
          };

        function articletiletemplate() {
          var isMobile = {
              t : function () {
                  return 'test';
                },
              Android : function () {
                  return navigator.userAgent.match(/Android/i);
                },
              BlackBerry : function () {
                  return navigator.userAgent.match(/BlackBerry/i);
                },
              iOS : function () {
                  return navigator.userAgent.match(/iPhone|iPad|iPod/i);
                },
              Opera : function () {
                  return navigator.userAgent.match(/Opera Mini/i);
                },
              Windows : function () {
                  return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
                },
              any : function () {
                  var p = isMobile;
                  return (p.Android() || p.BlackBerry() || p.iOS() || p.Opera() || p.Windows());
                }
            };
          $log.debug(navigator.userAgent);
          $log.debug(isMobile.Android());
          $log.debug(isMobile.Opera());
          if (isMobile.any()) {
            return 'common/views/article-mobile-tile.html';
          } else {
            return 'common/views/article-tile.html';
          }
        }
        function link(scope) {
           // scope.help = help();
           //item.node.field_external_link
           //item.node.field_internal_link

            scope.baseUrl = Drupal.settings.baseUrl;
            scope.dgetTarget = function (item) {
              var internal = item.node.field_internal_link;
              var external = item.node.field_external_link;
              $log.debug('internal');
              $log.debug(internal);
              if (internal.length === 0 || typeof(external) === 'undefined') {
                return '_self';
              } else {
                if (internal === 1) {
                  return '_self';
                } else {
                  return '_blank';
                }
              }
            };
            scope._addClass = function (target, classtoadd) {
              $(target.inViewTarget).addClass(classtoadd);
              $(target.inViewTarget).removeAttr('in-view');
              $(target.inViewTarget).removeAttr('in-view-options');
            };
            scope.commonshowvideo = function (tile) {
              //scope.videourl = youtubeEmbedUtils.getIdFromURL(scope.videos[0].field_article_video_video_url);
              scope.related = null;
              $modal.open({
                templateUrl: 'common/views/videomodal.html',
                windowTemplateUrl: 'common/views/myDlgTemplateWrapper.html',
                controller: function ($scope, $modalInstance) {
                  $log.debug(tile);
                  $scope.related = null;
                  //var videourl = tile.node.field_article_video_video_url;
                  $scope.title = tile.node.title;
                  $scope.body = tile.node.body;
                  $scope.v = tile.node.field_video_id;
                  $scope.videourl =  'https://www.youtube.com/embed/' + $scope.v + '?autoplay=1&enablejsapi=1&autohide=1&theme=light';
                  $scope.$on('youtube.player.ready', function () {
                    $scope.youtubePlayer.playVideo();
                  });

                  $log.debug(tile.node.nid);
                  relatedproducts.get_related_products(tile.node.nid).success(function (data) {
                    $log.debug(data.nodes);
                    $scope.related = data.nodes;
                  });
                  $modalInstance.opened.then(function () {
                    $timeout(function () {
                      //fluidIframe('video-container-' + $scope.v);
                    }, 500);
                  });
                  $scope.commonclose = function () {
                    $modalInstance.close();
                  };
                }
              });
            };
        /*    function fluidIframe(v) {
              // Find all YouTube videos
              var $allVideos = $('.fluid-iframe');
              $log.debug($allVideos);

                  // The element that is fluid width
              var $fluidEl = $('#' + v);

              // Figure out and save aspect ratio for each video
              $allVideos.each(function () {

                $(this)
                  .data('aspectRatio', this.height / this.width)

                  // and remove the hard coded width/height
                  .removeAttr('height')
                  .removeAttr('width');
                $(this).closest('.modal-video').addClass('no-transition');

              });

              // When the window is resized
              // (You'll probably want to debounce this)
              $(window).resize(function () {

                var newWidth = $fluidEl.width();

                // Resize all videos according to their own aspect ratio
                $allVideos.each(function () {

                  var $el = $(this);
                  $el
                    .width(newWidth)
                    .height(newWidth * $el.data('aspectRatio'));

                });

              // Kick off one resize to fix all videos on page load
              }).resize();

            }*/
          }


      }



  }());
