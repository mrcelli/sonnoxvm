(function () {
  angular
    .module('sonnox.page')
    .controller('pagectr', pagectr);


  pagectr.$inject = ['$scope', '$window', '$log', '$document'];

  function pagectr($scope, $window, $log) {

      $log.debug($scope);
      $scope.submenu = null;

      $scope.inview = function (event, status) {
        $log.debug(event);
        $log.debug(status);
        var target = $(event.inViewTarget);
        $scope.submenu = target;
        if (status === false) {
          target.removeClass('visible');
          target.addClass('submenu-notvisible');
          $('body').addClass('submenu-notvisible');
        } else {
          //target.addClass('visible');
          //target.removeClass('submenu-notvisible');
        }

      };
      $scope.notinview = function (event, status) {
        //var target = $(event.inViewTarget);
        $log.debug(status);
        if ($scope.submenu !== null) {
          if ($scope.submenu.hasClass('submenu-notvisible') && status === true) {
            $scope.submenu.addClass('visible');
            $scope.submenu.removeClass('submenu-notvisible');
            $('body').removeClass('submenu-notvisible');
            $scope.submenu = null;
          }
        }
      };


      $scope.page_change = function (page_value) {
        $log.debug(page_value);
        window.location.href =  page_value;
      };
      $scope.addClass_ = function (target, classname) {
          $(target).addClass(classname);
          $log.debug(classname);
        };
      $scope.goto = function (url) {
        $window.location = Drupal.settings.baseUrl + '/' + url;
      };



      $scope.imgLoadedEvents = {

          always: function (instance) {
              // Do stuff
              instance = instance;
              angular.element(instance.elements[0]).addClass('loaded');
            },

          done: function (instance) {
              $log.debug(instance);
              //angular.element(instance.elements[0]).addClass('loaded');

            },

          fail: function (instance) {
              // Do stuff
              instance = instance;
              angular.element(instance.elements[0]).addClass('loaded');

            }

        };
    }

})();
