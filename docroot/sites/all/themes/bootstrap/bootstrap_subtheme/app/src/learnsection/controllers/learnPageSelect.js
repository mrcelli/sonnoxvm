(function () {
  angular
    .module('sonnox.learnsection')
    .controller('learnPage', learnPage);

  learnPage.$inject = ['$scope', '$log'];

  function learnPage($scope, $log) {
    $log.debug($scope);
    $scope.page_change = function () {
      $log.debug($scope.page_value);
      window.location.href =  $scope.page_value;
    };
  }

}());
