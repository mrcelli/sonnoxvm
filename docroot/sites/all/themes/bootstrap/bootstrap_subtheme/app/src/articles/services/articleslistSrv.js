(function () {
  angular
    .module('sonnox.articles')
    .factory('articleslist', articleslist);

  articleslist.$inject = ['$log'];

  function articleslist($log) {

      return  {
        getlist : function () {
          $log.debug(Drupal.settings.sonnox.articleslist);
          return Drupal.settings.sonnox.articleslist;
        },
        getrelated : function () {
          $log.debug(Drupal.settings.sonnox.articleslist.related);
          return Drupal.settings.sonnox.articleslist.related.nodes;
        }
      };

    }


})();

