(function () {
  angular
    .module('sonnox.learnsection')
    .factory('relatedproducts', relatedproducts);

  relatedproducts.$inject = ['$log', '$http'];

  function relatedproducts($log, $http) {

      return  {
        get_related_products : function (nid) {
          return $http({ method: 'GET', url: '/json/relatedproducts/' + nid });
        }
      };

    }


})();
