(function () {
  angular
    .module('sonnox.page')
    .controller('landingCtr', landingCtr);


  landingCtr.$inject = ['$scope', '$log'];

  function landingCtr($scope, $log) {
    $scope.getCookie = function (cname)
    {
      var name = cname + '=';
      var ca = document.cookie.split(';');
      for (var i = 0; i < ca.length; i++) {
        var c = ca[i].trim();
        if (c.indexOf(name) === 0) { return c.substring(name.length, c.length); }
      }
      return '';
    };

    $scope.createCookie = function createCookie(name, value, days) {
      var expires  = null;
      if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = '; expires=' + date.toGMTString();
      } else {
        expires = '';
      }
      document.cookie = name +  '=' + value + expires + '; path=/';
    };

    $scope.showlanding = true;
    var landing = $scope.getCookie('landing');
    $log.debug(landing);
    if ((landing === 1) || (landing === '1')) {
      $scope.showlanding = true;
      $log.debug('landing == 1');
      //$('#landing').hide();
      $scope.createCookie('landing', 2);
    } else {
      $log.debug(landing);
      $scope.showlanding = false;
      //$('#landing').addClass('close');
      $('#landing').hide();
      $('#landing').remove();
      //$scope.createCookie('loading', );
    }

    var ua = navigator.userAgent.toLowerCase();
    var isAndroid = ua.indexOf('android') > -1; //&& ua.indexOf("mobile");
    if (isAndroid) {
      $('div').unbind('inview');
    }

    $('#landing').mousewheel(function (event, delta, deltaX, deltaY) {
                if (delta > 0) {
                  $('#landing').addClass('close');
                }
                if (deltaY < 0) {
                  $('#landing').addClass('close');
                }
                event.stopPropagation();
                event.preventDefault();
              });
    if (!Modernizr.cssanimations) {
    //jQuery fallback
      var loading = $('#landing');
      if (typeof(loading) !== 'undefined') {
        var height = loading.height();
        height = - (height - 10);
        loading.animate({opacity: 1 }, 8000,  function () {
          //$(this).css({display : 'none'});
          loading.animate({opacity: 0 }, 5000,  function () {
            $(this).css({display : 'none'});
          });
        });
      } else {
          //alert('cssanimations');
      }
    }

  }


})();


