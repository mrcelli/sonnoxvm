(function () {
    angular
        .module('sonnox.support')
        .directive('faqlistDrv', faqlistDrv);
    faqlistDrv.$inject = [  'faqsrv', '$log', '$document' ];

    function faqlistDrv(faqsrv, $log, $document) {

        return {
            restrict: 'E',
            templateUrl: 'support/views/faq-list.html',
            scope: {},
            link: link
          };

        function link(scope) {
          scope.$on('$destroy', function () {
              $document.off('scroll');
            });
          scope.faqlist = faqsrv.getlist();
          scope.category = {};
          scope.status = true;
          function escapeRegExp(string) {
              return string.replace(/([.*+?\^=!:${}()|\[\]\/\\])/g, '\\$1');
            }
          scope.faqOpen = function ($event) {
              $event.preventDefault();
              scope.status = true;
              $log.debug(scope);
              angular.forEach(scope.category, function (value, key) {
                //this.push(key + ': ' + value);
                scope.category[key] = true;
                value = value;
              });
            };
          scope.initcategory = function (category) {

            $log.debug('init category');
            if (typeof(scope.category[category]) === 'undefined') {
              scope.category[category] = false;
              //$log.debug('category');
              //$log.debug(scope.category);
            }

          };

          scope.filterBySearch = function (value) {

            var val = value.title;
            if (!scope.search) { return true; }
            var regex = new RegExp('\\b' + escapeRegExp(scope.search), 'i');
            return regex.test(val);
          };
          scope.faqClose = function ($event) {
            $event.preventDefault();
            scope.status = false;

            angular.forEach(scope.category, function (value, key) {
              //this.push(key + ': ' + value);
              $log.debug(key);
              scope.category[key] = false;
              value = value;
              key = key;
            });
            $log.debug(scope);

          };
          scope.setfaqstatus = function (faq, $event) {
            faq.view = !faq.view;
            $event.preventDefault();
            return faq.view;
          };




        }

      }




  }());
