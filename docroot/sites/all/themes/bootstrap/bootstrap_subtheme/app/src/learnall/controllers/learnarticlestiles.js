(function () {
  angular
    .module('sonnox.learnarticles')
    .controller('learnarticlestilesListCtr', learnarticlestilesListCtr);

  learnarticlestilesListCtr.$inject = ['$scope', '$rootScope', '$http', '$log', 'searchlearnarticles'];

  function learnarticlestilesListCtr($scope, $rootScope, $http, $log, searchlearnarticles) {
      $log.debug(searchlearnarticles);
      var result = searchlearnarticles.gettiles();
      $scope.terms = searchlearnarticles.getfilters();
      $log.debug($scope.terms);
      $scope.tiles =  result.tiles;
      $scope.totalpages = result.pager.pages;
      $scope.count = result.pager.count;
      $scope.currentpage = result.pager.page;
      $log.debug($scope);
      $rootScope.listscope = $scope;
      $scope.loading = false;

      $scope.termChanged = function () {
        $scope.termsvalues = $scope.termsselected.join(',');
        $log.debug($scope.termsvalues);
      };

      $scope.loadm = function () {
        if (searchlearnarticles.loading === true) { return; }
        if ($scope.totalpages === ($scope.currentpage + 1)) {
          searchlearnarticles.loading = false;
          $scope.loading = searchlearnarticles.loading;
          return;
        }
        $log.debug($scope.currentpage);
        $log.debug(searchlearnarticles.loading);
        $log.debug($scope.totalpages);
        searchlearnarticles.loading = true;
        $scope.loading = searchlearnarticles.loading;

        searchlearnarticles.set_page($scope.currentpage + 1);

        searchlearnarticles.loadmore().success(function (data) {
          $log.debug(data);
          $scope.tiles.push.apply($scope.tiles, data.nodes);
          $scope.totalpages = data.pager.pages;
          $scope.count = data.pager.count;
          $scope.currentpage = data.pager.page;
          $log.debug($scope.tiles);
          searchlearnarticles.loading = false;
          $scope.loading = searchlearnarticles.loading;
        }).error(function () {
          searchlearnarticles.loading = false;
          $scope.loading = searchlearnarticles.loading;
        });
      };



    }




}());
