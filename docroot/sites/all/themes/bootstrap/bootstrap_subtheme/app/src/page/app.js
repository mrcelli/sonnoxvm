(function () {
  'use strict';
  // Fix for naughty jQuery plugins.
  window.$ = window.jQuery;
  angular.module('sonnox.page', ['sonnox-templates', 'ui.bootstrap', 'infinite-scroll', 'sonnox.common']);
  angular.module('infinite-scroll').value('THROTTLE_MILLISECONDS', 250);
}());

