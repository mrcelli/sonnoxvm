(function () {
  angular
    .module('sonnox.common')
    .filter('doublequoteflt', doublequoteflt);

  doublequoteflt.$inject = [];

  function doublequoteflt() {
      return function (val) {
          return val.Replace('\'', '"');
        };
    }

})();

