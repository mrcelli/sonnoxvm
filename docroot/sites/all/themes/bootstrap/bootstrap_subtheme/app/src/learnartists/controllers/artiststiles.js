(function () {
  angular
    .module('sonnox.learnartists')
    .controller('artiststilesListCtr', artiststilesListCtr);

  artiststilesListCtr.$inject = ['$scope', '$rootScope', '$http', '$log', 'searchartists'];

  function artiststilesListCtr($scope, $rootScope, $http, $log, searchartists) {
      $log.debug(searchartists);
      var result = searchartists.gettiles();
      $scope.terms = searchartists.getfilters();
      $log.debug($scope.terms);
      $scope.tiles =  result.tiles;
      $scope.totalpages = result.pager.pages;
      $scope.count = result.pager.count;
      $scope.currentpage = result.pager.page;
      $log.debug($scope);
      $rootScope.listscope = $scope;
      $scope.loading = false;

      $scope.termChanged = function () {
        $scope.termsvalues = $scope.termsselected.join(',');
        $log.debug($scope.termsvalues);
      };

      $scope.loadm = function () {
        if (searchartists.loading === true) { return; }
        if ($scope.totalpages === ($scope.currentpage + 1)) {
          searchartists.loading = false;
          $scope.loading = searchartists.loading;
          return;
        }
        $log.debug($scope.currentpage);
        $log.debug(searchartists.loading);
        $log.debug($scope.totalpages);
        searchartists.loading = true;
        $scope.loading = searchartists.loading;

        searchartists.set_page($scope.currentpage + 1);

        searchartists.loadmore().success(function (data) {
          $log.debug(data);
          $scope.tiles.push.apply($scope.tiles, data.nodes);
          $scope.totalpages = data.pager.pages;
          $scope.count = data.pager.count;
          $scope.currentpage = data.pager.page;
          $log.debug($scope.tiles);
          searchartists.loading = false;
          $scope.loading = searchartists.loading;
        }).error(function () {
          searchartists.loading = false;
          $scope.loading = searchartists.loading;
        });
      };



    }




}());
