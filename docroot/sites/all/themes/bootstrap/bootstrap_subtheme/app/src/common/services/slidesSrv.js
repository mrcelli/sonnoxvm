(function () {
  angular
    .module('sonnox.common')
    .factory('slidesrv', slidesrv);
  slidesrv.$inject = ['$log'];
  function slidesrv($log) {
    $log.debug(Drupal);
    if (typeof Drupal.settings.sonnox === 'undefined') {
      return {
        slides : ['']
      };
    } else {
      return {
        slides : Drupal.settings.sonnox.slides
      };
    }
  }
}());
