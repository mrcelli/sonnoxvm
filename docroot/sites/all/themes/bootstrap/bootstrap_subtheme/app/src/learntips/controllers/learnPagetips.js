(function () {
  angular
    .module('sonnox.learntips')
    .controller('learnPagetips', learnPagetips);

  learnPagetips.$inject = ['$scope', '$log'];

  function learnPagetips($scope, $log) {
    $log.debug($scope);
    $scope.page_change = function () {
      $log.debug($scope.page_value);
      window.location.href =  $scope.page_value;
    };
  }

}());


