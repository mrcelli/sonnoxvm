(function () {
  angular
    .module('sonnox.learnarticles')
    .controller('learnPagelearnarticles', learnPagelearnarticles);

  learnPagelearnarticles.$inject = ['$scope', '$log'];

  function learnPagelearnarticles($scope, $log) {
    $log.debug($scope);
    $scope.page_change = function () {
      $log.debug($scope.page_value);
      window.location.href =  $scope.page_value;
    };
  }

}());
