(function () {
  angular
    .module('sonnox.learnarticles')
    .controller('learnarticlestilesListCtr', learnarticlestilesListCtr);

  learnarticlestilesListCtr.$inject = ['$scope', '$rootScope', '$http', '$log', 'searchlearnarticles'];

  function learnarticlestilesListCtr($scope, $rootScope, $http, $log, searcharticles) {
      $log.debug(searcharticles);
      var result = searcharticles.gettiles();
      $scope.terms = searcharticles.getfilters();
      $log.debug($scope.terms);
      $scope.tiles =  result.tiles;
      $scope.totalpages = result.pager.pages;
      $scope.count = result.pager.count;
      $scope.currentpage = result.pager.page;
      $log.debug($scope);
      $rootScope.listscope = $scope;
      $scope.loading = false;

      $scope.termChanged = function () {
        $scope.termsvalues = $scope.termsselected.join(',');
        $log.debug($scope.termsvalues);
      };

      $scope.loadm = function () {
        if (searcharticles.loading === true) { return; }
        if ($scope.totalpages === ($scope.currentpage + 1)) {
          searcharticles.loading = false;
          $scope.loading = searcharticles.loading;
          return;
        }
        $log.debug($scope.currentpage);
        $log.debug(searcharticles.loading);
        $log.debug($scope.totalpages);
        searcharticles.loading = true;
        $scope.loading = searcharticles.loading;

        searcharticles.set_page($scope.currentpage + 1);

        searcharticles.loadmore().success(function (data) {
          $log.debug(data);
          $scope.tiles.push.apply($scope.tiles, data.nodes);
          $scope.totalpages = data.pager.pages;
          $scope.count = data.pager.count;
          $scope.currentpage = data.pager.page;
          $log.debug($scope.tiles);
          searcharticles.loading = false;
          $scope.loading = searcharticles.loading;
        }).error(function () {
          searcharticles.loading = false;
          $scope.loading = searcharticles.loading;
        });
      };



    }




}());
