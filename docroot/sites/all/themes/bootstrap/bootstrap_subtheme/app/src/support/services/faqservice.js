(function () {
  angular
    .module('sonnox.support')
    .factory('faqsrv', faqsrv);

  faqsrv.$inject = ['$log'];

  function faqsrv($log) {

      return  {
        getshortlist : function () {
          $log.debug(Drupal.settings.sonnox);
          return Drupal.settings.sonnox.faqshortlist.faqs;
        },
        getlist: function () {
          //$log.debug(Drupal.settings.sonnox.faqlist.list);
          if (typeof(Drupal.settings.sonnox.faqlist) !== 'undefined') {
            return Drupal.settings.sonnox.faqlist.list;
          } else {
            return  null;
          }
        }
      };

    }


})();
