(function () {
  angular
    .module('sonnox.learntips')
    .controller('searchBoxtipsCtr', searchBoxtipsCtr);

  searchBoxtipsCtr.$inject = ['$scope', '$rootScope', '$http', '$log', 'searchtips'];

  function searchBoxtipsCtr($scope, $rootScope, $http, $log, searchtips) {
      $scope.loading = searchtips.loading;
      $scope.$watch(function () { return searchtips.loading; },
              function (newValue) {
                $scope.loading = newValue;
              }
             );

      $scope.search = function () {
        if (searchtips.loading ===  true) { return; }
        searchtips.set_q($scope.q);
        searchtips.set_page(0);
        searchtips.loading = true;
        $rootScope.listscope.loading = searchtips.loading;
        searchtips.loadmore().success(function (data) {
          $rootScope.listscope.loading = searchtips.loading;
          $log.debug(data);
          $rootScope.listscope.tiles = data.nodes;
          $rootScope.listscope.totalpages = data.pager.pages;
          $rootScope.listscope.count = data.pager.count;
          $rootScope.listscope.currentpage = data.pager.page;
          $log.debug($scope.tiles);
          searchtips.loading = false;
          $rootScope.listscope.loading = searchtips.loading;
        }).error(function () { searchtips.loading = false; $rootScope.listscope.loading = searchtips.loading; });
      };
    }




}());
