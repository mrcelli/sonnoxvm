(function () {
  angular
    .module('sonnox.support')
    .factory('howtovideosSrv', howtovideosSrv);

  howtovideosSrv.$inject = ['$log'];

  function howtovideosSrv($log) {

      return  {
        getlist : function () {
          $log.debug(Drupal.settings.sonnox);
          return Drupal.settings.sonnox.howtovideos.list;
        }
      };

    }


})();
