(function () {
  angular
    .module('sonnox.about')
    .factory('partnerssrv', partnerssrv);

  partnerssrv.$inject = ['$log'];

  function partnerssrv($log) {

      return  {
        getlist : function () {
          $log.debug(Drupal.settings.sonnox.partners);
          if (typeof(Drupal.settings.sonnox.partners.list) !== 'undefined') {
            return Drupal.settings.sonnox.partners.list;
          } else {
            return {};
          }
        }
      };

    }


})();

