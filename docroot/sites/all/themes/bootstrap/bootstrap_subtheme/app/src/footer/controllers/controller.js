(function () {
  angular
    .module('sonnox.footer')
    .controller('footerCtr', footerCtr);


  footerCtr.$inject = ['$scope', '$modal', '$timeout', '$http', '$log'];

  function footerCtr($scope, $modal, $timeout, $http, $log) {
      $scope.data = '';
      $scope.form = {};

      $scope.shownewsletter = function () {
        $scope.related = null;
        $modal.open({
          templateUrl: 'footer/views/newsletter.html',
          //windowTemplateUrl: 'learnsection/views/myDlgTemplateWrapper.html',
          controller: function ($scope, $modalInstance) {
            $modalInstance.opened.then(function () {
              $timeout(function () {

              }, 500);
            });
            $scope.submit = function () {
             // alert('submit');
              //$scope.email = 'submit';
              $scope.processForm();
            };
            $scope.showerrormessage = false;
            $scope.close = function () {
              $modalInstance.close();
              $scope.showerrormessage = false;
              $scope.showmessage = false;
            };
            $scope.showmessage = false;
            $scope.loading = false;
            $scope.processForm = function () {
              console.log($scope.formData);
              $scope.loading = true;
              var url = Drupal.settings.baseUrl + '/proxy.php';
              $http({
                method  : 'POST',
                withCredentials: true,
                url     : url,
                data    : $.param($scope.formData),  // pass in data as strings
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' }  // set the headers so angular passing info as form data (not request payload)
              })
                .success(function (data) {
                  $log.debug(data);
                  //console.log(data);

                  if (!data) {
                    // if not successful, bind errors to error variables
                   // $scope.errorName = data.errors.name;
                    //$scope.errorSuperhero = data.errors.superheroAlias;
                    $scope.message = 'here was an error, please check your internet connection.';
                  } else {
                    // if successful, bind success message to message
                    if (data.Result === 'OK') {
                      $scope.showerrormessage = false;
                      $scope.showmessage = true;
                      $scope.message = data.Message;
                    }
                    if (data.Result === 'Fail') {
                      $scope.showerrormessage = true;
                      $scope.errormessage = data.Message;
                    }
                  }
                  $scope.loading = false;
                })
                .error(function (data, status) {
                  console.error('Repos error', status, data);
                  $scope.showerrormessage = true;
                  $scope.message = data.Message;
                  $scope.loading = false;
                });

            };
          }
        });
      };


    }


})();
