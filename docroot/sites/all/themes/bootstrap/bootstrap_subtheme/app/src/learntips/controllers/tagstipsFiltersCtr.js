(function () {
  angular.module('sonnox.learntips')
  .controller('tagstipsFiltersCtr', tagstipsFiltersCtr);

  tagstipsFiltersCtr.$inject = ['$scope', '$rootScope', '$document', 'searchtips', '$log'];

  function tagstipsFiltersCtr($scope, $rootScope, $document, searchtips, $log) {
    // add
    var termlist = [];
    searchtips.tearmlist = termlist;
    var comparator = angular.equals;

    function contains(arr, item, comparator) {
      if (angular.isArray(arr)) {
        for (var i = arr.length; i--;) {
          if (comparator(arr[i], item)) {
            return true;
          }
        }
      }
      return false;
    }

    function add(arr, item, comparator) {
      arr = angular.isArray(arr) ? arr : [];
      if (!contains(arr, item, comparator)) {
        arr.push(item);
      }
      return arr;
    }

    // remove
    function remove(arr, item, comparator) {
      if (angular.isArray(arr)) {
        for (var i = arr.length; i--;) {
          if (comparator(arr[i], item)) {
            arr.splice(i, 1);
            break;
          }
        }
      }
      return arr;
    }
    $rootScope.listscope.loading = searchtips.loading;
    $scope.loading = searchtips.loading;
    $scope.loadtips = function () {
        if (searchtips.loading ===  true) { return; }
       // searchtips.set_q($scope.q);
        searchtips.set_page(0);
        searchtips.loading = true;
        $rootScope.listscope.loading = searchtips.loading;

        searchtips.loadmore().success(function (data) {
          $log.debug(data);
          $rootScope.listscope.tiles = data.nodes;
          $rootScope.listscope.totalpages = data.pager.pages;
          $rootScope.listscope.count = data.pager.count;
          $rootScope.listscope.currentpage = data.pager.page;
          $log.debug($scope.tiles);
          searchtips.loading = false;
          $rootScope.listscope.loading = searchtips.loading;
          $scope.loading = $rootScope.listscope.loading;
        }).error(function () { searchtips.loading = false; $rootScope.listscope.loading = searchtips.loading; $scope.loading = $rootScope.listscope.loading; });
      };

    $scope.toggletipsterm = function (tid) {
      if (searchtips.loading === true) { return; }
      termlist = add(termlist, tid, comparator);
      var trm = $document[0].getElementById('term-' + tid);
      if (angular.element(trm).hasClass('active')) {
        termlist = remove(termlist, tid, comparator);
        angular.element(trm).removeClass('active');
      } else {
        termlist = add(termlist, tid, comparator);
        angular.element(trm).addClass('active');
      }
      $scope.loadtips();
      $log.debug(termlist);
    };

  }


})();
