(function () {
  angular
    .module('sonnox.learnarticles')
    .factory('learnarticleslist', learnarticleslist);

  learnarticleslist.$inject = ['$log'];

  function learnarticleslist($log) {

      return  {
        getlist : function () {
          $log.debug(Drupal.settings.sonnox.learnarticleslist);
          return Drupal.settings.sonnox.learnarticleslist.learnarticles.nodes;
        },
        getrelated : function () {
          $log.debug(Drupal.settings.sonnox.learnarticleslist.related);
          return Drupal.settings.sonnox.learnarticleslist.related.nodes;
        }
      };

    }


})();
