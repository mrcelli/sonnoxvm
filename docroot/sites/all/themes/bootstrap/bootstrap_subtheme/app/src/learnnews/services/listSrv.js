(function () {
  angular
    .module('sonnox.learnsection')
    .factory('newslist', newslist);

  newslist.$inject = ['$log'];

  function newslist($log) {

      return  {
        getlist : function () {
          $log.debug(Drupal.settings.sonnox.newslist);
          return Drupal.settings.sonnox.newslist.news.nodes;
        },
        getrelated : function () {
          $log.debug(Drupal.settings.sonnox);
          return Drupal.settings.sonnox.newslist.related.nodes;
        }
      };

    }


})();
