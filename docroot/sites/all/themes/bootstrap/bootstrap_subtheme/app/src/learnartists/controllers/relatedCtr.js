(function () {
  angular
    .module('sonnox.learnartists')
    .controller('artistsrelatedCtr', artistsrelatedCtr);


  artistsrelatedCtr.$inject = ['$scope',  'artistslist'];

  function artistsrelatedCtr($scope, artistslist) {
      $scope.related = artistslist.getrelated();
    }
})();
