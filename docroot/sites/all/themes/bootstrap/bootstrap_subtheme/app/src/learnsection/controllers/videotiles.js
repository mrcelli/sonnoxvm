(function () {
  angular
    .module('sonnox.learnsection')
    .controller('videotilesListCtr', videotilesListCtr);

  videotilesListCtr.$inject = ['$scope', '$rootScope', '$http', '$log', 'searchvideos', '$modal', '$timeout', 'relatedproducts'];

  function videotilesListCtr($scope, $rootScope, $http, $log, searchvideos, $modal, $timeout, relatedproducts) {
      $log.debug(searchvideos);
      var result = searchvideos.gettiles();
      $scope.terms = searchvideos.getfilters();
      $log.debug($scope.terms);
      $scope.tiles =  result.tiles;
      $scope.totalpages = result.pager.pages;
      $scope.count = result.pager.count;
      $scope.currentpage = result.pager.page;
      $log.debug($scope);
      $rootScope.listscope = $scope;
      $scope.loading = false;

      $scope.termChanged = function () {
        $scope.termsvalues = $scope.termsselected.join(',');
        $log.debug($scope.termsvalues);
      };
      $scope.showvideo = function (tile) {
        $scope.related = null;
        $modal.open({
          templateUrl: 'learnsection/views/videomodal.html',
          windowTemplateUrl: 'learnsection/views/myDlgTemplateWrapper.html',
          controller: function ($scope, $modalInstance) {
            $log.debug(tile);
            $scope.related = null;
            var videourl = tile.node.field_video_id;
            $scope.title = tile.node.title;
            $scope.body = tile.node.body;
            var v = videourl; //videourl.replace('http://www.youtube.com/watch?v=', '');
            $log.debug(v);
            $scope.v = v;
            $scope.videourl =  'https://www.youtube.com/embed/' + v + '?autoplay=1&enablejsapi=1&autohide=1&theme=light';//;?autoplay=1&autohide=1&rel=0&fs=1&enablejsapi=1';


            $log.debug(tile.node.nid);
            relatedproducts.get_related_products(tile.node.nid).success(function (data) {
              $log.debug(data.nodes);
              $scope.related = data.nodes;
            });
            $modalInstance.opened.then(function () {
              $timeout(function () {
            //    fluidIframe(v);
              }, 500);
            });
            $scope.close = function () {
              $modalInstance.close();
            };
          }
        });
      };
      $scope.loadm = function () {
        if (searchvideos.loading === true) { return; }
        if ($scope.totalpages === ($scope.currentpage + 1)) {
          searchvideos.loading = false;
          $scope.loading = searchvideos.loading;
          return;
        }
        $log.debug($scope.currentpage);
        $log.debug(searchvideos.loading);
        $log.debug($scope.totalpages);
        searchvideos.loading = true;
        $scope.loading = searchvideos.loading;

        searchvideos.set_page($scope.currentpage + 1);

        searchvideos.loadmore().success(function (data) {
          $log.debug(data);
          $scope.tiles.push.apply($scope.tiles, data.nodes);
          $scope.totalpages = data.pager.pages;
          $scope.count = data.pager.count;
          $scope.currentpage = data.pager.page;
          $log.debug($scope.tiles);
          searchvideos.loading = false;
          $scope.loading = searchvideos.loading;
        }).error(function () {
          searchvideos.loading = false;
          $scope.loading = searchvideos.loading;
        });
      };


    }




}());
