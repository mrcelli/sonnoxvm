(function () {
  angular
    .module('sonnox.common')
    .factory('isMobile', isMobile);
  isMobile.$inject = ['$log'];


  function isMobile($log) {

    var obj  = {
      t : function () {
          return 'test';
        },
      Android : function () {
          return navigator.userAgent.match(/Android/i);
        },
      BlackBerry : function () {
          return navigator.userAgent.match(/BlackBerry/i);
        },
      iOS : function () {
          return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
      Opera : function () {
          return navigator.userAgent.match(/Opera Mini/i);
        },
      Windows : function () {
          return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
        },
      any : function () {
          var p = obj;
          $log.debug('any');
          $log.debug(p);
          return (p.Android() || p.BlackBerry() || p.iOS() || p.Opera() || p.Windows());
        }
    };


    return obj;
  }
}());
