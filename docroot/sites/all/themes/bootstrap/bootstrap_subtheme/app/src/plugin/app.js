(function () {
  'use strict';
  // Fix for naughty jQuery plugins.
  window.$ = window.jQuery;
  angular.module('sonnox.plugin', ['youtube-embed', 'sonnox.common']);

}());

