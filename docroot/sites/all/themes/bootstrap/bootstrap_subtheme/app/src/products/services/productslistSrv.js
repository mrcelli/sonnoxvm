(function () {
  angular
    .module('sonnox.products')
    .factory('productslistsrv', productslistsrv);

  productslistsrv.$inject = ['$log'];

  function productslistsrv($log) {

      return  {
        getlist : function () {
          $log.debug(Drupal.settings.sonnox.productslist);
          return Drupal.settings.sonnox.productslist.nodes;
        },
        getrelated : function () {
          $log.debug(Drupal.settings.sonnox.articleslist.related);
          return Drupal.settings.sonnox.articleslist.related.nodes;
        }
      };

    }


})();

