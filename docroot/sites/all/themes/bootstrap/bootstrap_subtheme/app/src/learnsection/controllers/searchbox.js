(function () {
  angular
    .module('sonnox.learnsection')
    .controller('searchBoxCtr', searchBoxCtr);

  searchBoxCtr.$inject = ['$scope', '$rootScope', '$http', '$log',  'searchvideos'];

  function searchBoxCtr($scope, $rootScope, $http, $log, searchvideos) {

      $scope.loading = searchvideos.loading;
      $scope.$watch(function () { return searchvideos.loading; },
              function (newValue) {
                $scope.loading = newValue;
              }
             );

      $scope.search = function () {

        if (searchvideos.loading ===  true) { return; }
        searchvideos.set_q($scope.q);
        searchvideos.set_page(0);
        searchvideos.loading = true;

        $rootScope.listscope.loading = searchvideos.loading;
        searchvideos.loadmore().success(function (data) {
          $rootScope.listscope.loading = searchvideos.loading;
          $log.debug(data);
          $rootScope.listscope.tiles = data.nodes;
          $rootScope.listscope.totalpages = data.pager.pages;
          $rootScope.listscope.count = data.pager.count;
          $rootScope.listscope.currentpage = data.pager.page;
          $log.debug($scope.tiles);
          searchvideos.loading = false;
          $rootScope.listscope.loading = searchvideos.loading;
        }).error(function () { searchvideos.loading = false; $rootScope.listscope.loading = searchvideos.loading; });
      };
    }




}());
