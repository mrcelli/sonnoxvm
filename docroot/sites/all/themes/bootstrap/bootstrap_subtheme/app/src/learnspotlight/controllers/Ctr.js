(function () {
  angular
    .module('sonnox.learnspotlight')
    .controller('spotlightCtr', spotlightCtr);


  spotlightCtr.$inject = ['$scope',  'spotlightlist'];

  function spotlightCtr($scope, spotlightlist) {
      $scope.spotlight = spotlightlist.getlist();
    }
})();
