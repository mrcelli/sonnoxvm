/**
 * Checklist-model
 * AngularJS directive for list of checkboxes
 */
(function () {
  angular.module('sonnox.articles')
  .directive('buttonlistModel', buttonlistModel);

  buttonlistModel.$inject = ['$parse', '$log'];

    // contains
  function buttonlistModel($parse, $log) {

    return {
      restrict: 'A',
      scope: true,
      link: LinkFn
    };
    function contains(arr, item, comparator) {
      if (angular.isArray(arr)) {
        for (var i = arr.length; i--;) {
          if (comparator(arr[i], item)) {
            return true;
          }
        }
      }
      return false;
    }

    // add
    function add(arr, item, comparator) {
      arr = angular.isArray(arr) ? arr : [];
      if (!contains(arr, item, comparator)) {
        arr.push(item);
      }
      return arr;
    }

    // remove
    function remove(arr, item, comparator) {
      if (angular.isArray(arr)) {
        for (var i = arr.length; i--;) {
          if (comparator(arr[i], item)) {
            arr.splice(i, 1);
            break;
          }
        }
      }
      return arr;
    }

    // http://stackoverflow.com/a/19228302/1458162
    function LinkFn(scope, elem, attr) {


      var comparator = angular.equals;

      scope.termlist = [];

      elem.on('click', function () {
          if (elem.hasClass('active')) {
            elem.addClass('active');
            scope.termlist = add(scope.termlist, attr.termvalue, comparator);
            $log.debug(scope);
          } else {
            elem.removeClass('active');
            scope.termlist = remove(scope.termlist, attr.termvalue, comparator);
            $log.debug(scope);
          }
          $log.debug('click');
        });

    }


  }

})();
