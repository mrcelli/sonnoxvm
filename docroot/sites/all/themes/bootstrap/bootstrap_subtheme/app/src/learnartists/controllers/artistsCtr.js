(function () {
  angular
    .module('sonnox.learnartists')
    .controller('artistsCtr', artistsCtr);


  artistsCtr.$inject = ['$scope',  'artistslist'];

  function artistsCtr($scope, artistslist) {
      $scope.artists = artistslist.getlist();
    }
})();
