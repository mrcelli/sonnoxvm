(function () {
  angular
    .module('sonnox.support')
    .controller('supportCtr', supportCtr);


  supportCtr.$inject = ['$scope', 'faqsrv', 'supnewsSrv', 'howtovideosSrv', '$modal', '$log'];

  function supportCtr($scope, faqsrv, supnewsSrv, howtovideosSrv, $modal, $log) {
      $scope.faqshortlist = faqsrv.getshortlist();
      //$scope.faqlist = faqsrv.getlist();
      $scope.supportnews = supnewsSrv.getshortlist();
      $scope.howtovideos = howtovideosSrv.getlist();
      function escapeRegExp(string) {
          return string.replace(/([.*+?\^=!:${}()|\[\]\/\\])/g, '\\$1');
        }
      $scope.showvideo = function (item) {
        $scope.related = null;
        $modal.open({
          templateUrl: 'support/views/videomodal.html',
          windowTemplateUrl: 'learnsection/views/myDlgTemplateWrapper.html',
          controller: function ($scope, $modalInstance) {
            $log.debug(item);
            $scope.related = null;
            var videourl = item.node.field_howto_video;
            $scope.title = item.node.title;
            $scope.body = item.node.body;

            var v = videourl.replace('http://www.youtube.com/watch?v=', '');
            v = v.replace('https://www.youtube.com/watch?v=', '');
            $log.debug(v);
            $scope.v = v;
            $scope.videourl =  'https://www.youtube.com/embed/' + v + '?autoplay=1&enablejsapi=1&autohide=1&theme=light';//;?autoplay=1&autohide=1&rel=0&fs=1&enablejsapi=1';


            $scope.close = function () {
              $modalInstance.close();
            };
          }
        });
      };
      $scope.category = {};
      $scope.status = true;
      $scope.faqOpen = function ($event) {
        $event.preventDefault();
        $scope.status = true;
        $log.debug($scope);
        angular.forEach($scope.category, function (value, key) {
          //this.push(key + ': ' + value);
          $scope.category[key] = true;
          value = value;
        });

      };
      $scope.initcategory = function (category) {

        $log.debug('init category');
        if (typeof($scope.category[category]) === 'undefined') {
          $scope.category[category] = false;
          $log.debug('category');
          $log.debug($scope.category);
        }

      };

      $scope.filterBySearch = function (value) {

        var val = value.title;
        if (!$scope.search) { return true; }
        var regex = new RegExp('\\b' + escapeRegExp($scope.search), 'i');
        return regex.test(val);
      };
      $scope.faqClose = function ($event) {
        $event.preventDefault();
        $scope.status = false;

        angular.forEach($scope.category, function (value, key) {
          //this.push(key + ': ' + value);
          $log.debug(key);
          $scope.category[key] = false;
          value = value;
          key = key;
        });
        $log.debug($scope);

      };
      $scope.setfaqstatus = function (faq, $event) {
        faq.view = !faq.view;
        $event.preventDefault();
        return faq.view;
      };


    }



})();
