(function () {
  angular
    .module('djc.tiles')
    .factory('searchtiles', searchtiles);

  searchtiles.$inject = ['$log', '$http'];

  function searchtiles($log, $http) {

    return  {
      gettiles : function () {
                        return Drupal.settings.djc.result;
                      },
      loadmore  : function (currentpage) {
          $log.debug(currentpage);
          return $http({method: 'GET', url: 'api/teasers?',   params: { page : currentpage + 1 } }); /*.success(function (data) {
            $log.debug(data);
            return data;

          }).error(function (data, status, headers, config) {
            $log.error(data);
            $log.error(status);
            $log.error(headers);
            $log.error(config);
          }); */
        }
    };

  }





}());
