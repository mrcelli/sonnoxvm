(function () {
    angular
        .module('sonnox.articles')
        .directive('articleshortlist', articleshortlist);
    articleshortlist.$inject = ['$timeout', '$log', '$http' ];

    function articleshortlist() {

        return {
            restrict: 'E',
            templateUrl: 'articles/views/articles-short-list.html',
            link: link
          };
      }

    function link() {


    }


  }());

(function () {
    angular
        .module('sonnox.articles')
        .directive('slideshow', slideshow);
    slideshow.$inject = [];

    function slideshow() {

        return {
            restrict: 'E',
            templateUrl: 'articles/views/slideshow.html',
            link: link
          };
      }

    function link() {


    }


  }());
