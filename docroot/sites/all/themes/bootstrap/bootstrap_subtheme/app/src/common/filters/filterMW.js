/* jshint ignore:start */
(function () {
  angular
    .module('sonnox.common')
    .filter('filterMW', filterMW);

  filterMW.$inject = ['$filter', '$log'];


  function filterMW($filter, $log) {

    function filterOrTerm(item, stringKeys, searchTerm) {

      //$log.debug(item);
      var result = null;
      angular.forEach(searchTerm, function (term) {
        //$log.debug('term');
        //$log.debug(term);
        angular.forEach(stringKeys, function (key) {
          //$log.debug('key');
          //$log.debug(key);
          //$log.debug(item[key]);
          term = term.toLowerCase();
          //$log.debug(term);
          //$log.debug(key);
          //$log.debug(item[key]);
          //$log.debug(item[key].toLowerCase().indexOf(term));
          if (item[key].toLowerCase().indexOf(term) !== -1) {
            //alert(key);
            //$log.debug(item);
            //$log.debug(' return item  ');
            result =  item;
          }
        });
       // return null;
      });
      return result;
    }
    return function (obj, searchProperties, searchQuery, searchQueryDelimiter) {
      //var filterBy = $filter('filterBy');
      var delimiter = searchQueryDelimiter || ' ';
      var hasSearchTerm =  (typeof(searchQuery) !== 'undefined');
      var hasSearchProperties = (typeof(searchProperties) !== 'undefined') && searchProperties.lenght !== 0;
      var searchTerms = hasSearchTerm ? searchQuery.split(delimiter) : [];
      var stringKeys = [];
      var list = obj;

      // Bail out if there is no work to do
      if (!hasSearchTerm || list.length === 0) {
        return obj;
      }
      //$log.debug(searchTerms);
      stringKeys = hasSearchProperties ? searchProperties : Object.keys(list);

      // use the terms as an AND filter
      var resultlist = [];
     // var  filterResult = $filter('orFilter')(list, , true);
      //$log.debug(filterResult);

      angular.forEach(list, function (item) {
        var result = null;
        //angular.forEach(searchTerms, function (searchTerm) {
        var itemtoadd = filterOrTerm(item, stringKeys, searchTerms);
        $log.debug(itemtoadd);
        if (typeof(itemtoadd) !== 'undefined') {
          result = itemtoadd;
        }
        //});
        if (result !== null) {
          resultlist.push(result); // = filterBy(list, stringKeys, searchTerm);
        }
      });

      return resultlist;
    };

  }
}());
/* jshint ignore:end */
