(function () {
  angular
    .module('sonnox.learnarticles')
    .controller('learnarticlesCtr', learnarticlesCtr);


  learnarticlesCtr.$inject = ['$scope',  'learnarticleslist'];

  function learnarticlesCtr($scope, learnarticleslist) {
      $scope.learnarticles = learnarticleslist.getlist();
    }
})();
