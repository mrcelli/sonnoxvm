// http://stackoverflow.com/questions/20068526/angularjs-directive-does-not-update-on-scope-variable-changes
// http://blog.thoughtram.io/angularjs/2015/01/02/exploring-angular-1.3-bindToController.html
//http://stackoverflow.com/questions/28622489/angularjs-access-scope-variable-inside-directive-template-and-update-controlle
(function () {
    angular
        .module('sonnox.products')
        .directive('productsList', productsList);
    productsList.$inject = ['$compile', '$log', '$templateCache', '$timeout', '$http' ];

    function productsList($compile, $log) {
        var directive = {
            restrict: 'E',
            scope : true,
            link: function ($scope, element) {
                $log.debug($scope.display);
                element.append($compile('<ng-include src="\'' + $scope.display + '\'"></ng-include>')($scope));
                $scope.$watch('display', function (value) {
                      $log.debug(value);
                      element.html($compile('<ng-include src="\'' + value + '\'"></ng-include>')($scope));
                    });
              }
          };



        return directive;
      }



  }());
