(function () {
  angular
    .module('sonnox.learnnews')
    .controller('newstilesListCtr', newstilesListCtr);

  newstilesListCtr.$inject = ['$scope', '$rootScope', '$http', '$log', 'searchnews'];

  function newstilesListCtr($scope, $rootScope, $http, $log, searchnews) {
      $log.debug(searchnews);
      var result = searchnews.gettiles();
      $scope.terms = searchnews.getfilters();
      $log.debug($scope.terms);
      $scope.tiles =  result.tiles;
      $scope.totalpages = result.pager.pages;
      $scope.count = result.pager.count;
      $scope.currentpage = result.pager.page;
      $log.debug($scope);
      $rootScope.listscope = $scope;
      $scope.loading = false;

      $scope.termChanged = function () {
        $scope.termsvalues = $scope.termsselected.join(',');
        $log.debug($scope.termsvalues);
      };

      $scope.loadm = function () {
        if (searchnews.loading === true) { return; }
        if ($scope.totalpages === ($scope.currentpage + 1)) {
          searchnews.loading = false;
          $scope.loading = searchnews.loading;
          return;
        }
        $log.debug($scope.currentpage);
        $log.debug(searchnews.loading);
        $log.debug($scope.totalpages);
        searchnews.loading = true;
        $scope.loading = searchnews.loading;

        searchnews.set_page($scope.currentpage + 1);

        searchnews.loadmore().success(function (data) {
          $log.debug(data);
          $scope.tiles.push.apply($scope.tiles, data.nodes);
          $scope.totalpages = data.pager.pages;
          $scope.count = data.pager.count;
          $scope.currentpage = data.pager.page;
          $log.debug($scope.tiles);
          searchnews.loading = false;
          $scope.loading = searchnews.loading;
        }).error(function () {
          searchnews.loading = false;
          $scope.loading = searchnews.loading;
        });
      };



    }




}());
