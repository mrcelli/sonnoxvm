(function () {
  'use strict';
  // Fix for naughty jQuery plugins.
  window.$ = window.jQuery;
  angular.module('sonnox.about', ['sonnox-templates', 'ui.bootstrap',  'ngMap']);
}());

