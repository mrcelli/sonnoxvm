(function () {
  angular
    .module('sonnox.learnsection')
    .factory('spotlightlist', spotlightlist);

  spotlightlist.$inject = ['$log'];

  function spotlightlist($log) {

      return  {
        getlist : function () {
          $log.debug(Drupal.settings.sonnox.spotlightlist);
          return Drupal.settings.sonnox.spotlightlist.spotlight.nodes;
        },
        getrelated : function () {
          $log.debug(Drupal.settings.sonnox.spotlightlist.related);
          return Drupal.settings.sonnox.spotlightlist.related.nodes;
        }
      };

    }


})();
