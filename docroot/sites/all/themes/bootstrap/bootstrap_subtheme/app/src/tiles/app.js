(function () {
  'use strict';
  // Fix for naughty jQuery plugins.
  window.$ = window.jQuery;
  angular.module('learn.tiles', ['sonnox-templates']);
}());
