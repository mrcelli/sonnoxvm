(function () {
  'use strict';
  // Fix for naughty jQuery plugins.
  window.$ = window.jQuery;
  angular.module('sonnox.learnspotlight', ['sonnox-templates', 'sonnox.common', 'ui.bootstrap', 'infinite-scroll']);
  //angular.module('infinite-scroll').value('THROTTLE_MILLISECONDS', 250);
}());

