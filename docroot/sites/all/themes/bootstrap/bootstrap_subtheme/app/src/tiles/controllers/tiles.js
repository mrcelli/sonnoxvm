(function () {
  angular
    .module('djc.tiles')
    .controller('tilesListCtr', tilesListCtr);

  tilesListCtr.$inject = ['$scope', '$http', '$log', 'searchtiles'];

  function tilesListCtr($scope, $http, $log, searchtiles) {
      $log.debug(searchtiles);
      var result = searchtiles.gettiles();
      $scope.tiles =  result.tiles;
      $scope.totalpages = result.pager.pages;
      $scope.count = result.pager.count;
      $scope.currentpage = result.pager.page;
      $log.debug($scope);
      $scope.loadm = function () {
        searchtiles.loadmore($scope.currentpage).success(function (data) {
          $log.debug(data);
          angular.extend($scope.tiles, angular.copy(data.nodes));
          $scope.totalpages = data.pager.pages;
          $scope.count = data.pager.count;
          $scope.currentpage = data.pager.page;
          $log.debug($scope.tiles);
        });
      };
    }




}());
