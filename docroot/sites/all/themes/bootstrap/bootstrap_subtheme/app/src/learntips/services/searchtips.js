(function () {
  angular
    .module('sonnox.learntips')
    .factory('searchtips', searchtips);

  searchtips.$inject = ['$log', '$http'];

  function searchtips($log, $http) {
    var page;

    return  {
      loading : false,
      gettiles : function () {
                        return Drupal.settings.sonnox.tipsresult;
                      },
      getfilters: function () {
                        return Drupal.settings.sonnox.tipsfilters.terms;
                      },
      loadmore  : function () {
          $log.debug(page);
          //var termsquery = this.tearmlist.join('+');
          $log.debug(this.q);
          $log.debug(this.tearmlist);
          return $http({method: 'GET', url: '/json/tips?',   params: { page :  page, query : this.q, 'taxonomytid[]' : this.tearmlist} });

        },
      set_q : function (query) {
        this.q = query;
      },
      get_q : function () {
        return this.q;
      },
      set_page : function (p) {
        page = p;
      },
      get_page : function () {
        return page;
      },
      tearmlist : [],
      q : ''

    };

  }





}());
