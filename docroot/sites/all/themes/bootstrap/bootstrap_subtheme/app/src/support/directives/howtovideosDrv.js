(function () {
    angular
        .module('sonnox.support')
        .directive('howtovideosDrv', howtovideosDrv);
    howtovideosDrv.$inject = [  '$http', '$timeout', '$log' ];

    function howtovideosDrv() {

        return {
            restrict: 'E',
            templateUrl: 'support/views/howtovideos-list.html',
            link: link
          };

      }

    function link(scope) {
      scope.baseUrl = Drupal.settings.baseUrl;

    }


  }());
