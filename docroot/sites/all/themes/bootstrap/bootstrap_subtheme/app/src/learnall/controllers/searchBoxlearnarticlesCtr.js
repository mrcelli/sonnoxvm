(function () {
  angular
    .module('sonnox.learnarticles')
    .controller('searchall', searchall);

  searchall.$inject = ['$scope', '$rootScope', '$http', '$log', 'searchlearnarticles'];

  function searchall($scope, $rootScope, $http, $log, searchlearnarticles) {
      $scope.loading = searchlearnarticles.loading;
      $scope.$watch(function () { return searchlearnarticles.loading; },
              function (newValue) {
                $scope.loading = newValue;
              }
             );

      $scope.search = function () {
        if (searchlearnarticles.loading ===  true) { return; }
        searchlearnarticles.set_q($scope.q);
        searchlearnarticles.set_page(0);
        searchlearnarticles.loading = true;
        $rootScope.listscope.loading = searchlearnarticles.loading;
        searchlearnarticles.loadmore().success(function (data) {
          $rootScope.listscope.loading = searchlearnarticles.loading;
          $log.debug(data);
          $rootScope.listscope.tiles = data.nodes;
          $rootScope.listscope.totalpages = data.pager.pages;
          $rootScope.listscope.count = data.pager.count;
          $rootScope.listscope.currentpage = data.pager.page;
          $log.debug($scope.tiles);
          searchlearnarticles.loading = false;
          $rootScope.listscope.loading = searchlearnarticles.loading;
        }).error(function () { searchlearnarticles.loading = false; $rootScope.listscope.loading = searchlearnarticles.loading; });
      };
    }




}());
