(function () {
  angular.module('sonnox.learnarticles')
  .controller('tagslearnarticlesFiltersCtr', tagslearnarticlesFiltersCtr);

  tagslearnarticlesFiltersCtr.$inject = ['$scope', '$rootScope', '$document', 'searchlearnarticles', '$log'];

  function tagslearnarticlesFiltersCtr($scope, $rootScope, $document, searchlearnarticles, $log) {
    // add
    var termlist = [];
    searchlearnarticles.tearmlist = termlist;
    var comparator = angular.equals;

    function contains(arr, item, comparator) {
      if (angular.isArray(arr)) {
        for (var i = arr.length; i--;) {
          if (comparator(arr[i], item)) {
            return true;
          }
        }
      }
      return false;
    }

    function add(arr, item, comparator) {
      arr = angular.isArray(arr) ? arr : [];
      if (!contains(arr, item, comparator)) {
        arr.push(item);
      }
      return arr;
    }

    // remove
    function remove(arr, item, comparator) {
      if (angular.isArray(arr)) {
        for (var i = arr.length; i--;) {
          if (comparator(arr[i], item)) {
            arr.splice(i, 1);
            break;
          }
        }
      }
      return arr;
    }
    $rootScope.listscope.loading = searchlearnarticles.loading;
    $scope.loading = searchlearnarticles.loading;
    $scope.loadlearnarticles = function () {
        if (searchlearnarticles.loading ===  true) { return; }
       // searchlearnarticles.set_q($scope.q);
        searchlearnarticles.set_page(0);
        searchlearnarticles.loading = true;
        $rootScope.listscope.loading = searchlearnarticles.loading;

        searchlearnarticles.loadmore().success(function (data) {
          $log.debug(data);
          $rootScope.listscope.tiles = data.nodes;
          $rootScope.listscope.totalpages = data.pager.pages;
          $rootScope.listscope.count = data.pager.count;
          $rootScope.listscope.currentpage = data.pager.page;
          $log.debug($scope.tiles);
          searchlearnarticles.loading = false;
          $rootScope.listscope.loading = searchlearnarticles.loading;
          $scope.loading = $rootScope.listscope.loading;
        }).error(function () { searchlearnarticles.loading = false; $rootScope.listscope.loading = searchlearnarticles.loading; $scope.loading = $rootScope.listscope.loading; });
      };

    $scope.togglelearnarticlesterm = function (tid) {
      if (searchlearnarticles.loading === true) { return; }
      termlist = add(termlist, tid, comparator);
      var trm = $document[0].getElementById('term-' + tid);
      if (angular.element(trm).hasClass('active')) {
        termlist = remove(termlist, tid, comparator);
        angular.element(trm).removeClass('active');
      } else {
        termlist = add(termlist, tid, comparator);
        angular.element(trm).addClass('active');
      }
      $scope.loadlearnarticles();
      $log.debug(termlist);
    };

  }


})();
