(function () {
  angular
    .module('sonnox.learnnews')
    .controller('newsCtr', newsCtr);


  newsCtr.$inject = ['$scope',  'newslist'];

  function newsCtr($scope, newslist) {
      $scope.news = newslist.getlist();
    }
})();
