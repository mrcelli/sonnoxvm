(function () {
  angular
    .module('sonnox.learnsection')
    .factory('tipslist', tipslist);

  tipslist.$inject = ['$log'];

  function tipslist($log) {

      return  {
        getlist : function () {
          $log.debug(Drupal.settings.sonnox.tipslist);
          return Drupal.settings.sonnox.tipslist.tips.nodes;
        },
        getrelated : function () {
          $log.debug(Drupal.settings.sonnox.tipslist.related);
          return Drupal.settings.sonnox.tipslist.related.nodes;
        }
      };

    }


})();
