(function () {
  angular
    .module('sonnox.learnspotlight')
    .controller('spotlighttilesListCtr', spotlighttilesListCtr);

  spotlighttilesListCtr.$inject = ['$scope', '$rootScope', '$http', '$log', 'searchspotlight'];

  function spotlighttilesListCtr($scope, $rootScope, $http, $log, searchspotlight) {
      $log.debug(searchspotlight);
      var result = searchspotlight.gettiles();
      $scope.terms = searchspotlight.getfilters();
      $log.debug($scope.terms);
      $scope.tiles =  result.tiles;
      $scope.totalpages = result.pager.pages;
      $scope.count = result.pager.count;
      $scope.currentpage = result.pager.page;
      $log.debug($scope);
      $rootScope.listscope = $scope;
      $scope.loading = false;

      $scope.termChanged = function () {
        $scope.termsvalues = $scope.termsselected.join(',');
        $log.debug($scope.termsvalues);
      };

      $scope.loadm = function () {
        if (searchspotlight.loading === true) { return; }
        if ($scope.totalpages === ($scope.currentpage + 1)) {
          searchspotlight.loading = false;
          $scope.loading = searchspotlight.loading;
          return;
        }
        $log.debug($scope.currentpage);
        $log.debug(searchspotlight.loading);
        $log.debug($scope.totalpages);
        searchspotlight.loading = true;
        $scope.loading = searchspotlight.loading;

        searchspotlight.set_page($scope.currentpage + 1);

        searchspotlight.loadmore().success(function (data) {
          $log.debug(data);
          $scope.tiles.push.apply($scope.tiles, data.nodes);
          $scope.totalpages = data.pager.pages;
          $scope.count = data.pager.count;
          $scope.currentpage = data.pager.page;
          $log.debug($scope.tiles);
          searchspotlight.loading = false;
          $scope.loading = searchspotlight.loading;
        }).error(function () {
          searchspotlight.loading = false;
          $scope.loading = searchspotlight.loading;
        });
      };



    }




}());
