(function () {
  angular.module('sonnox.learnspotlight')
  .controller('tagsspotlightFiltersCtr', tagsspotlightFiltersCtr);

  tagsspotlightFiltersCtr.$inject = ['$scope', '$rootScope', '$document', 'searchspotlight', '$log'];

  function tagsspotlightFiltersCtr($scope, $rootScope, $document, searchspotlight, $log) {
    // add
    var termlist = [];
    searchspotlight.tearmlist = termlist;
    var comparator = angular.equals;

    function contains(arr, item, comparator) {
      if (angular.isArray(arr)) {
        for (var i = arr.length; i--;) {
          if (comparator(arr[i], item)) {
            return true;
          }
        }
      }
      return false;
    }

    function add(arr, item, comparator) {
      arr = angular.isArray(arr) ? arr : [];
      if (!contains(arr, item, comparator)) {
        arr.push(item);
      }
      return arr;
    }

    // remove
    function remove(arr, item, comparator) {
      if (angular.isArray(arr)) {
        for (var i = arr.length; i--;) {
          if (comparator(arr[i], item)) {
            arr.splice(i, 1);
            break;
          }
        }
      }
      return arr;
    }
    $rootScope.listscope.loading = searchspotlight.loading;
    $scope.loading = searchspotlight.loading;
    $scope.loadspotlight = function () {
        if (searchspotlight.loading ===  true) { return; }
       // searchspotlight.set_q($scope.q);
        searchspotlight.set_page(0);
        searchspotlight.loading = true;
        $rootScope.listscope.loading = searchspotlight.loading;

        searchspotlight.loadmore().success(function (data) {
          $log.debug(data);
          $rootScope.listscope.tiles = data.nodes;
          $rootScope.listscope.totalpages = data.pager.pages;
          $rootScope.listscope.count = data.pager.count;
          $rootScope.listscope.currentpage = data.pager.page;
          $log.debug($scope.tiles);
          searchspotlight.loading = false;
          $rootScope.listscope.loading = searchspotlight.loading;
          $scope.loading = $rootScope.listscope.loading;
        }).error(function () { searchspotlight.loading = false; $rootScope.listscope.loading = searchspotlight.loading; $scope.loading = $rootScope.listscope.loading; });
      };

    $scope.togglespotlightterm = function (tid) {
      if (searchspotlight.loading === true) { return; }
      termlist = add(termlist, tid, comparator);
      var trm = $document[0].getElementById('term-' + tid);
      if (angular.element(trm).hasClass('active')) {
        termlist = remove(termlist, tid, comparator);
        angular.element(trm).removeClass('active');
      } else {
        termlist = add(termlist, tid, comparator);
        angular.element(trm).addClass('active');
      }
      $scope.loadspotlight();
      $log.debug(termlist);
    };

  }


})();
