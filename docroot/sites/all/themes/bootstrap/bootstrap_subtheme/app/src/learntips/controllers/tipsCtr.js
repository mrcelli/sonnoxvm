(function () {
  angular
    .module('sonnox.learntips')
    .controller('tipsCtr', tipsCtr);


  tipsCtr.$inject = ['$scope',  'tipslist'];

  function tipsCtr($scope, tipslist) {
      $scope.tips = tipslist.getlist();
    }
})();
