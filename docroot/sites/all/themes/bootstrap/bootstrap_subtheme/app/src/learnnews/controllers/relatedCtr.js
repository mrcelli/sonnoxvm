(function () {
  angular
    .module('sonnox.learnnews')
    .controller('newsrelatedCtr', newsrelatedCtr);


  newsrelatedCtr.$inject = ['$scope',  'newslist'];

  function newsrelatedCtr($scope, newslist) {
      $scope.related = newslist.getrelated();
    }
})();
