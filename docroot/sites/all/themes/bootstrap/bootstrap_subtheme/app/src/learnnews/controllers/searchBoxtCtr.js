(function () {
  angular
    .module('sonnox.learnnews')
    .controller('searchBoxnewsCtr', searchBoxnewsCtr);

  searchBoxnewsCtr.$inject = ['$scope', '$rootScope', '$http', '$log', 'searchnews'];

  function searchBoxnewsCtr($scope, $rootScope, $http, $log, searchnews) {
      $scope.loading = searchnews.loading;
      $scope.$watch(function () { return searchnews.loading; },
              function (newValue) {
                $scope.loading = newValue;
              }
             );

      $scope.search = function () {
        if (searchnews.loading ===  true) { return; }
        searchnews.set_q($scope.q);
        searchnews.set_page(0);
        searchnews.loading = true;
        $rootScope.listscope.loading = searchnews.loading;
        searchnews.loadmore().success(function (data) {
          $rootScope.listscope.loading = searchnews.loading;
          $log.debug(data);
          $rootScope.listscope.tiles = data.nodes;
          $rootScope.listscope.totalpages = data.pager.pages;
          $rootScope.listscope.count = data.pager.count;
          $rootScope.listscope.currentpage = data.pager.page;
          $log.debug($scope.tiles);
          searchnews.loading = false;
          $rootScope.listscope.loading = searchnews.loading;
        }).error(function () { searchnews.loading = false; $rootScope.listscope.loading = searchnews.loading; });
      };
    }




}());
