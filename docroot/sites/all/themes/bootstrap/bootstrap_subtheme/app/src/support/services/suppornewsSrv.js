(function () {
  angular
    .module('sonnox.support')
    .factory('supnewsSrv', supnewsSrv);

  supnewsSrv.$inject = ['$log'];

  function supnewsSrv($log) {

      return  {
        getshortlist : function () {
          $log.debug(Drupal.settings.sonnox);
          return Drupal.settings.sonnox.supportnews.list;
        },
        getlist: function () {
          $log.debug(Drupal.settings.sonnox.faqshortlist.faq);
          return Drupal.settings.sonnox.faqshortlist.faq;
        }
      };

    }


})();
