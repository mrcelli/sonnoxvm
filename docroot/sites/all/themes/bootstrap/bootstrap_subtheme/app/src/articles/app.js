(function () {
  'use strict';
  // Fix for naughty jQuery plugins.
  window.$ = window.jQuery;
  angular.module('sonnox.articles', ['sonnox-templates', 'ui.bootstrap',  'sonnox.common', 'ngAnimate', 'angular-flexslider']);

}());


