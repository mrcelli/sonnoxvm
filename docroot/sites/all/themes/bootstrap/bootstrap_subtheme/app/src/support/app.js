(function () {
  'use strict';
  // Fix for naughty jQuery plugins.
  window.$ = window.jQuery;
  angular.module('sonnox.support', ['sonnox-templates', 'pasvaz.bindonce', 'ui.bootstrap']);
  //angular.module('infinite-scroll').value('THROTTLE_MILLISECONDS', 250);
}());

