(function () {
  angular
    .module('sonnox.plugin')
    .factory('relatedvideosrv', relatedvideosrv);

  relatedvideosrv.$inject = ['$log'];

  function relatedvideosrv($log) {
      return  {
        getlist: function () {
          $log.debug(Drupal.settings.sonnox);
          if (typeof(Drupal.settings.sonnox.videos) !== 'undefined') {
            return Drupal.settings.sonnox.videos.list;
          } else {
            return  null;
          }
        }
      };

    }


})();
