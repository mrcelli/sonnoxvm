(function () {
  angular
    .module('sonnox.learnnews')
    .controller('learnPagenews', learnPagenews);

  learnPagenews.$inject = ['$scope', '$log'];

  function learnPagenews($scope, $log) {
    $log.debug($scope);
    $scope.page_change = function () {
      $log.debug($scope.page_value);
      window.location.href =  $scope.page_value;
    };
  }

}());


