(function () {
  angular.module('sonnox.learnartists')
  .controller('tagsArtistsFiltersCtr', tagsArtistsFiltersCtr);

  tagsArtistsFiltersCtr.$inject = ['$scope', '$rootScope', '$document', 'searchartists', '$log'];

  function tagsArtistsFiltersCtr($scope, $rootScope, $document, searchartists, $log) {
    // add
    var termlist = [];
    searchartists.tearmlist = termlist;
    var comparator = angular.equals;

    function contains(arr, item, comparator) {
      if (angular.isArray(arr)) {
        for (var i = arr.length; i--;) {
          if (comparator(arr[i], item)) {
            return true;
          }
        }
      }
      return false;
    }

    function add(arr, item, comparator) {
      arr = angular.isArray(arr) ? arr : [];
      if (!contains(arr, item, comparator)) {
        arr.push(item);
      }
      return arr;
    }

    // remove
    function remove(arr, item, comparator) {
      if (angular.isArray(arr)) {
        for (var i = arr.length; i--;) {
          if (comparator(arr[i], item)) {
            arr.splice(i, 1);
            break;
          }
        }
      }
      return arr;
    }
    $rootScope.listscope.loading = searchartists.loading;
    $scope.loading = searchartists.loading;
    $scope.loadartists = function () {
        if (searchartists.loading ===  true) { return; }
       // searchartists.set_q($scope.q);
        searchartists.set_page(0);
        searchartists.loading = true;
        $rootScope.listscope.loading = searchartists.loading;
        //$('#infinite-container').empty();
        searchartists.loadmore().success(function (data) {
          $log.debug(data);
          $rootScope.listscope.tiles = data.nodes;
          $rootScope.listscope.totalpages = data.pager.pages;
          $rootScope.listscope.count = data.pager.count;
          $rootScope.listscope.currentpage = data.pager.page;
          $log.debug($scope.tiles);
          searchartists.loading = false;
          $rootScope.listscope.loading = searchartists.loading;
          $scope.loading = $rootScope.listscope.loading;
        }).error(function () { searchartists.loading = false; $rootScope.listscope.loading = searchartists.loading; $scope.loading = $rootScope.listscope.loading; });
      };

    $scope.toggleartiststerm = function (tid) {
      if (searchartists.loading === true) { return; }
      termlist = add(termlist, tid, comparator);
      var trm = $document[0].getElementById('term-' + tid);
      if (angular.element(trm).hasClass('active')) {
        termlist = remove(termlist, tid, comparator);
        angular.element(trm).removeClass('active');
      } else {
        termlist = add(termlist, tid, comparator);
        angular.element(trm).addClass('active');
      }
      $scope.loadartists();
      $log.debug(termlist);
    };

  }


})();
