(function () {
  angular
    .module('sonnox.about')
    .controller('peopleCtr', peopleCtr);


  peopleCtr.$inject = ['$scope', 'peoplesrv'];

  function peopleCtr($scope, peoplesrv) {
      $scope.people = peoplesrv.getlist();

    }


})();


(function () {
  angular
    .module('sonnox.about')
    .controller('partnersCtr', partnersCtr);


  partnersCtr.$inject = ['$scope', 'partnerssrv'];

  function partnersCtr($scope, partnerssrv) {
      $scope.partners = partnerssrv.getlist();
    }


})();




(function () {
  angular
    .module('sonnox.about')
    .controller('locationCtr', locationCtr);


  locationCtr.$inject = ['$scope', 'NgMap'];

  function locationCtr($scope, NgMap) {
      NgMap.getMap().then(function (map) {
        console.log(map.getCenter());
        //console.log('markers', map.markers);
        //console.log('shapes', map.shapes);

      });
      $scope.address = '';
      $scope.setaddress = function (ad) {
        $scope.address = ad;
      };
      $scope.directions = {origin: $scope.address, destination: 'Manor Barns, Finstock OX73DG UK', panelName: 'directions-panel'};
      $scope.map = {
        center:  {lat : 51.8488517, lng : -1.4936862},
        options: function () {
          return {
            streetViewControl: true,
            scrollwheel: false,
            styles : [
              {
                'featureType': 'poi',
                'elementType': 'geometry.fill',
                'stylers': [
                  { 'color': '#6e8abb' }
                ]
              }, {
                'featureType': 'landscape',
                'stylers': [
                  { 'color': '#6e8abb' }
                ]
              }, {
                'stylers': [
                  { 'visibility': 'simplified' }
                ]
              }
            ]
          };
        }
      };
      $scope.marker = {
        position: {lat : 51.8488517, lng : -1.4936862},
        scrollwheel: false
      };

      //$scope.styles = '[{featureType : \'poi\',elementType : \'geometry.fill\',stylers: [{ color : \'#6e8abb\' }]}, {featureType: \'landscape\',stylers : [{ color : \'#6e8abb\' }]}, {stylers: [{  visibility : \'simplified\' }]}]';
    }


})();
