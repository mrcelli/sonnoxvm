(function () {
  angular.module('sonnox.learnsection')
  .controller('tagsFiltersCtr', tagsFiltersCtr);

  tagsFiltersCtr.$inject = ['$scope', '$rootScope', '$document', 'searchvideos', '$log'];

  function tagsFiltersCtr($scope, $rootScope, $document, searchvideos, $log) {
    // add
    var termlist = [];
    searchvideos.tearmlist = termlist;
    var comparator = angular.equals;

    function contains(arr, item, comparator) {
      if (angular.isArray(arr)) {
        for (var i = arr.length; i--;) {
          if (comparator(arr[i], item)) {
            return true;
          }
        }
      }
      return false;
    }

    function add(arr, item, comparator) {
      arr = angular.isArray(arr) ? arr : [];
      if (!contains(arr, item, comparator)) {
        arr.push(item);
      }
      return arr;
    }

    // remove
    function remove(arr, item, comparator) {
      if (angular.isArray(arr)) {
        for (var i = arr.length; i--;) {
          if (comparator(arr[i], item)) {
            arr.splice(i, 1);
            break;
          }
        }
      }
      return arr;
    }
    $rootScope.listscope.loading = searchvideos.loading;
    $scope.loading = searchvideos.loading;
    $scope.loadvideos = function () {
        if (searchvideos.loading ===  true) { return; }
       // searchvideos.set_q($scope.q);
        searchvideos.set_page(0);
        searchvideos.loading = true;
        $rootScope.listscope.loading = searchvideos.loading;

        searchvideos.loadmore().success(function (data) {
          $log.debug(data);
          $rootScope.listscope.tiles = data.nodes;
          $rootScope.listscope.totalpages = data.pager.pages;
          $rootScope.listscope.count = data.pager.count;
          $rootScope.listscope.currentpage = data.pager.page;
          $log.debug($scope.tiles);
          searchvideos.loading = false;
          $rootScope.listscope.loading = searchvideos.loading;
          $scope.loading = $rootScope.listscope.loading;
        }).error(function () { searchvideos.loading = false; $rootScope.listscope.loading = searchvideos.loading; $scope.loading = $rootScope.listscope.loading; });
      };

    $scope.toggleterm = function (tid) {
      if (searchvideos.loading === true) { return; }
      termlist = add(termlist, tid, comparator);
      var trm = $document[0].getElementById('term-' + tid);
      if (angular.element(trm).hasClass('active')) {
        termlist = remove(termlist, tid, comparator);
        angular.element(trm).removeClass('active');
      } else {
        termlist = add(termlist, tid, comparator);
        angular.element(trm).addClass('active');
      }
      $scope.loadvideos();
      $log.debug(termlist);
    };

  }


})();
