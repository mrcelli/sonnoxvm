(function () {
    'use strict';

    var app = angular.module('sonnox', ['ngSanitize',
                           'ui.bootstrap',
                           'sonnox.products',
                           'sonnox.learnarticles',
                           'sonnox.learntips',
                           'sonnox.learnsection',
                           'sonnox.learnartists',
                           'sonnox.learnnews',
                           'sonnox.learnspotlight',
                           'sonnox.articles',
                           'angular-flexslider',
                           'ngAnimate',
                           'angular.filter',
                           'angular-images-loaded',
                           'sonnox.page',
                           'sonnox.footer',
                           'sonnox.support',
                           'sonnox.plugin',
                           'sonnox.about'
                           ])
    .config(['$provide', function ($provide) {
        $provide.decorator('$browser', ['$delegate', function ($delegate) {
            $delegate.onUrlChange = function () {};
            $delegate.url = function () {
              return '';
            };
            return $delegate;
          }]);
      }]);
    app.config(['$logProvider', function ($logProvider) {
      $logProvider.debugEnabled(false);
    }]);


  }());


