(function () {
  'use strict';
  // Fix for naughty jQuery plugins.
  window.$ = window.jQuery;
  angular.module('sonnox.common', ['sonnox-templates', 'angular-images-loaded', 'youtube-embed', 'pasvaz.bindonce', 'angular-inview', 'or-filter']);

}());

