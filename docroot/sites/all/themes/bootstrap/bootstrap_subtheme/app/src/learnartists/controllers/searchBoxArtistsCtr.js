(function () {
  angular
    .module('sonnox.learnartists')
    .controller('searchBoxArtistsCtr', searchBoxArtistsCtr);

  searchBoxArtistsCtr.$inject = ['$scope', '$rootScope', '$http', '$log', 'searchartists'];

  function searchBoxArtistsCtr($scope, $rootScope, $http, $log, searchartists) {
      $scope.loading = searchartists.loading;
      $scope.$watch(function () { return searchartists.loading; },
              function (newValue) {
                $scope.loading = newValue;
              }
             );

      $scope.search = function () {
        if (searchartists.loading ===  true) { return; }
        searchartists.set_q($scope.q);
        searchartists.set_page(0);
        searchartists.loading = true;
        $rootScope.listscope.loading = searchartists.loading;
        searchartists.loadmore().success(function (data) {
          $rootScope.listscope.loading = searchartists.loading;
          $log.debug(data);
          $rootScope.listscope.tiles = data.nodes;
          $rootScope.listscope.totalpages = data.pager.pages;
          $rootScope.listscope.count = data.pager.count;
          $rootScope.listscope.currentpage = data.pager.page;
          $log.debug($scope.tiles);
          searchartists.loading = false;
          $rootScope.listscope.loading = searchartists.loading;
        }).error(function () { searchartists.loading = false; $rootScope.listscope.loading = searchartists.loading; });
      };
    }




}());
