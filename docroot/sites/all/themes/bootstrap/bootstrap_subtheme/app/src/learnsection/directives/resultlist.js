(function () {
    angular
        .module('sonnox.learnsection')
        .directive('videolist', videolist);
    videolist.$inject = ['$compile', '$timeout', '$log', '$http' ];

    function videolist() {
        var link = function (scope) {
          scope.baseUrl = Drupal.settings.baseUrl;
        };
        return {
            restrict: 'E',
            templateUrl: videotemplate,// 'learnsection/views/video-list.html',
            link: link
           // transclude: true
          };
      }

    function videotemplate() {
      var isMobile = {
          t : function () {
              return 'test';
            },
          Android : function () {
              return navigator.userAgent.match(/Android/i);
            },
          BlackBerry : function () {
              return navigator.userAgent.match(/BlackBerry/i);
            },
          iOS : function () {
              return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
          Opera : function () {
              return navigator.userAgent.match(/Opera Mini/i);
            },
          Windows : function () {
              return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
            },
          isiPad : function () {
              return  navigator.userAgent.match(/iPad/i);
            },
          any : function () {
              var p = isMobile;
              return (p.Android() || p.BlackBerry() || p.iOS() || p.Opera() || p.Windows());
            }
        };

      if (isMobile.any()) {
        if (isMobile.isiPad()) {
          return 'learnsection/views/video-list.html';
        } else {
          return 'learnsection/views/video-list-mobile.html';
        }

      } else {
        //return 'learnsection/views/video-list-mobile.html';
        return 'learnsection/views/video-list.html';
      }
    }




  }());
