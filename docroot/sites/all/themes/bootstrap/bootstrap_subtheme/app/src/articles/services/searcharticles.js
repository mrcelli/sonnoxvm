(function () {
  angular
    .module('sonnox.articles')
    .factory('searcharticles', searcharticles);

  searcharticles.$inject = ['$log', '$http'];

  function searcharticles($log, $http) {
    var page;

    return  {
      loading : false,
      gettiles : function () {
                        return Drupal.settings.sonnox.articleslist;
                      },
      getfilters: function () {
                        return Drupal.settings.sonnox.articlesfilters.terms;
                      },
      loadmore  : function () {
          $log.debug(page);
          //var termsquery = this.tearmlist.join('+');
          $log.debug(this.q);
          $log.debug(this.tearmlist);
          return $http({method: 'GET', url: '/json/articles?',   params: { page :  page, query : this.q, 'taxonomytid[]' : this.tearmlist} });

        },
      set_q : function (query) {
        this.q = query;
      },
      get_q : function () {
        return this.q;
      },
      set_page : function (p) {
        page = p;
      },
      get_page : function () {
        return page;
      },
      tearmlist : [],
      q : ''

    };

  }





}());
