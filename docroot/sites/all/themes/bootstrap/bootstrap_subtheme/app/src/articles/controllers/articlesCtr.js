(function () {
  angular
    .module('sonnox.articles')
    .controller('articlesCtr', articlesCtr);


  articlesCtr.$inject = ['$scope',  'articleslist', 'searcharticles', '$log', '$document', 'slidesrv'];

  function articlesCtr($scope, articleslist, searcharticles, $log, $document, slidesrv) {


      $scope.articles = articleslist.getlist();
      $scope.slides = (typeof(slidesrv.slides) !== 'undefined') ? slidesrv.slides.nodes : [];
      $log.debug('slides');
      $log.debug($scope.slides);
      var result = searcharticles.gettiles();
      $scope.terms = searcharticles.getfilters();
      $log.debug($scope.terms);
      $scope.tiles =  result.tiles;
      $scope.totalpages = result.pager.pages;
      $scope.count = result.pager.count;
      $scope.currentpage = result.pager.page;
      $log.debug($scope);
      //$scope = $scope;
      $scope.loading = false;

      $scope.termChanged = function () {
        $scope.termsvalues = $scope.termsselected.join(',');
        $log.debug($scope.termsvalues);
      };

      $scope.getUrl = function (item) {
        if (item.node.field_external_link.length === 0 || typeof(item.node.field_external_link) === 'undefined') {
          return item.node.path;
        } else {
          return  item.node.field_external_link;
        }
      };
      $scope.getTarget = function (item) {

        if (item.node.field_external_link.length === 0 || typeof(item.node.field_external_link) === 'undefined') {
          return '_self';
        }
        else {
          if (typeof(item.node.field_internal_link) !== 'undefined') {
            if ((item.node.field_internal_link === 1) || (item.node.field_internal_link === '1'))  {

              return '_self';
            } else {
              return '_blank';
            }
          } else {
            return '_blank';
          }
        }
      };

      $scope.slideTemplate = function (item) {
        var style = item.node.field_slide_style;
        if (style === '') {
          style = 'default';
        }
        $log.debug(style);
        return 'articles/views/slide-' + style + '.html';
      };

      $scope.loadm = function () {
        if (searcharticles.loading === true) { return; }
        if ($scope.totalpages === ($scope.currentpage + 1)) {
          searcharticles.loading = false;
          $scope.loading = searcharticles.loading;
          return;
        }
        $log.debug($scope.currentpage);
        $log.debug(searcharticles.loading);
        $log.debug($scope.totalpages);
        searcharticles.loading = true;
        $scope.loading = searcharticles.loading;

        searcharticles.set_page($scope.currentpage + 1);

        searcharticles.loadmore().success(function (data) {
          $log.debug(data);
          $scope.tiles.push.apply($scope.tiles, data.nodes);
          $scope.totalpages = data.pager.pages;
          $scope.count = data.pager.count;
          $scope.currentpage = data.pager.page;
          $log.debug($scope.tiles);
          searcharticles.loading = false;
          $scope.loading = searcharticles.loading;
        }).error(function () {
          searcharticles.loading = false;
          $scope.loading = searcharticles.loading;
        });
      };
      $scope.page_change = function () {
        $log.debug($scope.page_value);
        window.location.href =  $scope.page_value;
      };
      $scope.related =  null;//articleslist.getrelated();

      $scope.loading = searcharticles.loading;
      $scope.$watch(function () { return searcharticles.loading; },
              function (newValue) {
                $scope.loading = newValue;
              }
             );

      $scope.search = function () {
        if (searcharticles.loading ===  true) { return; }
        searcharticles.set_q($scope.q);
        searcharticles.set_page(0);
        searcharticles.loading = true;
        $scope.loading = searcharticles.loading;
        searcharticles.loadmore().success(function (data) {
          $scope.loading = searcharticles.loading;
          $log.debug(data);
          $scope.tiles = data.nodes;
          $scope.totalpages = data.pager.pages;
          $scope.count = data.pager.count;
          $scope.currentpage = data.pager.page;
          $log.debug($scope.tiles);
          searcharticles.loading = false;
          $scope.loading = searcharticles.loading;
        }).error(function () { searcharticles.loading = false; $scope.loading = searcharticles.loading; });
      };



      var termlist = [];
      searcharticles.tearmlist = termlist;
      var comparator = angular.equals;

      function contains(arr, item, comparator) {
        if (angular.isArray(arr)) {
          for (var i = arr.length; i--;) {
            if (comparator(arr[i], item)) {
              return true;
            }
          }
        }
        return false;
      }

      function add(arr, item, comparator) {
        arr = angular.isArray(arr) ? arr : [];
        if (!contains(arr, item, comparator)) {
          arr.push(item);
        }
        return arr;
      }

      // remove
      function remove(arr, item, comparator) {
        if (angular.isArray(arr)) {
          for (var i = arr.length; i--;) {
            if (comparator(arr[i], item)) {
              arr.splice(i, 1);
              break;
            }
          }
        }
        return arr;
      }

      $scope.getslideimage = function  (elm, pos) {
        elm = $scope.getElm(elm);
        $log.debug('image');
        $log.debug(elm);
        if (typeof(pos) === 'undefined') {
          return  'url(' + elm.node.field_slideshow_image.src + ')';
        } else {
          if (typeof(elm.node.field_slideshow_image[pos]) !== 'undefined') {
            return  'url(' + elm.node.field_slideshow_image[pos].src + ')';
          } else {
            if (typeof(elm.node.field_slideshow_image[0]) === 'undefined') {
              return  'url(' + elm.node.field_slideshow_image.src + ')';
            } else {
              return  'url(' + elm.node.field_slideshow_image[0].src + ')';
            }
          }
        }
      };

      $scope.getElm = function (elm) {
        if (elm instanceof Array) {
          return  elm[0];
        } else {
          return elm;
        }
      };

    //$scope.loading = searcharticles.loading;
      $scope.loading = searcharticles.loading;
      $scope.loadarticles = function () {
          if (searcharticles.loading ===  true) { return; }
         // searcharticles.set_q($scope.q);
          searcharticles.set_page(0);
          searcharticles.loading = true;
          $scope.loading = searcharticles.loading;

          searcharticles.loadmore().success(function (data) {
            $log.debug(data);
            $scope.tiles = data.nodes;
            $scope.totalpages = data.pager.pages;
            $scope.count = data.pager.count;
            $scope.currentpage = data.pager.page;
            $log.debug($scope.tiles);
            searcharticles.loading = false;
            $scope.loading = searcharticles.loading;
            $scope.loading = $scope.loading;
          }).error(function () { searcharticles.loading = false; $scope.loading = searcharticles.loading; $scope.loading = $scope.loading; });
        };

      $scope.togglearticlesterm = function (tid) {
        if (searcharticles.loading === true) { return; }
        termlist = add(termlist, tid, comparator);
        var trm = $document[0].getElementById('term-' + tid);
        if (angular.element(trm).hasClass('active')) {
          termlist = remove(termlist, tid, comparator);
          angular.element(trm).removeClass('active');
        } else {
          termlist = add(termlist, tid, comparator);
          angular.element(trm).addClass('active');
        }
        $scope.loadarticles();
        $log.debug(termlist);
      };
    }
})();
