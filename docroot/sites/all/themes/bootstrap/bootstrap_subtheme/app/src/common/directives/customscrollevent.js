
(function () {

    angular.module('sonnox.common')
        .directive('customscrollevent', customscrollevent);
    customscrollevent.$inject = [ '$log' ];

    function customscrollevent($log) {
      return {
        restrict: 'A',
        link: function (scope, elem, attrs) {
          var fName = attrs.customscrollevent,
          func = function (ev) {
            scope[fName](ev);
          };
          $log.debug('scroll event');
          $log.debug(elem);
          //elem.on('scroll', func);
          elem.off('scroll', func);
          scope.$on('$destroy', function () {
            elem.off('scroll', func);
          });
        }
      };
    }

  }());
