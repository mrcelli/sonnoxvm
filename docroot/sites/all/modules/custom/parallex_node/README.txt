djc article category
===============

The djc_article_category 


Requirements
==============

-
html editors!


Installation
=============

-Download and extract your module in your sites/all/modules/djc_article_category folder.

-Enable it through the administration area (admin/modules) or through drush
 (drush -y pm-enable newsletter).




Usage
==========

Y


Author/maintainer
===================

Original Author and Maintainer:

Riccardo Ravaro 



Support
=======

Issues should be posted in the issue queue on drupal.org:

