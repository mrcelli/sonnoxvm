<?php
/**
 * @file
 * ct_system_requirements.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function ct_system_requirements_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_system_requirements_conten'
  $field_bases['field_system_requirements_conten'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_system_requirements_conten',
    'foreign keys' => array(
      'node' => array(
        'columns' => array(
          'target_id' => 'nid',
        ),
        'table' => 'node',
      ),
    ),
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 1,
          ),
          'views_filter_option_limit' => array(
            'status' => 1,
          ),
        ),
        'sort' => array(
          'type' => 'none',
        ),
        'target_bundles' => array(
          'article_file' => 'article_file',
          'article_image' => 'article_image',
          'article_soundcloud' => 'article_soundcloud',
          'article_text' => 'article_text',
          'article_video' => 'article_video',
          'parallex' => 'parallex',
        ),
      ),
      'profile2_private' => FALSE,
      'target_type' => 'node',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  return $field_bases;
}
