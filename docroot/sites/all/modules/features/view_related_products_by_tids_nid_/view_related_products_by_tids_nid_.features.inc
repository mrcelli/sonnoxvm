<?php
/**
 * @file
 * view_related_products_by_tids_nid_.features.inc
 */

/**
 * Implements hook_views_api().
 */
function view_related_products_by_tids_nid__views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
