<?php
/**
 * @file
 * view_related_products_by_tids_nid_.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function view_related_products_by_tids_nid__views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'related_product_by_tag';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'related product by tag';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'role';
  $handler->display->display_options['access']['role'] = array(
    3 => '3',
    1 => '1',
    2 => '2',
  );
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['distinct'] = TRUE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'views_json';
  $handler->display->display_options['style_options']['plaintext_output'] = 1;
  $handler->display->display_options['style_options']['remove_newlines'] = 0;
  $handler->display->display_options['style_options']['jsonp_prefix'] = '';
  $handler->display->display_options['style_options']['using_views_api_mode'] = 0;
  $handler->display->display_options['style_options']['object_arrays'] = 0;
  $handler->display->display_options['style_options']['numeric_strings'] = 1;
  $handler->display->display_options['style_options']['bigint_string'] = 0;
  $handler->display->display_options['style_options']['pretty_print'] = 0;
  $handler->display->display_options['style_options']['unescaped_slashes'] = 0;
  $handler->display->display_options['style_options']['unescaped_unicode'] = 0;
  $handler->display->display_options['style_options']['char_encoding'] = array();
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_article_entity_reference_target_id']['id'] = 'field_article_entity_reference_target_id';
  $handler->display->display_options['relationships']['field_article_entity_reference_target_id']['table'] = 'field_data_field_article_entity_reference';
  $handler->display->display_options['relationships']['field_article_entity_reference_target_id']['field'] = 'field_article_entity_reference_target_id';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Tags */
  $handler->display->display_options['fields']['field_tags']['id'] = 'field_tags';
  $handler->display->display_options['fields']['field_tags']['table'] = 'field_data_field_tags';
  $handler->display->display_options['fields']['field_tags']['field'] = 'field_tags';
  $handler->display->display_options['fields']['field_tags']['label'] = '';
  $handler->display->display_options['fields']['field_tags']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_tags']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_tags']['delta_offset'] = '0';
  /* Field: Content: product plug in tile image */
  $handler->display->display_options['fields']['field_product_plug_in_tile_image']['id'] = 'field_product_plug_in_tile_image';
  $handler->display->display_options['fields']['field_product_plug_in_tile_image']['table'] = 'field_data_field_product_plug_in_tile_image';
  $handler->display->display_options['fields']['field_product_plug_in_tile_image']['field'] = 'field_product_plug_in_tile_image';
  $handler->display->display_options['fields']['field_product_plug_in_tile_image']['label'] = '';
  $handler->display->display_options['fields']['field_product_plug_in_tile_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_product_plug_in_tile_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_product_plug_in_tile_image']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Content: article entity reference */
  $handler->display->display_options['fields']['field_article_entity_reference']['id'] = 'field_article_entity_reference';
  $handler->display->display_options['fields']['field_article_entity_reference']['table'] = 'field_data_field_article_entity_reference';
  $handler->display->display_options['fields']['field_article_entity_reference']['field'] = 'field_article_entity_reference';
  /* Field: Content: Path */
  $handler->display->display_options['fields']['path']['id'] = 'path';
  $handler->display->display_options['fields']['path']['table'] = 'node';
  $handler->display->display_options['fields']['path']['field'] = 'path';
  $handler->display->display_options['fields']['path']['relationship'] = 'field_article_entity_reference_target_id';
  $handler->display->display_options['fields']['path']['label'] = '';
  $handler->display->display_options['fields']['path']['element_label_colon'] = FALSE;
  /* Field: Content: square image */
  $handler->display->display_options['fields']['field_square_image']['id'] = 'field_square_image';
  $handler->display->display_options['fields']['field_square_image']['table'] = 'field_data_field_square_image';
  $handler->display->display_options['fields']['field_square_image']['field'] = 'field_square_image';
  $handler->display->display_options['fields']['field_square_image']['label'] = '';
  $handler->display->display_options['fields']['field_square_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_square_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_square_image']['settings'] = array(
    'image_style' => 'medium',
    'image_link' => '',
  );
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Similar By Terms: Nid */
  $handler->display->display_options['arguments']['similar_nid']['id'] = 'similar_nid';
  $handler->display->display_options['arguments']['similar_nid']['table'] = 'node';
  $handler->display->display_options['arguments']['similar_nid']['field'] = 'similar_nid';
  $handler->display->display_options['arguments']['similar_nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['similar_nid']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['similar_nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['similar_nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['similar_nid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['similar_nid']['break_phrase'] = TRUE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'product_plug_in' => 'product_plug_in',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'related_product');
  $handler->display->display_options['path'] = 'json/relatedproducts';

  /* Display: Page 2 */
  $handler = $view->new_display('page', 'Page 2', 'page_2');
  $handler->display->display_options['path'] = 'json/product';
  $export['related_product_by_tag'] = $view;

  return $export;
}
