<?php
/**
 * @file
 * feed_youtube_feed_v2.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function feed_youtube_feed_v2_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'youtube_feed_v2-jsonpath_parser_4-required';
  $feeds_tamper->importer = 'youtube_feed_v2';
  $feeds_tamper->source = 'jsonpath_parser:4';
  $feeds_tamper->plugin_id = 'required';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 3;
  $feeds_tamper->description = 'Required field';
  $export['youtube_feed_v2-jsonpath_parser_4-required'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'youtube_feed_v2-jsonpath_parser_4-trim';
  $feeds_tamper->importer = 'youtube_feed_v2';
  $feeds_tamper->source = 'jsonpath_parser:4';
  $feeds_tamper->plugin_id = 'trim';
  $feeds_tamper->settings = array(
    'mask' => '',
    'side' => 'trim',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'Trim';
  $export['youtube_feed_v2-jsonpath_parser_4-trim'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'youtube_feed_v2-jsonpath_parser_4-unique';
  $feeds_tamper->importer = 'youtube_feed_v2';
  $feeds_tamper->source = 'jsonpath_parser:4';
  $feeds_tamper->plugin_id = 'unique';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 4;
  $feeds_tamper->description = 'Unique';
  $export['youtube_feed_v2-jsonpath_parser_4-unique'] = $feeds_tamper;

  return $export;
}
