<?php
/**
 * @file
 * feed_youtube_feed_v2.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function feed_youtube_feed_v2_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'youtube_feed_v2';
  $feeds_importer->config = array(
    'name' => 'youtube feed v2',
    'description' => 'youtube feed v2 manual importer',
    'fetcher' => array(
      'plugin_key' => 'FeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => FALSE,
        'use_pubsubhubbub' => FALSE,
        'designated_hub' => '',
        'request_timeout' => NULL,
        'auto_scheme' => 'http',
        'accept_invalid_cert' => FALSE,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsJSONPathParser',
      'config' => array(
        'context' => '$.items.*',
        'sources' => array(
          'jsonpath_parser:0' => 'snippet.title',
          'jsonpath_parser:1' => 'snippet.description',
          'jsonpath_parser:2' => 'id.videoId',
          'jsonpath_parser:3' => 'snippet.publishedAt',
          'jsonpath_parser:4' => 'id.videoId',
          'jsonpath_parser:5' => 'snippet.thumbnails.medium.url',
          'jsonpath_parser:6' => 'id.videoId',
          'jsonpath_parser:7' => 'snippet.thumbnails.medium.url',
          'jsonpath_parser:8' => 'snippet.publishedAt',
        ),
        'debug' => array(
          'options' => array(
            'context' => 0,
            'jsonpath_parser:0' => 0,
            'jsonpath_parser:1' => 0,
            'jsonpath_parser:2' => 0,
            'jsonpath_parser:3' => 0,
            'jsonpath_parser:4' => 0,
            'jsonpath_parser:5' => 0,
            'jsonpath_parser:6' => 0,
            'jsonpath_parser:7' => 0,
            'jsonpath_parser:8' => 0,
          ),
        ),
        'allow_override' => 0,
        'convert_four_byte' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => 0,
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'jsonpath_parser:0',
            'target' => 'title',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'jsonpath_parser:1',
            'target' => 'body',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'jsonpath_parser:2',
            'target' => 'field_tags',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'jsonpath_parser:3',
            'target' => 'created',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'jsonpath_parser:4',
            'target' => 'field_video_id',
            'unique' => 1,
          ),
          5 => array(
            'source' => 'jsonpath_parser:5',
            'target' => 'field_video_thumbnail_high:uri',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'jsonpath_parser:6',
            'target' => 'guid',
            'unique' => 0,
          ),
          7 => array(
            'source' => 'jsonpath_parser:7',
            'target' => 'field_video_thumbnail:uri',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'jsonpath_parser:8',
            'target' => 'field_publishedat:start',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'update_non_existent' => 'skip',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => 'article_video',
      ),
    ),
    'content_type' => 'youtube_video_importer',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['youtube_feed_v2'] = $feeds_importer;

  return $export;
}
