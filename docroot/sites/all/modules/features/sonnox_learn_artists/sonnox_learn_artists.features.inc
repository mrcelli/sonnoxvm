<?php
/**
 * @file
 * sonnox_learn_artists.features.inc
 */

/**
 * Implements hook_views_api().
 */
function sonnox_learn_artists_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
