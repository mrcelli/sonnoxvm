<?php
/**
 * @file
 * ct_product_bundle.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ct_product_bundle_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function ct_product_bundle_node_info() {
  $items = array(
    'product_bundle' => array(
      'name' => t('product bundle'),
      'base' => 'node_content',
      'description' => t('product bundle content type'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
