<?php
/**
 * @file
 * views_partnerslist.features.inc
 */

/**
 * Implements hook_views_api().
 */
function views_partnerslist_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
