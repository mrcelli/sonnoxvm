<?php
/**
 * @file
 * views_homepage_slideshow.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function views_homepage_slideshow_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'homepage_slideshow';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'homepage slideshow';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'homepage slideshow';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'views_json';
  $handler->display->display_options['style_options']['plaintext_output'] = 1;
  $handler->display->display_options['style_options']['remove_newlines'] = 0;
  $handler->display->display_options['style_options']['jsonp_prefix'] = '';
  $handler->display->display_options['style_options']['using_views_api_mode'] = 0;
  $handler->display->display_options['style_options']['object_arrays'] = 0;
  $handler->display->display_options['style_options']['numeric_strings'] = 1;
  $handler->display->display_options['style_options']['bigint_string'] = 0;
  $handler->display->display_options['style_options']['pretty_print'] = 0;
  $handler->display->display_options['style_options']['unescaped_slashes'] = 0;
  $handler->display->display_options['style_options']['unescaped_unicode'] = 0;
  $handler->display->display_options['style_options']['char_encoding'] = array();
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['type'] = 'text_summary_or_trimmed';
  $handler->display->display_options['fields']['body']['settings'] = array(
    'trim_length' => '600',
  );
  /* Field: Field: Image */
  $handler->display->display_options['fields']['field_image']['id'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['table'] = 'field_data_field_image';
  $handler->display->display_options['fields']['field_image']['field'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['label'] = '';
  $handler->display->display_options['fields']['field_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_image']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  $handler->display->display_options['fields']['field_image']['delta_limit'] = 'all';
  $handler->display->display_options['fields']['field_image']['delta_offset'] = '0';
  /* Field: Content: slideshow image */
  $handler->display->display_options['fields']['field_slideshow_image']['id'] = 'field_slideshow_image';
  $handler->display->display_options['fields']['field_slideshow_image']['table'] = 'field_data_field_slideshow_image';
  $handler->display->display_options['fields']['field_slideshow_image']['field'] = 'field_slideshow_image';
  $handler->display->display_options['fields']['field_slideshow_image']['label'] = '';
  $handler->display->display_options['fields']['field_slideshow_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_slideshow_image']['empty'] = '[field_image]';
  $handler->display->display_options['fields']['field_slideshow_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_slideshow_image']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  $handler->display->display_options['fields']['field_slideshow_image']['delta_offset'] = '0';
  /* Field: Content: Path */
  $handler->display->display_options['fields']['path']['id'] = 'path';
  $handler->display->display_options['fields']['path']['table'] = 'node';
  $handler->display->display_options['fields']['path']['field'] = 'path';
  $handler->display->display_options['fields']['path']['label'] = '';
  $handler->display->display_options['fields']['path']['element_label_colon'] = FALSE;
  /* Field: Content: over image */
  $handler->display->display_options['fields']['field_over_image']['id'] = 'field_over_image';
  $handler->display->display_options['fields']['field_over_image']['table'] = 'field_data_field_over_image';
  $handler->display->display_options['fields']['field_over_image']['field'] = 'field_over_image';
  $handler->display->display_options['fields']['field_over_image']['label'] = '';
  $handler->display->display_options['fields']['field_over_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_over_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_over_image']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Content: Sub Text */
  $handler->display->display_options['fields']['field_caption']['id'] = 'field_caption';
  $handler->display->display_options['fields']['field_caption']['table'] = 'field_data_field_caption';
  $handler->display->display_options['fields']['field_caption']['field'] = 'field_caption';
  $handler->display->display_options['fields']['field_caption']['label'] = '';
  $handler->display->display_options['fields']['field_caption']['element_label_colon'] = FALSE;
  /* Field: Content: call to action button */
  $handler->display->display_options['fields']['field_call_to_action_button']['id'] = 'field_call_to_action_button';
  $handler->display->display_options['fields']['field_call_to_action_button']['table'] = 'field_data_field_call_to_action_button';
  $handler->display->display_options['fields']['field_call_to_action_button']['field'] = 'field_call_to_action_button';
  $handler->display->display_options['fields']['field_call_to_action_button']['label'] = '';
  $handler->display->display_options['fields']['field_call_to_action_button']['element_label_colon'] = FALSE;
  /* Field: Content: call to action button text */
  $handler->display->display_options['fields']['field_call_to_action_button_text']['id'] = 'field_call_to_action_button_text';
  $handler->display->display_options['fields']['field_call_to_action_button_text']['table'] = 'field_data_field_call_to_action_button_text';
  $handler->display->display_options['fields']['field_call_to_action_button_text']['field'] = 'field_call_to_action_button_text';
  $handler->display->display_options['fields']['field_call_to_action_button_text']['label'] = '';
  $handler->display->display_options['fields']['field_call_to_action_button_text']['element_label_colon'] = FALSE;
  /* Field: Content: Text Color */
  $handler->display->display_options['fields']['field_text_color']['id'] = 'field_text_color';
  $handler->display->display_options['fields']['field_text_color']['table'] = 'field_data_field_text_color';
  $handler->display->display_options['fields']['field_text_color']['field'] = 'field_text_color';
  $handler->display->display_options['fields']['field_text_color']['label'] = '';
  $handler->display->display_options['fields']['field_text_color']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_text_color']['type'] = 'colorfield_raw_rgb';
  $handler->display->display_options['fields']['field_text_color']['settings'] = array(
    'display_hash' => 1,
  );
  /* Field: Content: Slide Title */
  $handler->display->display_options['fields']['field_slide_title']['id'] = 'field_slide_title';
  $handler->display->display_options['fields']['field_slide_title']['table'] = 'field_data_field_slide_title';
  $handler->display->display_options['fields']['field_slide_title']['field'] = 'field_slide_title';
  $handler->display->display_options['fields']['field_slide_title']['label'] = '';
  $handler->display->display_options['fields']['field_slide_title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_slide_title']['type'] = 'text_plain';
  /* Field: Content: external link */
  $handler->display->display_options['fields']['field_external_link']['id'] = 'field_external_link';
  $handler->display->display_options['fields']['field_external_link']['table'] = 'field_data_field_external_link';
  $handler->display->display_options['fields']['field_external_link']['field'] = 'field_external_link';
  $handler->display->display_options['fields']['field_external_link']['label'] = '';
  $handler->display->display_options['fields']['field_external_link']['element_label_colon'] = FALSE;
  /* Field: Content: Button text Color */
  $handler->display->display_options['fields']['field_button_text_color']['id'] = 'field_button_text_color';
  $handler->display->display_options['fields']['field_button_text_color']['table'] = 'field_data_field_button_text_color';
  $handler->display->display_options['fields']['field_button_text_color']['field'] = 'field_button_text_color';
  $handler->display->display_options['fields']['field_button_text_color']['label'] = '';
  $handler->display->display_options['fields']['field_button_text_color']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_button_text_color']['type'] = 'colorfield_raw_rgb';
  $handler->display->display_options['fields']['field_button_text_color']['settings'] = array(
    'display_hash' => 1,
  );
  /* Field: Content: color */
  $handler->display->display_options['fields']['field_color']['id'] = 'field_color';
  $handler->display->display_options['fields']['field_color']['table'] = 'field_data_field_color';
  $handler->display->display_options['fields']['field_color']['field'] = 'field_color';
  $handler->display->display_options['fields']['field_color']['label'] = '';
  $handler->display->display_options['fields']['field_color']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_color']['type'] = 'colorfield_raw_rgb';
  $handler->display->display_options['fields']['field_color']['settings'] = array(
    'display_hash' => 1,
  );
  /* Field: Content: slide style */
  $handler->display->display_options['fields']['field_slide_style']['id'] = 'field_slide_style';
  $handler->display->display_options['fields']['field_slide_style']['table'] = 'field_data_field_slide_style';
  $handler->display->display_options['fields']['field_slide_style']['field'] = 'field_slide_style';
  $handler->display->display_options['fields']['field_slide_style']['label'] = '';
  $handler->display->display_options['fields']['field_slide_style']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_slide_style']['type'] = 'list_key';
  /* Field: Content: Internal Link */
  $handler->display->display_options['fields']['field_internal_link']['id'] = 'field_internal_link';
  $handler->display->display_options['fields']['field_internal_link']['table'] = 'field_data_field_internal_link';
  $handler->display->display_options['fields']['field_internal_link']['field'] = 'field_internal_link';
  $handler->display->display_options['fields']['field_internal_link']['label'] = '';
  $handler->display->display_options['fields']['field_internal_link']['element_label_colon'] = FALSE;
  /* Field: Content: slide position */
  $handler->display->display_options['fields']['field_slide_position']['id'] = 'field_slide_position';
  $handler->display->display_options['fields']['field_slide_position']['table'] = 'field_data_field_slide_position';
  $handler->display->display_options['fields']['field_slide_position']['field'] = 'field_slide_position';
  $handler->display->display_options['fields']['field_slide_position']['label'] = '';
  $handler->display->display_options['fields']['field_slide_position']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_slide_position']['settings'] = array(
    'thousand_separator' => '',
    'prefix_suffix' => 0,
  );
  /* Sort criterion: Content: slide position (field_slide_position) */
  $handler->display->display_options['sorts']['field_slide_position_value']['id'] = 'field_slide_position_value';
  $handler->display->display_options['sorts']['field_slide_position_value']['table'] = 'field_data_field_slide_position';
  $handler->display->display_options['sorts']['field_slide_position_value']['field'] = 'field_slide_position_value';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: promote to slideshow frontpage (field_promote_to_slideshow_front) */
  $handler->display->display_options['filters']['field_promote_to_slideshow_front_value']['id'] = 'field_promote_to_slideshow_front_value';
  $handler->display->display_options['filters']['field_promote_to_slideshow_front_value']['table'] = 'field_data_field_promote_to_slideshow_front';
  $handler->display->display_options['filters']['field_promote_to_slideshow_front_value']['field'] = 'field_promote_to_slideshow_front_value';
  $handler->display->display_options['filters']['field_promote_to_slideshow_front_value']['value'] = array(
    1 => '1',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'homepage_slideshow_json');
  $handler->display->display_options['path'] = 'json/homepageslideshow';
  $export['homepage_slideshow'] = $view;

  return $export;
}
