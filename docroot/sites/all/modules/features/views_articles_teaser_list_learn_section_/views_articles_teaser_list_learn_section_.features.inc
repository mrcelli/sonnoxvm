<?php
/**
 * @file
 * views_articles_teaser_list_learn_section_.features.inc
 */

/**
 * Implements hook_views_api().
 */
function views_articles_teaser_list_learn_section__views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
