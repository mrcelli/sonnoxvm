<?php
/**
 * @file
 * ct_product_plugin_.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ct_product_plugin__ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function ct_product_plugin__node_info() {
  $items = array(
    'product_plug_in' => array(
      'name' => t('product plug in'),
      'base' => 'node_content',
      'description' => t('product plug in content type'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
