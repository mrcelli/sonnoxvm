<?php
/**
 * @file
 * sonnox_learn_tips_.features.inc
 */

/**
 * Implements hook_views_api().
 */
function sonnox_learn_tips__views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
