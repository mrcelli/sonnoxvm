<?php
/**
 * @file
 * ct_iframe.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ct_iframe_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function ct_iframe_node_info() {
  $items = array(
    'iframe' => array(
      'name' => t('iframe'),
      'base' => 'node_content',
      'description' => t('basic type for iframe pages'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
