<?php
/**
 * @file
 * sonnox_learn_articles.features.inc
 */

/**
 * Implements hook_views_api().
 */
function sonnox_learn_articles_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
