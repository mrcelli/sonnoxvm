<?php
/**
 * @file
 * spotlight_list_.features.inc
 */

/**
 * Implements hook_views_api().
 */
function spotlight_list__views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
