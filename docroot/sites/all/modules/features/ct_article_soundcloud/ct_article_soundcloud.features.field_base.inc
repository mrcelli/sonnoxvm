<?php
/**
 * @file
 * ct_article_soundcloud.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function ct_article_soundcloud_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_soundcloud_url'
  $field_bases['field_soundcloud_url'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_soundcloud_url',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'soundcloudfield',
    'settings' => array(
      'profile2_private' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'soundcloud',
  );

  return $field_bases;
}
