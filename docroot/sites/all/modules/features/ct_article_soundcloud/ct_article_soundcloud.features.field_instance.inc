<?php
/**
 * @file
 * ct_article_soundcloud.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function ct_article_soundcloud_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-article_soundcloud-body'
  $field_instances['node-article_soundcloud-body'] = array(
    'bundle' => 'article_soundcloud',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'content' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'landing' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'responsive_tab' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'slideshow' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'system_requirements' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'trim_length' => 600,
        ),
        'type' => 'text_summary_or_trimmed',
        'weight' => 0,
      ),
      'thumbnail' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'body',
    'label' => 'Body',
    'required' => FALSE,
    'settings' => array(
      'display_summary' => TRUE,
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'rows' => 20,
        'summary_rows' => 5,
      ),
      'type' => 'text_textarea_with_summary',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'node-article_soundcloud-field_promote_to_submenu'
  $field_instances['node-article_soundcloud-field_promote_to_submenu'] = array(
    'bundle' => 'article_soundcloud',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'content' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 2,
      ),
      'landing' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'responsive_tab' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'slideshow' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'system_requirements' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'thumbnail' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_promote_to_submenu',
    'label' => 'promote to submenu',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 0,
      ),
      'type' => 'options_onoff',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'node-article_soundcloud-field_soundcloud_url'
  $field_instances['node-article_soundcloud-field_soundcloud_url'] = array(
    'bundle' => 'article_soundcloud',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'content' => array(
        'label' => 'hidden',
        'module' => 'soundcloudfield',
        'settings' => array(),
        'type' => 'soundcloud_default',
        'weight' => 1,
      ),
      'default' => array(
        'label' => 'above',
        'module' => 'soundcloudfield',
        'settings' => array(),
        'type' => 'soundcloud_default',
        'weight' => 1,
      ),
      'landing' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'responsive_tab' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'slideshow' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'system_requirements' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'thumbnail' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_soundcloud_url',
    'label' => 'soundcloud url',
    'required' => 0,
    'settings' => array(
      'autoplay' => FALSE,
      'color' => 'ff7700',
      'height' => 81,
      'height_sets' => 305,
      'html5_player_height' => 166,
      'html5_player_height_sets' => 450,
      'showartwork' => FALSE,
      'showcomments' => FALSE,
      'showplaycount' => FALSE,
      'soundcloudplayer' => array(
        'autoplay' => 0,
        'color' => 'ff7700',
        'flash_player' => array(
          'height' => 81,
          'height_sets' => 305,
          'showcomments' => 0,
        ),
        'html5_player' => array(
          'html5_player_height' => 166,
          'html5_player_height_sets' => 450,
        ),
        'showartwork' => 0,
        'showplaycount' => 0,
        'visual_player' => array(
          'visual_player_height' => 450,
        ),
        'width' => 100,
      ),
      'user_register_form' => FALSE,
      'visual_player_height' => 450,
      'width' => 100,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'soundcloudfield',
      'settings' => array(),
      'type' => 'soundcloud_url',
      'weight' => 4,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Body');
  t('promote to submenu');
  t('soundcloud url');

  return $field_instances;
}
