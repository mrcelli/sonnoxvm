<?php
/**
 * @file
 * ct_article_soundcloud.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ct_article_soundcloud_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function ct_article_soundcloud_node_info() {
  $items = array(
    'article_soundcloud' => array(
      'name' => t('article_soundcloud'),
      'base' => 'node_content',
      'description' => t('soundcloud track'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
