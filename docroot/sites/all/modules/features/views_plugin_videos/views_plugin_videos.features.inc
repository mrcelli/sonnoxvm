<?php
/**
 * @file
 * views_plugin_videos.features.inc
 */

/**
 * Implements hook_views_api().
 */
function views_plugin_videos_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
