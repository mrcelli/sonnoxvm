<?php
/**
 * @file
 * ct_specification.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ct_specification_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function ct_specification_node_info() {
  $items = array(
    'specification' => array(
      'name' => t('Specification'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
