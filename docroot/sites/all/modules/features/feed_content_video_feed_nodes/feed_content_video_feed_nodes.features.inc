<?php
/**
 * @file
 * feed_content_video_feed_nodes.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function feed_content_video_feed_nodes_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function feed_content_video_feed_nodes_node_info() {
  $items = array(
    'youtube_video_importer' => array(
      'name' => t('youtube video importer'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
