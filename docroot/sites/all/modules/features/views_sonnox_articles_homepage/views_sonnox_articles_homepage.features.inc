<?php
/**
 * @file
 * views_sonnox_articles_homepage.features.inc
 */

/**
 * Implements hook_views_api().
 */
function views_sonnox_articles_homepage_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
