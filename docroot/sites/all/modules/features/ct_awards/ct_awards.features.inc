<?php
/**
 * @file
 * ct_awards.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ct_awards_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function ct_awards_node_info() {
  $items = array(
    'awards' => array(
      'name' => t('awards'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
