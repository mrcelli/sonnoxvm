<?php
/**
 * @file
 * ct_article_video.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ct_article_video_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function ct_article_video_node_info() {
  $items = array(
    'article_video' => array(
      'name' => t('article_video'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('<none>'),
      'help' => '',
    ),
  );
  return $items;
}
