<?php
/**
 * @file
 * views_faq_group_by_terms.features.inc
 */

/**
 * Implements hook_views_api().
 */
function views_faq_group_by_terms_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
