<?php
/**
 * @file
 * ct_parallex.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ct_parallex_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function ct_parallex_node_info() {
  $items = array(
    'parallex' => array(
      'name' => t('parallex'),
      'base' => 'node_content',
      'description' => t('parallex content type'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
