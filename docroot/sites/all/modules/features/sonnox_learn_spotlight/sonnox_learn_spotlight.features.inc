<?php
/**
 * @file
 * sonnox_learn_spotlight.features.inc
 */

/**
 * Implements hook_views_api().
 */
function sonnox_learn_spotlight_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
