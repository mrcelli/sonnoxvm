<?php
/**
 * @file
 * ct_how_to_videos.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ct_how_to_videos_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function ct_how_to_videos_node_info() {
  $items = array(
    'how_to_videos' => array(
      'name' => t('How To videos'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
