<?php
/**
 * @file
 * ct_how_to_videos.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function ct_how_to_videos_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_howto_video'
  $field_bases['field_howto_video'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_howto_video',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'video_embed_field',
    'settings' => array(
      'profile2_private' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'video_embed_field',
  );

  return $field_bases;
}
