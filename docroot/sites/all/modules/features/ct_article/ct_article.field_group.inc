<?php
/**
 * @file
 * ct_article.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function ct_article_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_home_page_slideshow|node|article|form';
  $field_group->group_name = 'group_home_page_slideshow';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'article';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'home page slideshow',
    'weight' => '8',
    'children' => array(
      0 => 'field_slideshow_image',
      1 => 'field_promote_to_slideshow_front',
      2 => 'field_html5_video',
      3 => 'field_call_to_action_button',
      4 => 'field_call_to_action_button_text',
      5 => 'field_sub_title',
      6 => 'field_color',
      7 => 'field_caption',
      8 => 'field_over_image',
      9 => 'field_slide_title',
      10 => 'field_text_color',
      11 => 'field_button_text_color',
      12 => 'field_slide_style',
      13 => 'field_slide_position',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'home page slideshow',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'region region-help',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_home_page_slideshow|node|article|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('home page slideshow');

  return $field_groups;
}
