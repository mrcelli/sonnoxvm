<?php
/**
 * @file
 * ct_support_news_.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ct_support_news__ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function ct_support_news__node_info() {
  $items = array(
    'support_news' => array(
      'name' => t('Support News'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
