<?php
/**
 * @file
 * sonnox_learn_news_.features.inc
 */

/**
 * Implements hook_views_api().
 */
function sonnox_learn_news__views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
