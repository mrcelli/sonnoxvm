<?php
/**
 * @file
 * artists_list.features.inc
 */

/**
 * Implements hook_views_api().
 */
function artists_list_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
