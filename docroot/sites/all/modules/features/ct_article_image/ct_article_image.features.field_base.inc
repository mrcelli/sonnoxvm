<?php
/**
 * @file
 * ct_article_image.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function ct_article_image_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_article_image'
  $field_bases['field_article_image'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_article_image',
    'foreign keys' => array(
      'fid' => array(
        'columns' => array(
          'fid' => 'fid',
        ),
        'table' => 'file_managed',
      ),
    ),
    'indexes' => array(
      'fid' => array(
        0 => 'fid',
      ),
    ),
    'locked' => 0,
    'module' => 'image',
    'settings' => array(
      'default_image' => 0,
      'profile2_private' => FALSE,
      'uri_scheme' => 'public',
    ),
    'translatable' => 0,
    'type' => 'image',
  );

  // Exported field_base: 'field_image_style'
  $field_bases['field_image_style'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_image_style',
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'center' => 'text on the center, image on the center bottom',
        'left' => ' text on the left, image in the background',
        'left-picture' => ' text on the right, picture on the left image in the background',
        'right' => 'text on the right, image in the background',
        'right-picture' => ' text on the left, picture on the right image in the background',
        'col-sm-5' => ' image in half of the column',
      ),
      'allowed_values_function' => '',
      'profile2_private' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  return $field_bases;
}
