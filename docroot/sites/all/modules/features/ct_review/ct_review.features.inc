<?php
/**
 * @file
 * ct_review.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ct_review_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function ct_review_node_info() {
  $items = array(
    'review' => array(
      'name' => t('review'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title review'),
      'help' => '',
    ),
  );
  return $items;
}
