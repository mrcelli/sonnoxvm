<?php
/**
 * @file
 * ct_article_file.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ct_article_file_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function ct_article_file_node_info() {
  $items = array(
    'article_file' => array(
      'name' => t('article file'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
