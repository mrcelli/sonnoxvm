<?php
/**
 * @file
 * ct_promotion_tiles.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ct_promotion_tiles_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function ct_promotion_tiles_node_info() {
  $items = array(
    'promotion_tile' => array(
      'name' => t('promotion tile'),
      'base' => 'node_content',
      'description' => t('promotion tile content type'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
