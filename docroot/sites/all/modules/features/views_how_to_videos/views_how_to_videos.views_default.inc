<?php
/**
 * @file
 * views_how_to_videos.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function views_how_to_videos_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'how_to_videos';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'how to videos';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'how to videos';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'views_json';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: how to video */
  $handler->display->display_options['fields']['field_howto_video']['id'] = 'field_howto_video';
  $handler->display->display_options['fields']['field_howto_video']['table'] = 'field_data_field_howto_video';
  $handler->display->display_options['fields']['field_howto_video']['field'] = 'field_howto_video';
  $handler->display->display_options['fields']['field_howto_video']['label'] = '';
  $handler->display->display_options['fields']['field_howto_video']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_howto_video']['click_sort_column'] = 'video_url';
  $handler->display->display_options['fields']['field_howto_video']['type'] = 'video_embed_field_url';
  $handler->display->display_options['fields']['field_howto_video']['settings'] = array(
    'video_style' => 'normal',
  );
  /* Field: Content: how to video - Description */
  $handler->display->display_options['fields']['field_howto_video_description']['id'] = 'field_howto_video_description';
  $handler->display->display_options['fields']['field_howto_video_description']['table'] = 'field_data_field_howto_video';
  $handler->display->display_options['fields']['field_howto_video_description']['field'] = 'field_howto_video_description';
  $handler->display->display_options['fields']['field_howto_video_description']['label'] = '';
  $handler->display->display_options['fields']['field_howto_video_description']['element_label_colon'] = FALSE;
  /* Field: Content: how to video - Thumbnail path */
  $handler->display->display_options['fields']['field_howto_video_thumbnail_path']['id'] = 'field_howto_video_thumbnail_path';
  $handler->display->display_options['fields']['field_howto_video_thumbnail_path']['table'] = 'field_data_field_howto_video';
  $handler->display->display_options['fields']['field_howto_video_thumbnail_path']['field'] = 'field_howto_video_thumbnail_path';
  $handler->display->display_options['fields']['field_howto_video_thumbnail_path']['label'] = '';
  $handler->display->display_options['fields']['field_howto_video_thumbnail_path']['element_label_colon'] = FALSE;
  /* Field: Content: how to video - Video URL */
  $handler->display->display_options['fields']['field_howto_video_video_url']['id'] = 'field_howto_video_video_url';
  $handler->display->display_options['fields']['field_howto_video_video_url']['table'] = 'field_data_field_howto_video';
  $handler->display->display_options['fields']['field_howto_video_video_url']['field'] = 'field_howto_video_video_url';
  $handler->display->display_options['fields']['field_howto_video_video_url']['label'] = '';
  $handler->display->display_options['fields']['field_howto_video_video_url']['element_label_colon'] = FALSE;
  /* Field: Field: weight */
  $handler->display->display_options['fields']['field_weight']['id'] = 'field_weight';
  $handler->display->display_options['fields']['field_weight']['table'] = 'field_data_field_weight';
  $handler->display->display_options['fields']['field_weight']['field'] = 'field_weight';
  $handler->display->display_options['fields']['field_weight']['label'] = '';
  $handler->display->display_options['fields']['field_weight']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_weight']['settings'] = array(
    'thousand_separator' => ' ',
    'prefix_suffix' => 0,
  );
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  /* Field: videothumbnail */
  $handler->display->display_options['fields']['field_video_thumbnail']['id'] = 'field_video_thumbnail';
  $handler->display->display_options['fields']['field_video_thumbnail']['table'] = 'field_data_field_video_thumbnail';
  $handler->display->display_options['fields']['field_video_thumbnail']['field'] = 'field_video_thumbnail';
  $handler->display->display_options['fields']['field_video_thumbnail']['ui_name'] = 'videothumbnail';
  $handler->display->display_options['fields']['field_video_thumbnail']['label'] = '';
  $handler->display->display_options['fields']['field_video_thumbnail']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_video_thumbnail']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_video_thumbnail']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Sort criterion: Field: weight (field_weight) */
  $handler->display->display_options['sorts']['field_weight_value']['id'] = 'field_weight_value';
  $handler->display->display_options['sorts']['field_weight_value']['table'] = 'field_data_field_weight';
  $handler->display->display_options['sorts']['field_weight_value']['field'] = 'field_weight_value';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'how_to_videos' => 'how_to_videos',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'json');
  $handler->display->display_options['path'] = 'json/howtovideos';
  $export['how_to_videos'] = $view;

  return $export;
}
