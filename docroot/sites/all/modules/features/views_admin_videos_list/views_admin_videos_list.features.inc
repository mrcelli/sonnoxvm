<?php
/**
 * @file
 * views_admin_videos_list.features.inc
 */

/**
 * Implements hook_views_api().
 */
function views_admin_videos_list_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
