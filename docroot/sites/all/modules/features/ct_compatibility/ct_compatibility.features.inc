<?php
/**
 * @file
 * ct_compatibility.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ct_compatibility_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function ct_compatibility_node_info() {
  $items = array(
    'compatibility' => array(
      'name' => t('compatibility'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
