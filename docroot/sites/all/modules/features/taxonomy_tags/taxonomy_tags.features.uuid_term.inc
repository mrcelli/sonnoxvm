<?php
/**
 * @file
 * taxonomy_tags.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function taxonomy_tags_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Limiter',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '032accf4-2c38-4934-a348-dc84b0755b6b',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Electric',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '03325695-c8f8-4793-99a0-3653ed2eac00',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Senheisser',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '034cb516-c2a2-4fed-9225-fd3b93d654a1',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'attacks',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '04afeb28-e3fc-417e-9ed6-f7cf5a702287',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Tools',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '05ba1425-1bf9-4e2f-a0a1-331d7a016e3f',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'De-esser',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '05d666a3-7e53-433e-bf34-e1fe2c25eb09',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Real World Studios',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '06c69a26-8616-4ea6-8fc5-1376a753d46d',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'guitar',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '096a137c-7030-49c2-bc62-17f5c12a6a2a',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Tradeshows',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '0c3e6eaf-0bfe-45e5-b014-4a3966a17ccb',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(
      'und' => array(
        0 => array(
          'value' => 1,
        ),
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Back To Front (Musical Album)',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '0db5856c-116e-4000-a0d9-13c2165d6897',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Toontrack',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '0e883ecc-1e9c-45f5-843e-16d8c1520140',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'transients',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '0f42def6-da46-4018-9ca1-62031fe157c6',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Claudine Ohayon',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '117bd5a5-5c84-4781-8d93-be7174845156',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'EQ',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '13cad5ed-c800-4079-af7d-373975948e51',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Pro-Codec',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '1603d37a-6e78-49a4-b332-6300995afcf5',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Producer',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '173011cc-6b63-4000-af79-e272655db16b',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Compressor',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '19d8bf19-8057-4be9-b582-8cf7f3cd6d88',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Plugin',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '1b32e2f7-3be8-4a61-bcca-f758b5e74a16',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'DJ',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '1dd853b5-9eea-4041-b8b8-fdd8c901855f',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Michael',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '2292fc51-1c90-40dd-bbb1-fc036f3e7aaa',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'DAW',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '2403f465-d9a3-44b5-9439-fb5c3ea77661',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Mick Guzauski',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '26853bad-9215-4710-b02b-34b8bd85065e',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Inflator',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '270010f2-1d10-41b3-88cb-9cc10907216d',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'software',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '2e209885-de0a-43b9-921f-86bd645b1c9e',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'encode',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '315c91e6-d890-4d31-8f5c-ef848e85559f',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'DeNoise',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '32fb3793-3121-4207-a84e-deb596e0178b',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Pro Tools 11',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '359bd084-d4aa-4c44-a363-ce93dc7ba1e1',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Drums',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '3a64dd7a-dcaf-42ed-a3a2-35e96364e7eb',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'TransMod',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '3b21b3fb-123e-4ac4-80bd-aab657fa094b',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Mixing Engineer (Profession)',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '3b2a7538-8628-40f0-ae74-1d996ca3f3d4',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'DeNoising',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '3cbd3e94-2bff-4526-b8d7-1605cf980247',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Omar Hakim (Musical Artist)',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '3f34ee40-8ef2-473d-b369-f7bd9ac874c7',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'oxford',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '4643aa23-faae-437b-8c8c-b6f96faaee89',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'mix',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '48a8125c-476f-4155-91fb-23233e008661',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'mastering',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '4aa3c5a9-4fd4-4a86-a1ff-4f4aa208410c',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Expander',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '4b853bac-051f-407b-9891-fd35065ad3b2',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Paul McCa...',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '50acb0a1-5e44-44c8-81f7-78d4a92c2069',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Fab Dupont',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '531eb653-5cc5-4fe4-88bd-fb8536cda5d6',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Rich',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '5578d072-56ff-426b-b82a-5b3900bbff7c',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Oxford Plug-ins',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '55d70791-c734-4070-9321-b8862c96ae05',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'SuprEsser',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '5636c033-6007-441f-8959-458d70afd71d',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Transient',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '58f8380c-159b-4b70-aa30-c1c2f564b3d3',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'P...',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '59bbd8e6-cf4f-4cde-9d7b-3744d2440467',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Side-Chain',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '5bdcd29a-8fbc-4cab-af4f-e9726822a4e5',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Parallel Compression',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '5d39e91e-9a97-4a6d-860c-0f8103e4e8e4',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Peter Gabriel (Musical Artist)',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '5e098b9b-9f0f-4b49-9222-a5c61eced0c8',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Mastered for iTunes',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '5fc48d34-bda1-4fd8-96e6-3341dd1ce452',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'digital',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '639b6ca4-6512-46d3-8173-ab75bf901c8a',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Codec',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '65fa3274-f787-4777-9f36-58fc5c2948cc',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Studios',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '663d408c-8f64-4461-8620-74422fd5da54',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Mixing',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '68341e05-ee56-4bcb-a2c3-90517e273b16',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Pro Tools (Software)',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '69b447c3-dfd3-4f5d-8169-54d868a60828',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'ProTools',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '6c8e67c3-15b7-4d00-b2b5-172ad50820de',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Dynamic EQ',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '6da10ea6-fe1a-4808-8632-09f7adebd3fe',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'AAX',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '6eba3062-c91a-4933-ac00-477f572ec5fd',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'desser',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '70507c07-fe35-42b9-9232-5b622b82db81',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Compression',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '7111a852-0094-43b8-a523-d3d2432d88d2',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'HD',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '7153c887-2842-45be-8bd5-cec02726797a',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Broadcast',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '7ac3c732-5954-4e48-ac41-1dcd0aa535e5',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Reverb',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '7b2a8532-6553-4990-a91b-e3a0343fb28b',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'plugins',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '7b49ac47-48aa-4b9e-80b3-da51d6e7f9a8',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'AU',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '7b5da7e7-f847-425b-9265-0c4a3a6a85a4',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Lady',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '7fa23fcc-514f-400b-81ec-d3756486d16d',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Dance Music',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '83fe2618-8c5c-4a3f-9120-13b2dafa5224',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Guitars',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '85ad025f-a9d2-4dad-ae6d-ec9e6c571dfe',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Cuba...',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '882a61db-6f20-4f35-ae89-ef8370f918ee',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => '...',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '8ad8ab1f-3fe1-4e63-8614-3fbff60669ec',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Oxford Dynamics',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '8b97990c-a9a6-4d79-99a8-f85923840ee5',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Tozzoli',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '8c436b74-2f78-4a3d-8e62-14d83a70e712',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'www',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '8c90054f-5bda-4bf5-873c-7b29828eca01',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Masterdisk',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '8f74530f-b0a1-4160-8901-ab0aa4b7b631',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'pro tools',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '92e0a5cf-a5e4-4c97-8c19-c7d3ad9bca8d',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'shredding',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '9307fd0d-6f4f-450e-9975-52fc57b918f3',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Fraunhofer',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '943715bb-343f-4dc1-9f33-9e4b498c29f6',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Recording',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '9765acc7-fbc0-4336-9b95-7bfee8160956',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'interviews',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '97a5a4ba-76e4-43be-b8e5-379f86075d67',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(
      'und' => array(
        0 => array(
          'value' => 1,
        ),
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Drummer',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '97f4ec0d-dba7-4621-bac1-353fb03427d8',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Restore',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '98354b1a-f801-4d12-a2b1-89ca28dd03d2',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Gear',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '989d916f-584f-4ac0-814c-adb06231586e',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Units',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '99205d1b-79c2-4c2f-ad9f-0de2ccd433b1',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'voice-over',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '9a692fc9-ff48-4088-8ee1-69157fd80670',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'MP3',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '9bcb2335-e793-42b8-9593-4c6d6ede0465',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Restoration',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '9d8f453b-47d1-4eec-8264-a50df9f5eb78',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'RTAS',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '9fb2486f-2725-4084-9653-d765f8a99aa4',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'PowerCore',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'a21812f9-ebcd-4c62-92ea-834e12342a91',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'tutorials',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'a4d2a48b-317d-42db-aa29-15b744ab64fa',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(
      'und' => array(
        0 => array(
          'value' => 1,
        ),
      ),
    ),
  );
  $terms[] = array(
    'name' => 'sonnox',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'ab566087-f852-481d-8de3-e76676e231a4',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'plug-in',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'ab81125a-c776-4763-852a-a2a6bc808685',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Oxford Plugins',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'ac4a20e1-adab-4183-a6ab-eb22d060a8f8',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'TDM',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'af620654-5d46-4f83-8e8b-3c1f09e9673f',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'StoneBridge',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'b0c4fbf1-f32f-4e4b-b2b2-7db402ad2fe8',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'equaliser',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'b138949d-de2a-48d4-9581-8972f8ddaf4e',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Dynamic',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'b486de84-9fae-406a-ab40-fbc7e7848598',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Compresser',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'b4a53c57-3010-4678-b4a3-cf59cdaedb67',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Voice Actor (Film Job)',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'b552b0e6-72de-4fe2-974f-a99db5bb3f8e',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'ambience',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'b6973186-1bd3-4f83-92b2-a01943052103',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Cubase...',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'b77ba602-44cc-4c0d-9c37-18557ddbfc53',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Daft Punk (Musical Group)',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'b7925fb6-6149-4c57-9db8-fc03c12915d6',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => '',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'b991d7a4-189f-4fae-a39e-3fb39f73ac89',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Pro',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'bac280bc-fcb2-49a6-8bfa-31e5e2f0dabb',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Tony Maserati',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'bb79c755-9cec-4e40-b64c-edd4ed2bcc24',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Core',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'bf49726b-1e31-4d70-b757-24e8cc85d68f',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Logic',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'c3f4a033-9653-46f5-a3bb-cf65f02a339b',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Universal Mastering',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'c435fe7b-0a37-423d-b26e-27fcbe4bd092',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Bass',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'c6261e55-7a2c-4fa8-84b8-f9a8adaea667',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Dynamics',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'c930e2c6-b40b-4643-a3da-8ae5adbeb86c',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Encoding',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'c9b1910f-845f-4f90-a8c1-5322f9b69616',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'engineer',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'cb306ef6-8094-4301-902b-f9e6f3c7229f',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Universal',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'cb42ca71-b452-4762-a8b2-1f9e9f0f658e',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Brauer',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'ccaf5da1-db9d-40bb-85b6-b49a17c31853',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Music',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'cdd446dd-c791-4c55-be5a-ee16457e6290',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'QuickTips',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'd06acfe7-fc3d-4d08-b14f-d88ef4742494',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(
      'und' => array(
        0 => array(
          'value' => 1,
        ),
      ),
    ),
  );
  $terms[] = array(
    'name' => 'AAC',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'd181a0e7-809e-4591-9309-fb58c99df915',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'VST',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'd6275bcf-3317-4442-9a7b-767f053bf4c4',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Audio Mixing (Film Job)',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'd6b0ec2e-c71e-4431-9993-7deb2995c4ca',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Gate',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'd8d75e33-fe3e-4ea7-917e-19d936379463',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'GML',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'daa4a94f-1a1d-421a-8e14-f43e6ccf601b',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Power',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'dc9232aa-5768-4ab3-a06b-1877717a5c29',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Oxford Reverb',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'df4a81dc-02f8-4145-aa31-6180687c847f',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Cubase',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'dfb9a7ac-1ba9-462f-9848-7c193d5d327d',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Deesser',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'e03163ab-89ed-4a94-a73e-6d3481629829',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Drum Compression',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'e3173ea6-11cb-4e1e-99fe-afd4c9a28c41',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'decode',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'e77d153b-a790-49ce-9aa2-1de473bd281a',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'DeNoiser',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'e88a5908-8263-4461-9ac6-bce04a6c74c0',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'plug-ins',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'ea2c0e72-cb6c-4e00-9cfb-0947b5043112',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'So...',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'ed93d174-6c6c-4899-a63c-5f35ee8d44dc',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'equalizer',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'edbc6bf4-ffc4-44ef-a052-1b02913a0709',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Studio',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'f7253649-d465-4245-b739-7b0678031192',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Coldplay',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'f9c61c75-465b-46a7-8af1-2387a02371cf',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Modulator',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'f9c80bfc-5482-4303-84e0-e2451bd835f6',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'audio',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'faf6703b-fcc0-4bce-b08b-96ee5840b282',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'percussion',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'fc6a2bbf-c971-4283-8d44-ea5bdc31f0bf',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Live Sound',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'fe2aa90b-ab65-4aeb-b197-542258e391b0',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(
      'und' => array(
        0 => array(
          'value' => 1,
        ),
      ),
    ),
  );
  return $terms;
}
