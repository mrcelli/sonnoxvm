<?php
/**
 * @file
 * learn_videos_json_views.features.inc
 */

/**
 * Implements hook_views_api().
 */
function learn_videos_json_views_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
