<?php
/**
 * @file
 * ct_format.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ct_format_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function ct_format_node_info() {
  $items = array(
    'format' => array(
      'name' => t('format'),
      'base' => 'node_content',
      'description' => t('audio format'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
