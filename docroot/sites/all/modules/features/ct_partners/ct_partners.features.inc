<?php
/**
 * @file
 * ct_partners.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ct_partners_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function ct_partners_node_info() {
  $items = array(
    'partners' => array(
      'name' => t('partners'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
