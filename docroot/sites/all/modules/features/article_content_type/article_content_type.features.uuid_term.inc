<?php
/**
 * @file
 * article_content_type.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function article_content_type_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'bundles',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '16134410-60fb-4f34-97a7-c79689fdda3e',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'editor',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '246fcbb9-6619-4ee0-a7f5-85f3678c3782',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Artists',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 1,
    'uuid' => '2cece1fa-b2da-4332-9cd6-e7c13450e820',
    'vocabulary_machine_name' => 'category',
    'field_category_icon' => array(
      'und' => array(
        0 => array(
          'bundle' => 'fontawesome',
          'icon' => 'circle',
        ),
      ),
    ),
    'field_sonnox_path' => array(),
  );
  $terms[] = array(
    'name' => 'Bundles',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 6,
    'uuid' => '4a962500-0549-45c4-8c1e-697f4a58257e',
    'vocabulary_machine_name' => 'category',
    'field_category_icon' => array(),
    'field_sonnox_path' => array(),
  );
  $terms[] = array(
    'name' => 'elite',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '89db45ad-70de-4a16-a94a-5263dfbc9c97',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Tips',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 3,
    'uuid' => '8d0d2472-2e84-4ce1-a352-b25edf6ec106',
    'vocabulary_machine_name' => 'category',
    'field_category_icon' => array(),
    'field_sonnox_path' => array(),
  );
  $terms[] = array(
    'name' => 'artists',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '9a78916e-5a12-45ba-8d38-a108fd3966dc',
    'vocabulary_machine_name' => 'tags',
    'field_video_page_filter' => array(),
  );
  $terms[] = array(
    'name' => 'Spotlight',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'a9294c70-f948-4d5f-9716-fca23f7a85ff',
    'vocabulary_machine_name' => 'category',
    'field_category_icon' => array(),
    'field_sonnox_path' => array(),
  );
  $terms[] = array(
    'name' => 'Video',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 2,
    'uuid' => 'b55c3e9e-51f7-4e1f-9caa-60d7500185bc',
    'vocabulary_machine_name' => 'category',
    'field_category_icon' => array(),
    'field_sonnox_path' => array(),
  );
  $terms[] = array(
    'name' => 'Plug-in',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 5,
    'uuid' => 'bb745a46-891f-4754-a897-95875f3ef33d',
    'vocabulary_machine_name' => 'category',
    'field_category_icon' => array(),
    'field_sonnox_path' => array(),
  );
  $terms[] = array(
    'name' => 'News',
    'description' => '<p>category for news</p>
',
    'format' => 'filtered_html',
    'weight' => 4,
    'uuid' => 'dc3de5fc-158c-4868-8160-f3de6a1b3801',
    'vocabulary_machine_name' => 'category',
    'field_category_icon' => array(
      'und' => array(
        0 => array(
          'bundle' => 'fontawesome',
          'icon' => 'bullhorn',
        ),
      ),
    ),
    'field_sonnox_path' => array(),
  );
  $terms[] = array(
    'name' => 'Reviews',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'e6de3be4-1211-4f08-bca3-19903b13c38f',
    'vocabulary_machine_name' => 'category',
    'field_category_icon' => array(),
    'field_sonnox_path' => array(),
  );
  return $terms;
}
