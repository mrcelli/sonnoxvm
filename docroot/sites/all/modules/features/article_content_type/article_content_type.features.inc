<?php
/**
 * @file
 * article_content_type.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function article_content_type_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function article_content_type_node_info() {
  $items = array(
    'article' => array(
      'name' => t('Article'),
      'base' => 'node_content',
      'description' => t('Use <em>articles</em> for time-sensitive content like news, press releases or blog posts.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'article_image' => array(
      'name' => t('article_image'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'article_soundcloud' => array(
      'name' => t('article_soundcloud'),
      'base' => 'node_content',
      'description' => t('soundcloud track'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'article_text' => array(
      'name' => t('article_text'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'parallex' => array(
      'name' => t('parallex'),
      'base' => 'node_content',
      'description' => t('parallex content type'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
