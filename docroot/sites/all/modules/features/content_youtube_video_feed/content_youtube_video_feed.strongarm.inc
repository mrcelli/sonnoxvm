<?php
/**
 * @file
 * content_youtube_video_feed.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function content_youtube_video_feed_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_youtube_video_importer';
  $strongarm->value = 0;
  $export['comment_anonymous_youtube_video_importer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_youtube_video_importer';
  $strongarm->value = 1;
  $export['comment_default_mode_youtube_video_importer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_youtube_video_importer';
  $strongarm->value = '50';
  $export['comment_default_per_page_youtube_video_importer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_youtube_video_importer';
  $strongarm->value = 1;
  $export['comment_form_location_youtube_video_importer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_youtube_video_importer';
  $strongarm->value = '1';
  $export['comment_preview_youtube_video_importer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_youtube_video_importer';
  $strongarm->value = 1;
  $export['comment_subject_field_youtube_video_importer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_youtube_video_importer';
  $strongarm->value = '0';
  $export['comment_youtube_video_importer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_youtube_video_importer';
  $strongarm->value = array();
  $export['menu_options_youtube_video_importer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_youtube_video_importer';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_youtube_video_importer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_youtube_video_importer';
  $strongarm->value = array();
  $export['node_options_youtube_video_importer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_youtube_video_importer';
  $strongarm->value = '1';
  $export['node_preview_youtube_video_importer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_youtube_video_importer';
  $strongarm->value = 0;
  $export['node_submitted_youtube_video_importer'] = $strongarm;

  return $export;
}
