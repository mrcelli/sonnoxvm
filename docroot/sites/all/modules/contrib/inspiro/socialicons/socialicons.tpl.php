<ul class="socialLinks">
  <?php if($settings['twitter']): ?>
  <li><a href="<?php print $settings['twitter'];?>" title="Twitter"><i class="icon-twitter"></i></a></li>
  <?php endif;?>
  <?php if($settings['facebook']): ?>
  <li><a href="<?php print $settings['facebook'];?>" title="Facebook"><i class="icon-facebook"></i></a></li>
  <?php endif;?>
  <?php if($settings['googleplus']): ?>
  <li><a href="<?php print $settings['googleplus'];?>" title="Google Plus"><i class="icon-google-plus"></i></a></li>
  <?php endif;?>
  <?php if($settings['dribbble']): ?>
  <li><a href="<?php print $settings['dribbble'];?>" title="Dribbble"><i class="icon-dribbble"></i></a></li>
  <?php endif;?>
  <?php if($settings['linkedin']): ?>
  <li><a href="<?php print $settings['linkedin'];?>" title="LinkedIn"><i class="icon-linkedin"></i></a></li>
  <?php endif;?>
  <?php if($settings['flickr']): ?>
  <li><a href="<?php print $settings['flickr'];?>" title="Flickr"><i class="icon-flickr"></i></a></li>
  <?php endif;?>
  <?php if($settings['pinterest']): ?>
  <li><a href="<?php print $settings['pinterest'];?>" title="Pinterest"><i class="icon-pinterest"></i></a></li>
  <?php endif;?>
  <?php if($settings['vimeo']): ?>
  <li><a href="<?php print $settings['vimeo'];?>" title="Vimeo"><i class="icon-vimeo"></i></a></li>
  <?php endif;?>
  <?php if($settings['youtube']): ?>
  <li><a href="<?php print $settings['youtube'];?>" title="Youtube"><i class="icon-youtube"></i></a></li>
  <?php endif;?>
  <?php if($settings['rss']): ?>
  <li><a href="<?php print $settings['rss'];?>" title="RSS"><i class="icon-feed"></i></a></li>
  <?php endif;?>
</ul>
