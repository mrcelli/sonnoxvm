/**
 * Implement jquery.responsivelazyloader.
 *
 * @link http://jetmartin.github.io/responsive-lazy-loader
 */

jQuery(function($) {
  /* If responsivelazyloader is enabled and this path is not excluded. */
  if(Drupal.settings.responsivelazyloader_enabled != undefined && Drupal.settings.responsivelazyloader_enabled == true){
    /* Override defaults settings. */
    for(key in Drupal.settings.responsivelazyloader){
      $.fn.responsivelazyloader.defaults[key] = Drupal.settings.responsivelazyloader[key];
    }
    /* Lazy Loader init. */
    $("img[data-src]").responsivelazyloader();
    /* Lazyloader init after AjaxStop event. */
    $(document).ajaxStop(function(){
      $("img[data-src]").responsivelazyloader();
    });
  }
});
