<?php
/**
 * @file
 * Admin pages for ResponsiveLazyLoader module.
 */

/**
 * Admin Form: UI.
 */
function responsivelazyloader_admin($form, &$form_state, $op = NULL) {
  global $_responsivelazyloader_config, $conf;
  $settings = $_responsivelazyloader_config;

  $form = array();

  $form['responsivelazyloader'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Lazyloader Configuration'),
    '#collapsible'   => FALSE,
    '#collapsed'     => FALSE,
    '#description'   => t('Setup the responsive lazyloader configuration.'),
  );
  // Check image allow insecure derivatives.
  if (!empty($conf['image_allow_insecure_derivatives'])) {
    // Check plugins.
    if (module_exists('libraries')) {
      $path = libraries_get_path('responsivelazyloader');
      if (!file_exists($path . '/jquery.responsivelazyloader.js') && !file_exists($path . '/jquery.responsivelazyloader.min.js')) {
        drupal_set_message(t('jQuery responsivelazyloader must be installed in order to use the responsivelazyloader. Please go to !page for instructions.', array('!page' => filter_xss(l(t('Status Report'), 'admin/reports/status')))), 'warning', FALSE);
      }
      else {
        // Basics settings.
        $form['responsivelazyloader']['enabled'] = array(
          '#type'          => 'checkbox',
          '#title'         => t('Enabled'),
          '#default_value' => $settings['enabled'],
          '#description'   => t('Enable/Disable Lazyloader'),
        );
        $form['responsivelazyloader']['distance'] = array(
          '#type'          => 'textfield',
          '#title'         => t('Distance'),
          '#default_value' => $settings['distance'],
          '#size'          => 6,
          '#maxlength'     => 6,
          '#description'   => t('The distance (in pixels) from the image to the browser window before it triggers the loading of the image.'),
        );
        $form['responsivelazyloader']['exclude'] = array(
          '#type'          => 'checkbox',
          '#title'         => t('Exclude Pages'),
          '#default_value' => $settings['exclude'],
          '#description'   => t('Allows you to exclude the following pages of lazyloading instead of including pages. If you check "exclude" without adding pages, you will disable the lazyloading.'),
        );
        $form['responsivelazyloader']['pages'] = array(
          '#type'          => 'textarea',
          '#title'         => t('Pages'),
          '#default_value' => $settings['pages'],
          '#description'   => t('List the page paths to be included/excluded from lazyloading. Set one page per line as "node/1" or "admin/*".'),
        );
        // Advanced settings.
        $form['responsivelazyloader']['advance'] = array(
          '#type'          => 'fieldset',
          '#title'         => t('Advanced settings'),
          '#collapsible'   => TRUE,
          '#collapsed'     => TRUE,
          '#description'   => t('Some advanced settings.'),
        );
        $form['responsivelazyloader']['advance']['token'] = array(
          '#type'          => 'textfield',
          '#title'         => t('Token'),
          '#default_value' => $settings['token'],
          '#description'   => t('Token use for responsive urls. Will be replaced by image styles names.'),
        );
        // Sample debug code.
        $code = '$(this).next().remove(); $(this).after("<p>' . t('Current mediaquerry :') . ' "+$(this).data("current-mediaq")+"<br />' . t('Current display :') . ' "+$(this).data("display")+"</p>");';
        $form['responsivelazyloader']['advance']['onImageShow'] = array(
          '#type'          => 'textarea',
          '#title'         => t('onImageShow'),
          '#default_value' => (!empty($settings['onImageShow'])) ? $settings['onImageShow'] : NULL,
          '#description'   => t('Callback when an image has been lazy loaded. Most useful callback.<br />For debugging you can copy/paste the following code :<br />@code', array('@code' => $code)),
        );
        $form['responsivelazyloader']['advance']['onImageError'] = array(
          '#type'          => 'textarea',
          '#title'         => t('onImageError'),
          '#default_value' => (!empty($settings['onImageError'])) ? $settings['onImageError'] : NULL,
          '#description'   => t('Callback when an image could not be lazy loaded. Could be useful for Track events to be able to find the errors.'),
        );
        $form['responsivelazyloader']['advance']['onAllImagesLoad'] = array(
          '#type'          => 'textarea',
          '#title'         => t('onAllImagesLoad'),
          '#default_value' => (!empty($settings['onAllImagesLoad'])) ? $settings['onAllImagesLoad'] : NULL,
          '#description'   => t('Callback when all the images of the set are loaded. Could be useful for custom Track events.'),
        );
        // Help.
        $form['responsivelazyloader']['help'] = array(
          '#type'          => 'fieldset',
          '#title'         => t('help'),
          '#collapsible'   => TRUE,
          '#collapsed'     => TRUE,
        );
        $form['responsivelazyloader']['help']['markup'] = array(
          '#type' => 'markup',
          '#markup' => t('There are some specific case of use about Responsive lazyloader:'),
        );
        $form['responsivelazyloader']['help']['list'] = array(
          '#theme' => 'item_list',
          '#items' => array(t('If you use some breakpoints without styles, the images will be lazyloaded but be shown in the default style and not the style according to the current breakpoint & display.'),
            t('If you use images in your RTE, you need to use a module such as "!asset" to be able to lazyload your images.',
              array(
                '!asset'  => l(t('asset'), 'http://drupal.org/project/asset'),
              )
            ),
            t('If you use some Drupal AJAX features, you need to use "!jquery_update" and update to >= 1.7 release.',
              array(
                '!jquery_update'  => l(t('jQuery update'), 'http://drupal.org/project/jquery_update'),
              )
            ),
          ),
        );
        // Submit button.
        $form['save']      = array(
          '#type' => 'submit',
          '#value' => t('Save'),
        );
      }
    }
  }
  else {
    $form['responsivelazyloader']['markup'] = array(
      '#type'          => 'markup',
      '#markup'        => t(
        "You must allow images insecure derivatives in your Drupal config files to use the current release of Responsive Lazy Loader.<br />If you understand the security risk and agree with the deal, add the following code in you'r settings.php file.<br />More information on !link.<br />%code",
        array(
          '!link' => l(t('Drupal 7.20 release notes'), 'https://drupal.org/drupal-7.20-release-notes'),
          '%code' => '$conf[\'image_allow_insecure_derivatives\'] = TRUE;',
        )
      ),
    );
  }

  return $form;
}

/**
 * Admin Form : Submit.
 *
 * The submited values will override default config variables.
 */
function responsivelazyloader_admin_submit($form, $form_state) {
  global $_responsivelazyloader_config;
  $settings = $_responsivelazyloader_config;

  $settings['enabled']  = $form_state['values']['enabled'];
  $settings['distance'] = $form_state['values']['distance'];
  $settings['token']    = $form_state['values']['token'];
  $settings['exclude']  = $form_state['values']['exclude'];
  $settings['pages']    = $form_state['values']['pages'];
  if (!empty($form_state['values']['onImageShow'])) {
    $settings['onImageShow'] = $form_state['values']['onImageShow'];
  }
  else {
    unset($settings['onImageShow']);
  }
  if (!empty($form_state['values']['onImageError'])) {
    $settings['onImageError'] = $form_state['values']['onImageError'];
  }
  else {
    unset($settings['onImageError']);
  }
  if (!empty($form_state['values']['onAllImagesLoad'])) {
    $settings['onAllImagesLoad'] = $form_state['values']['onAllImagesLoad'];
  }
  else {
    unset($settings['onAllImagesLoad']);
  }

  variable_set('responsivelazyloader', $settings);
}

/**
 * Admin Form: Breakpoints.
 */
function responsivelazyloader_admin_breakpoints($form, &$form_state, $op = NULL) {
  global $_responsivelazyloader_config;
  $settings = $_responsivelazyloader_config;
  $form = array();
  // Confirm form.
  if (isset($form_state['storage']['confirm'])) {
    return confirm_form(
      $form,
      t('Are you sure you want to delete %breakpoint breakpoint ?', array('%breakpoint' => $form_state['storage']['removeelement'])),
      'admin/config/media/responsivelazyloader/breakpoints',
      t('This action will remove the breakpoint and the associated configurations. This action cannot be undone.'),
      NULL,
      NULL,
      'rll breakpoint remove'
    );
  }
  /*
   * Manage MediaQueries FORM.
   */
  $form['mediaqueries'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Breakpoints'),
    '#collapsible'   => FALSE,
    '#collapsed'     => FALSE,
    '#tree'          => TRUE,
    '#description'   => t('The mediaqueries used as breakpoints. Be careful, the order has the same value as in CSS. The latest one will override the first one if there is a conflict.'),
    '#theme'         => 'responsivelazyloader_admin_breakpoints_display',
  );
  $mq_i = 0;
  foreach ($settings['settings']['mediaqueries'] as $key => $mediaq) {
    $form['mediaqueries'][$mq_i]['weight'] = array(
      '#type'          => 'weight',
      '#delta'         => count($settings['settings']['mediaqueries']) + 2,
      '#default_value' => $mq_i,
    );
    $form['mediaqueries'][$mq_i]['key'] = array(
      '#type'          => 'hidden',
      '#title'         => t('Key'),
      '#default_value' => $key,
      '#description'   => t('MediaQuery unique identifier.'),
    );
    $form['mediaqueries'][$mq_i]['mediaq'] = array(
      '#type'          => 'textarea',
      '#title'         => t('MediaQuery'),
      '#maxlength'     => 800,
      '#default_value' => $mediaq,
      '#description'   => t('The mediaquery. Same syntax as in CSS except the initial "@media". Ex : "(max-width: 767px)"'),
    );
    $form['mediaqueries'][$mq_i]['remove'] = array(
      '#type'          => 'submit',
      '#value'         => t('Remove'),
      '#id'            => 'rll-remove-' . $key,
      '#name'          => 'rll-remove-' . $key,
      '#submit'        => array('responsivelazyloader_admin_breakpoints_remove_submit'),
    );
    $mq_i++;
  }
  // Submit form button.
  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

/**
 * Responsivelazyloader_admin_breakpoints_remove_submit function.
 *
 * Remove breakpoints confirm form.
 */
function responsivelazyloader_admin_breakpoints_remove_submit($form, &$form_state) {
  $remove_element = str_replace('rll-remove-', '', $form_state['clicked_button']['#id']);
  $form_state['storage']['confirm'] = TRUE;
  $form_state['storage']['removeelement'] = $remove_element;
  $form_state['rebuild'] = TRUE;
}

/**
 * Responsivelazyloader_admin_breakpoints_submit function.
 *
 * Update or delete breakpoints.
 */
function responsivelazyloader_admin_breakpoints_submit($form, &$form_state) {
  global $_responsivelazyloader_config;
  $settings = $_responsivelazyloader_config;
  // Remove breakpoint.
  if (isset($form_state['storage']['confirm'])) {
    // Remove breakpoint.
    $remove_element = str_replace('rll-remove-', '', $form_state['storage']['removeelement']);
    unset($settings['settings']['mediaqueries'][$remove_element]);
    // Remove associated display.
    foreach ($settings['settings']['displays'] as $key => $display) {
      if (isset($settings['settings']['displays'][$key]['display'][$remove_element])) {
        unset($settings['settings']['displays'][$key]['display'][$remove_element]);
      }
    }
    variable_set('responsivelazyloader', $settings);
    drupal_set_message(t('The breakpoint %breakpoint has been removed.', array('%breakpoint' => $remove_element)));
  }
  // Edit breakpoint.
  else {
    $breakpoints = array();
    // Sort by weight.
    $mediaqueries = $form_state['values']['mediaqueries'];
    uasort($mediaqueries, 'drupal_sort_weight');
    foreach ($mediaqueries as $mediaquery) {
      if (!empty($mediaquery['key']) && !empty($mediaquery['mediaq'])) {
        $breakpoints[$mediaquery['key']] = $mediaquery['mediaq'];
      }
    }
    $settings['settings']['mediaqueries']  = $breakpoints;
    variable_set('responsivelazyloader', $settings);
  }
}

/**
 * Responsivelazyloader_add_breakpoints.
 */
function responsivelazyloader_add_breakpoint($form, $form_state, $op = NULL) {
  $form = array();
  // Form redirection on success.
  $form['#redirect'] = 'admin/config/media/responsivelazyloader/breakpoints';
  /*
   * Maanage MediaQueries FORM.
   */
  $form['mediaqueries'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('New breakpoint'),
    '#collapsible'   => FALSE,
    '#collapsed'     => FALSE,
    '#tree'          => TRUE,
    '#description'   => t('A new mediaquery used as breakpoints.'),
    '#prefix'        => '<div id="rll-ajax-breakpoints-div">',
    '#suffix'        => '</div>',
  );
  $form['mediaqueries']['key'] = array(
    '#type'          => 'machine_name',
    '#title'         => t('Key'),
    '#default_value' => NULL,
    '#description'   => t('MediaQuery unique identifier.'),
    '#machine_name' => array(
      'exists' => 'responsivelazyloader_mediaqueries_key_exists',
    ),
  );
  $form['mediaqueries']['mediaq'] = array(
    '#type'          => 'textarea',
    '#title'         => t('MediaQuery'),
    '#default_value' => NULL,
    '#description'   => t('The mediaquery. Same syntax as in CSS except the initia "@media". Ex : "(max-width: 767px)"'),
  );
  // Submit button.
  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

/**
 * Responsivelazyloader_add_breakpoint_submit.
 */
function responsivelazyloader_add_breakpoint_submit($form, &$form_state) {
  global $_responsivelazyloader_config;
  $settings = $_responsivelazyloader_config;
  // Add form redirection.
  $form_state['redirect'] = $form['#redirect'];
  // Save datas.
  $settings['settings']['mediaqueries'][$form_state['values']['mediaqueries']['key']] = $form_state['values']['mediaqueries']['mediaq'];
  variable_set('responsivelazyloader', $settings);
  // Add success message.
  drupal_set_message(t('The new breakpoint has been added. You need to configure this breakpoint, otherwise the default image (no styles) will always be used.'));
}

/**
 * Validate the Key is unique.
 */
function responsivelazyloader_mediaqueries_key_exists($element) {
  global $_responsivelazyloader_config;
  $settings = $_responsivelazyloader_config;
  return (array_key_exists($element, $settings['settings']['mediaqueries']));
}

/**
 * Admin Form: displays.
 */
function responsivelazyloader_admin_displays($form, &$form_state, $op = NULL) {
  global $_responsivelazyloader_config;
  $settings = $_responsivelazyloader_config['settings'];
  // Confirm form.
  if (isset($form_state['storage']['confirm'])) {
    return confirm_form(
      $form,
      t('Are you sure you want to delete the %display display ?', array('%display' => $form_state['storage']['removeelement'])),
      'admin/config/media/responsivelazyloader/displays',
      t('This action will remove the display. This action cannot be undone.'),
      NULL,
      NULL,
      'rll display remove'
    );
  }
  $form['#tree'] = TRUE;
  $form['markup'] = array(
    '#type'          => 'markup',
    '#markup'        => t('Manage the displays.'),
  );
  $form['show_weight'] = array(
    '#title'         => t('Show weight.'),
    '#type'          => 'checkbox',
  );
  $form['displayTabs'] = array(
    '#type'          => 'vertical_tabs',
  );
  $i = 0;
  foreach ($settings['displays'] as $key => $value) {
    $form[$key] = responsivelazyloader_admin_set_displays($form, $form_state, $key, $i);
    $i++;
  }
  $form['save']      = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

/**
 * Create Form : displays.
 */
function responsivelazyloader_admin_add_displays($form, &$form_state, $op = NULL) {
  // Form redirection on success.
  $form['#redirect'] = 'admin/config/media/responsivelazyloader/displays';
  $form['markup'] = array(
    '#type'   => 'markup',
    '#markup' => t('Manage the displays.'),
  );
  $form[drupal_html_id(RESPONSIVELAZYLOADER_NEW_DISPLAY)] = responsivelazyloader_admin_set_displays($form, $form_state, RESPONSIVELAZYLOADER_NEW_DISPLAY);
  $form['save'] = array(
    '#type'   => 'submit',
    '#value'  => t('Save'),
  );
  return $form;
}

/**
 * Maanage displays FORM.
 */
function responsivelazyloader_admin_set_displays($form, $form_state, $key = NULL, $i = 0) {
  global $_responsivelazyloader_config;
  $settings = $_responsivelazyloader_config['settings'];
  // Return all the images styles as available options.
  $options = array(t('-Defaults-'));
  $options_no_defaults = array();
  $styles = image_styles();
  foreach ($styles as $style) {
    $options[$style['name']] = $style['label'];
    $options_no_defaults[$style['name']] = $style['label'];
  }
  // Loop on all availables mediaQ.
  $mq = $settings['mediaqueries'];
  $mq['defaults'] = array();

  $form = array(
    '#type'          => 'fieldset',
    '#group'         => ($key == RESPONSIVELAZYLOADER_NEW_DISPLAY) ? NULL : 'displayTabs',
    '#title'         => ($key == RESPONSIVELAZYLOADER_NEW_DISPLAY) ? t('New display') : $key,
    '#collapsible'   => TRUE,
    '#collapsed'     => FALSE,
    '#tree'          => TRUE,
    '#description'   => t('Manage the %display displays.', array('%display' => ($key == RESPONSIVELAZYLOADER_NEW_DISPLAY) ? t('New display') : $key)),
    '#theme'         => 'responsivelazyloader_admin_displays_display',
  );
  $form['key'] = array(
    '#type'          => 'machine_name',
    '#title'         => t('Key'),
    '#default_value' => NULL,
    '#description'   => t('Display unique identifier.'),
    '#machine_name'  => array(
      'exists' => 'responsivelazyloader_displays_key_exists',
    ),
  );
  // If not new display, change the machine_name to hidden field.
  if ($key != RESPONSIVELAZYLOADER_NEW_DISPLAY) {
    $form['key']['#type'] = 'hidden';
    $form['key']['#value'] = $key;
    unset($form['key']['machine_name']);
  }
  // Do not manage parent on DEFAULTS.
  if ($key != 'defaults') {
    $key_as_html_id = ($key == RESPONSIVELAZYLOADER_NEW_DISPLAY) ? str_replace(' ', '-', $key) : $key;
    $form['use_parent'] = array(
      '#type'          => 'checkbox',
      '#title'         => t('Use Parent'),
      '#default_value' => ($key == RESPONSIVELAZYLOADER_NEW_DISPLAY || $key != RESPONSIVELAZYLOADER_NEW_DISPLAY && empty($settings['displays'][$key]['parent'])) ? 0 : 1,
      '#description'   => t('Use parent for advance settings only.<br />The parent display class will be use by default.'),
      '#states'        => array(
        'invisible' => array(
          ':input[name="' . $key_as_html_id . '[use_parent]"]' => array('checked' => TRUE),
        ),
      ),
    );
    $form['parent'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Parent'),
      '#default_value' => ($key == RESPONSIVELAZYLOADER_NEW_DISPLAY || empty($settings['displays'][$key]['parent'])) ? NULL : $settings['displays'][$key]['parent'],
      '#description'   => t('The parent element who has the display class using jQuery notation.<br />Ex : ".region" for the region class. Leave emplty for not use.'),
      '#states'        => array(
        'invisible' => array(
          ':input[name="' . $key_as_html_id . '[use_parent]"]' => array('checked' => FALSE),
        ),
      ),
    );
    $form['gridClass'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Parent display'),
      '#default_value' => ($key == RESPONSIVELAZYLOADER_NEW_DISPLAY) ? NULL : $settings['displays'][$key]['gridClass'],
      '#description'   => t('The parent element displays class name (do not add a "."). <br /> Ex : "sidebar" for the sidebar class.'),
    );
  }
  foreach ($mq as $breakpoint => $mediaq) {
    $tmp_value = (isset($settings['displays'][$key]['display'][$breakpoint])) ? $settings['displays'][$key]['display'][$breakpoint] : RESPONSIVELAZYLOADER_DEFAULTS;
    $form['styles'][$breakpoint] = array(
      '#type'          => 'select',
      '#title'         => t('%media Image style', array('%media' => $breakpoint)),
      '#options'       => ($breakpoint == RESPONSIVELAZYLOADER_DEFAULTS) ? $options_no_defaults : $options,
      '#default_value' => $tmp_value,
      '#description'   => t('The image style used for %display display on %media breakpoint.', array('%display' => $key, '%media' => $breakpoint)),
      '#required'      => ($breakpoint == 'defaults') ? TRUE : FALSE,
    );
  }
  // Weight to manage display order.
  if ($key != RESPONSIVELAZYLOADER_NEW_DISPLAY) {
    $form['weight'] = array(
      '#title'         => t('Weight'),
      '#description'   => t('Use the weight to reorder the displays. As a remember, the first valid display will be used.'),
      '#type'          => ($key != RESPONSIVELAZYLOADER_DEFAULTS) ? 'weight' : 'hidden',
      '#delta'         => count($settings['displays']),
      '#default_value' => ($key != RESPONSIVELAZYLOADER_DEFAULTS) ? $i : (count($settings['displays']) + 2),
      '#states'        => array(
        'invisible' => array(
          ':input[name="show_weight"]' => array('checked' => FALSE),
        ),
      ),
    );
    // Remove button if not default.
    if ($key != RESPONSIVELAZYLOADER_DEFAULTS) {
      $form['remove'] = array(
        '#type'          => 'submit',
        '#value'         => t('Remove'),
        '#id'            => 'rll-remove-' . $key,
        '#name'          => 'rll-remove-' . $key,
        '#submit'        => array('responsivelazyloader_admin_display_remove_submit'),
      );
    }
  }
  return $form;
}
/**
 * Validate the Key is unique.
 */
function responsivelazyloader_displays_key_exists($element) {
  global $_responsivelazyloader_config;
  $settings = $_responsivelazyloader_config;
  return (array_key_exists($element, $settings['settings']['displays']));
}
/**
 * Responsivelazyloader_admin_add_displays_submit.
 */
function responsivelazyloader_admin_add_displays_submit($form, &$form_state) {
  global $_responsivelazyloader_config;
  $settings = $_responsivelazyloader_config;
  // Add form redirection.
  $form_state['redirect'] = $form['#redirect'];
  // Add new values.
  $values = $form_state['values'][drupal_html_id(RESPONSIVELAZYLOADER_NEW_DISPLAY)];
  $new_display = array(
    // NULL if default.
    'gridClass' => (isset($values['gridClass'])) ? $values['gridClass'] : NULL,
    'display' => array(),
  );
  // Set the parent if exist.
  if (!empty($values['parent'])) {
    $new_display['parent'] = $values['parent'];
  }
  foreach ($values['styles'] as $breakpoint => $style) {
    // Save only non empty styles.
    if (!empty($style)) {
      $new_display['display'][$breakpoint] = $style;
    }
  }
  $settings['settings']['displays'] = array_merge(array($values['key'] => $new_display), $settings['settings']['displays']);
  variable_set('responsivelazyloader', $settings);
  drupal_set_message(t('The new Display has been added.'));
}
/**
 * Responsivelazyloader_admin_displays_submit.
 */
function responsivelazyloader_admin_display_remove_submit($form, &$form_state) {
  $remove_element = str_replace('rll-remove-', '', $form_state['clicked_button']['#id']);
  $form_state['storage']['confirm'] = TRUE;
  $form_state['storage']['removeelement'] = $remove_element;
  $form_state['rebuild'] = TRUE;
}
/**
 * Responsivelazyloader_admin_displays_submit.
 */
function responsivelazyloader_admin_displays_submit($form, &$form_state) {
  global $_responsivelazyloader_config;
  $settings = $_responsivelazyloader_config;
  // Remove display.
  if (isset($form_state['storage']['confirm'])) {
    unset($settings['settings']['displays'][$form_state['storage']['removeelement']]);
    variable_set('responsivelazyloader', $settings);
    drupal_set_message(t('The display has been removed.'));
  }
  // Edit displays.
  else {
    $displays = array();
    $sorted = $form_state['values'];
    uasort($sorted, 'drupal_sort_weight');
    foreach ($sorted as $key => $display) {
      if (!empty($display['styles'])) {
        $displays[$key] = array(
          // NULL if default.
          'gridClass' => (isset($display['gridClass'])) ? $display['gridClass'] : NULL,
          'display' => array(),
        );
        // Set the parent if exist.
        if (!empty($display['parent'])) {
          $displays[$key]['parent'] = $display['parent'];
        }
        foreach ($display['styles'] as $breakpoint => $style) {
          // Save only non empty styles.
          if (!empty($style)) {
            $displays[$key]['display'][$breakpoint] = $style;
          }
        }
      }
    }
    $settings['settings']['displays']  = $displays;
    variable_set('responsivelazyloader', $settings);
  }
}

/**
 * Theme_responsivelazyloader_admin_breakpoints_display.
 */
function theme_responsivelazyloader_admin_breakpoints_display($vars) {
  $element = $vars['element'];
  // Rendered Array.
  $output = '';
  // Others rendered eleemnts.
  $output2 = '';
  $require = ' <span class="form-required" title="' . t('This field is required.') . '">*</span>';
  // Table headers.
  $header = array(
    t('Key') . $require,
    t('MediaQuery'),
    '',
    t('Weight'),
  );
  // Create rows.
  $rows = array();
  foreach (element_children($element) as $key) {
    if (isset($element[$key]['key'])) {
      // Add classes.
      $element[$key]['weight']['#attributes']['class'][] = 'elements-weight';
      // Remove unusefull elements for rendering.
      unset($element[$key]['key']['#title']);
      unset($element[$key]['mediaq']['#title']);
      $tpm_key = array('#type' => 'markup', '#markup' => $element[$key]['key']['#value']);
      // Create row.
      $row = array(
        drupal_render($element[$key]['key']) . drupal_render($tpm_key),
        drupal_render($element[$key]['mediaq']),
        drupal_render($element[$key]['remove']),
        drupal_render($element[$key]['weight']),
      );
      // Add the row to rows.
      $rows[] = array(
        'data' => $row,
        'class' => array('draggable'),
      );
    }
    // Render non rows elemnts.
    else {
      $output2 .= drupal_render($element[$key]);
    }
  }
  // Render Draggable Array.
  drupal_add_tabledrag('responsivelazyloader-admin-breakpoints-table', 'order', 'sibling', 'elements-weight', NULL, NULL, TRUE);
  $output .= theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'empty' => t('There are no Breakpoints.'),
    'attributes' => array('id' => 'responsivelazyloader-admin-breakpoints-table'),
  ));
  // Output array first.
  return $output . $output2;
}

/**
 * Theme_responsivelazyloader_admin_displays_display.
 */
function theme_responsivelazyloader_admin_displays_display($vars) {
  // Default variables.
  $element = $vars['element'];
  $table = '';
  $table2 = '';
  $output = '';
  $output2 = '';
  $require = ' <span class="form-required" title="' . t('This field is required.') . '">*</span>';
  // Table headers.
  $header = array(
    t('Parent'),
    t('Parent display'),
  );
  $header2 = array(
    t('Display'),
    t('Image style'),
  );
  $rows = array(); $row = array(); $rows2 = array();
  foreach (element_children($element) as $key) {
    // Create first array rows.
    if ($key == 'use_parent' || $key == 'parent' || $key == 'gridClass') {
      // Remove unusefull elements for rendering.
      if ($key != 'use_parent') {
        unset($element[$key]['#title']);
      }
      if ($key == 'parent' && isset($row['parent'])) {
        $row['parent'] = $row['parent'] . drupal_render($element[$key]);
      }
      else {
        $row[(($key == 'gridClass') ? 'gridClass' : 'parent')] = drupal_render($element[$key]);
      }
    }
    // Create second array rows.
    elseif ($key == 'styles') {
      foreach (element_children($element[$key]) as $style) {
        $row2 = array();
        $row2[] = filter_xss($element[$key][$style]['#title']) . (($style == 'defaults') ? $require : NULL);
        // Remove unusefull elements for rendering.
        unset($element[$key][$style]['#title']);
        $row2[] = drupal_render($element[$key][$style]);
        // Add the row to rows.
        $rows2[] = array(
          'data' => $row2,
        );
      }
    }
    // Render others elements.
    elseif ($key == 'remove' || $key == 'weight') {
      $output2 .= drupal_render($element[$key]);
    }
    // Render others elements.
    else {
      $output .= drupal_render($element[$key]);
    }
  }
  // Add the row to rows.
  $rows[] = array(
    'data' => $row,
  );
  // Render Array.
  $table .= (!empty($row)) ? theme('table', array('header' => $header, 'rows' => $rows)) : NULL;
  $table2 .= theme('table', array('header' => $header2, 'rows' => $rows2));
  // Output array first.
  return $output . $table . $table2 . $output2;
}
