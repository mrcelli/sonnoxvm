Responsivelazyloader
======================

##SUMMARY

This module delays the loading of images in web pages. Images That are not
visible on the screen, are not loaded until the user scrolls to them.
This module also allows you to load different images styles
in order to load the most appropriate image according to the current breakpoint
or display.
In other words, you can load a small image on phones and large ones on desktop
with any additional contribution. This also work for an image in the main page
or in the sidebar.

For a full description of the module, visit the project page:
  https://drupal.org/sandbox/jetmartin/2158105

To submit bug reports, feature suggestions, or to track changes,
visit the issue queue:
  https://drupal.org/project/issues/2158105


##REQUIREMENTS

This module requires the following modules:
* The libraries module (https://www.drupal.org/project/libraries)

* The jQuery responsivelazyloader plugin
  (http://plugins.jquery.com/responsivelazyloader/)

* Allow image insecure derivatives.
  Read more : https://drupal.org/drupal-7.20-release-notes


##INSTALLATION

* Install libraries module.

* Download jQuery responsivelazyloader and place it inside
  “sites/all/libraries/responsivelazyloader”.
  (http://plugins.jquery.com/responsivelazyloader/)

* Allow image insecure derivatives.
  Add the following line to your settings.php file:
$conf['image_allow_insecure_derivatives'] = TRUE;

* Install as usual,
  see https://drupal.org/documentation/install/modules-themes/modules-7
  for further information.

* Set your responsive lazy loader configuration.
  Do not hesitate to create specific images styles before.


##CONFIGURATION

* Configure user permissions in Administration » People » Permissions

* Configure responsive lazyloader in
  Administration » Configuration » Media » Responsive Lazyloader

* Configure responsive lazyloader breakpoints in
  Administration » Configuration » Media » Responsive Lazyloader » Breakpoints

* Configure responsive lazyloader displays in
  Administration » Configuration » Media » Responsive Lazyloader » Displays

If you do not know how to configure see the jQuery plugin documentation
at jetmartin.github.io/responsive-lazy-loader


##BREAKPOINTS CONFIGURATION

If you do not know about breakpoints (mediaqueries), this module
is already setup with basic ones.
For a quick configuration, copy/paste the mediaqueries from the theme
stylesheets.
You can use advanced mediaqueries as orientation or pixel-ratio detection.
You can detect "retina" devices and load different images on hi-resolution
phones/tablets.
e.g. : (max-width: 380px) and (-webkit-min-device-pixel-ratio: 2),
(max-width: 380px) and (min-resolution: 192dpi),
(max-width: 380px) and (min-resolution: 2dppx)


##DISPLAYS CONFIGURATION

The default display will be used if you don't specify any custom displays.
To create a new custom display, click at "New display" on the page :
Administration » Configuration » Media » Responsive Lazyloader » Displays.

* Fill in the key field. This field is an unique identifier.

* Fill the Parent display field by a class name.
  This class should be set in one of the parent elements of the image.

* You can use the Parent field for advanced settings
  (if many elements use the same class as an example).
  You must fill it by a jquery element declaration as "section.sidebar".
  
* On the display page, there is one default display and one display
  for every breakpoint. You can setup one image style for each breakpoint.
  If you do not select a style for a breakpoint, the default one will be used.
  You must select a default image style.


##CONTACT

Current maintainers:
* J-Et. MARTIN (jetmartin) - https://drupal.org/user/1998622

This project has been sponsored by:
* Valtech
  Visit http://www.valtech.fr
