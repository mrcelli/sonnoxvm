/**
 * @file
 * Linkit dialog functions.
 */

(function ($) {

// Create the Linkit namespaces.
Drupal.linkit = Drupal.linkit || {};
Drupal.linkit.currentInstance = Drupal.linkit.currentInstance || {};
Drupal.linkit.dialogHelper = Drupal.linkit.dialogHelper || {};
Drupal.linkit.insertPlugins = Drupal.linkit.insertPlugins || {};

/**
 * Create the modal dialog.
 */
Drupal.linkit.createModal = function() {
  // Create the modal dialog element.
  Drupal.linkit.createModalElement()
  // Create jQuery UI Dialog.
  .dialog(Drupal.linkit.modalOptions())
  // Remove the title bar from the modal.
  .siblings(".ui-dialog-titlebar").hide();

  // Make the modal seem "fixed".
  $(window).bind("scroll resize", function() {
    $('#linkit-modal').dialog('option', 'position', ['center', 50]);
  });

  // Get modal content.
  Drupal.linkit.getDashboard();
};

/**
 * Create and append the modal element.
 */
Drupal.linkit.createModalElement = function() {
  // Create a new div and give it an ID of linkit-modal.
  // This is the dashboard container.
  var linkitModal = $('<div id="linkit-modal"></div>');

  // Create a modal div in the <body>.
  $('body').append(linkitModal);

  return linkitModal;
};

/**
 * Default jQuery dialog options used when creating the Linkit modal.
 */
Drupal.linkit.modalOptions = function() {
  return {
    dialogClass: 'linkit-wrapper',
    modal: true,
    draggable: false,
    resizable: false,
    width: 520,
    position: ['center', 50],
    minHeight: 0,
    zIndex: 210000,
    close: Drupal.linkit.modalClose,
    open: function (event, ui) {
      // Change the overlay style.
      $('.ui-widget-overlay').css({
        opacity: 0.5,
        filter: 'Alpha(Opacity=50)',
        backgroundColor: '#FFFFFF'
      });
    },
    title: 'Linkit'
  };
};

/**
 * Close the Linkit modal.
 */
Drupal.linkit.modalClose = function (e) {
  $('#linkit-modal').dialog('destroy').remove();
  // Make sure the current intstance settings are removed when the modal is
  // closed.
  Drupal.settings.linkit.currentInstance = {};

  // The event object does not have a preventDefault member in
  // Internet Explorer prior to version 9.
  if (e && e.preventDefault) {
    e.preventDefault();
  }
  else {
    return false;
  }
};

/**
 *
 */
Drupal.linkit.getDashboard = function () {
  // Create the AJAX object.
  var ajax_settings = {};
  ajax_settings.event = 'LinkitDashboard';
  ajax_settings.url = Drupal.settings.linkit.dashboardPath +  Drupal.settings.linkit.currentInstance.profile;
  ajax_settings.progress = {
    type: 'throbber',
    message : Drupal.t('Loading Linkit dashboard...')
  };

  Drupal.ajax['linkit-modal'] = new Drupal.ajax('linkit-modal', $('#linkit-modal')[0], ajax_settings);

  // @TODO: Jquery 1.5 accept success setting to be an array of functions.
  // But we have to wait for jquery to get updated in Drupal core.
  // In the meantime we have to override it.
  Drupal.ajax['linkit-modal'].options.success = function (response, status) {
    if (typeof response == 'string') {
      response = $.parseJSON(response);
    }

    // Call the ajax success method.
    Drupal.ajax['linkit-modal'].success(response, status);
    // Run the afterInit function.
    var helper = Drupal.settings.linkit.currentInstance.helper;
    Drupal.linkit.getDialogHelper(helper).afterInit();

    // Set focus in the search field.
    $('#linkit-modal .linkit-search-element').focus();
  };

  // Update the autocomplete url.
  Drupal.settings.linkit.currentInstance.autocompletePathParsed =
    Drupal.settings.linkit.autocompletePath.replace('___profile___',  Drupal.settings.linkit.currentInstance.profile);

  // Trigger the ajax event.
  $('#linkit-modal').trigger('LinkitDashboard');
};

/**
 * Register new dialog helper.
 */
Drupal.linkit.registerDialogHelper = function(name, helper) {
  Drupal.linkit.dialogHelper[name] = helper;
};

/**
 * Get a dialog helper.
 */
Drupal.linkit.getDialogHelper = function(name) {
  return Drupal.linkit.dialogHelper[name];
};

/**
 * Register new insert plugins.
 */
Drupal.linkit.registerInsertPlugin = function(name, plugin) {
  Drupal.linkit.insertPlugins[name] = plugin;
};

/**
 * Get an insert plugin.
 */
Drupal.linkit.getInsertPlugin = function(name) {
  return Drupal.linkit.insertPlugins[name];
};

})(jQuery);
;
/**
 * @file
 * Helper functions for memcache_admin module.
 */

// Global Killswitch
if (Drupal.jsEnabled) {
  $(document).ready(function() {
    $("body").append($("#memcache-devel"));
  });
}
;
(function ($) {

// Written by
// Bram ten Hove, bramth@goalgorilla.com
// Daniel Beeke, daniel@goalgorilla.com

// Explain link in query log
Drupal.behaviors.search_krumo_search = {
  attach: function() {

    // Define krumo root.
    var k = $('.krumo-root:not(".processed")');

    // Check if there is a krumo.
    if ($(k).addClass('processed').length > 0) {
      var form  = '<div class="search-krumo">';
      form     += '  <form id="search-krumo">';
      form     += '    <input class="form-text" type="text" name="search-query" />';
      // If there are more than one krumo's.
      if ($(k).length > 1) {
        form   += '    <select class="form-select" name="search-option">';
        form   += '      <option value="all">search all</option>';
        // For each krumo.
        $(k).each(function(i) {
          i++;
          form += '      <option value="'+ i +'">search krumo #'+ i +'</option>';
        });
        form   += '    </select>';
      }
      form     += '    <input class="form-submit" type="submit" value="submit" name="submit" />';
      form     += '  </form>';
      form     += '</div>';
      form     += '<div class="search-krumo-results"></div>';

      // Insert the form before the first krumo.
      k.eq(0).before(form);
    }

    // On submit execute the following.
    $('form#search-krumo').submit(function() {
      // Remove result and classes from previous query.
      $('.krumo-element.krumo-query-result').removeClass('krumo-query-result');
      $('.krumo-nest').hide().prev().removeClass('krumo-opened');
      $('.search-krumo-results').html('');

      // Get query value and option value as variables.
      var q = $('input[name=search-query]', this).val();
      var o = $('select[name=search-option]', this).val();
      // If the query is not empty, we can proceed.
      if (q) {
        var k;
        if (o && o != 'all') {
          k = $('.messages.status ul li:nth-child('+ o +') .krumo-root');
        }
        else {
          k = $('.krumo-root');
        }
        // Find all elements with the query.
        $('.krumo-element > a:contains('+ q +'), .krumo-element > strong:contains('+ q +')', k).each(function(i) {
          // Add result class.
          $(this).parent().addClass('krumo-query-result');

          // Expand parents until the query result is layed open before the user.
          $(this).parent().parents(".krumo-nest").show().prev().addClass('krumo-opened');
        });
        // Show result overview.
        $('.search-krumo-results').html('Found '+ $('.krumo-element > a:contains('+ q +'), .krumo-element > strong:contains('+ q +')', k).length +' elements');
      }
      else {
        $('.search-krumo-results').html('Empty query');
      }
      return false;
    });

  }
}

})(jQuery);
;
(function ($) {
Drupal.behaviors.search_krumo_trail = {
  attach: function() {

    $('.krumo-element:not(".processed")').addClass('processed').append(Drupal.t('<span class="krumo-get-path"><a href="#">Get path</a></span>'));

    // The function to return the path.
    $('.krumo-get-path').click( function(){
      // Function for getting a path to an element in PHP.
      var pathItems = [];
      var parent = $(this).parents('.krumo-root');
      var krumoIndex = parent.index('.krumo-root');

      // Array which will hold all the pieces of the trail.
      var currentItem = ['Trail', $(this).parent().children('.krumo-name').text()];
      pathItems.push(currentItem);

      // Filling the trail array.
      $(this).parents('.krumo-nest').each(function(i) {
        // Get the element type.
        var elementType = $(this).prev('.krumo-element').children('.krumo-type').text().toString().split(' ');
        // Objects.
        if (elementType[0] == 'Object') {
          var currentItem = ['Object', $(this).prev('.krumo-element').children('.krumo-name').text()];
        }
        // Arrays.
        else if (elementType[0] == 'Array,') {
          var currentItem = ['Array', $(this).prev('.krumo-element').children('.krumo-name').text()];
        }
        pathItems.push(currentItem);
      });

      // The string with the whole trail which will be returned at the end.
      var trail = '';
      // For each item in the trail array we are going to add it to the trail.
      $.each(pathItems, function(i) {
        // Fix the trail for arrays.
        if (pathItems[i +1] && pathItems[i +1][0] == 'Array') {
          // Integers should be returned as integers.
          if (parseInt($(this)[1]) == $(this)[1]) {
            trail = '[' + $(this)[1] + ']' + trail;
          }
          // Replace 'und' by the Drupal constant LANGUAGE_NONE.
          else if ($(this)[1] == 'und') {
            trail = '[LANGUAGE_NONE]' + trail;
          }
          // Else we return the item as a string in the trail.
          else {
            trail = "['" + $(this)[1] + "']" + trail;
          }
        }
        // Fix the trail for objects.
        else if (pathItems[i +1] && pathItems[i +1][0] == 'Object') {
          // Replace 'und' by the Drupal constant LANGUAGE_NONE.
          if ($(this)[1] == 'und') {
            trail = '->{LANGUAGE_NONE}' + trail;
          }
          // Else we add the item to the trail.
          else {
            trail = '->' + $(this)[1] + trail;
          }
        }
        else {
          // Add the variable name if it could be found.
          if (Drupal.settings.searchKrumo !== undefined && Drupal.settings.searchKrumo.variable[krumoIndex] !== undefined) {
            trail = Drupal.settings.searchKrumo.variable[krumoIndex] + trail;
          }
          // Otherwise we return the default variable name.
          else {
            trail = '$var' + trail;
          }
        }
      });

      $(this).addClass('hidden').hide().before('<input id="trail-input" value="' + trail + '" />');

      $('#trail-input').select().blur(function() {
        $(this).remove();
        $('.krumo-get-path.hidden').show();
      });

      return false;
    });
  }
}
})(jQuery);
;
(function ($) {

/**
 * Attaches double-click behavior to toggle full path of Krumo elements.
 */
Drupal.behaviors.devel = {
  attach: function (context, settings) {

    // Add hint to footnote
    $('.krumo-footnote .krumo-call').once().before('<img style="vertical-align: middle;" title="Click to expand. Double-click to show path." src="' + settings.basePath + 'misc/help.png"/>');

    var krumo_name = [];
    var krumo_type = [];

    function krumo_traverse(el) {
      krumo_name.push($(el).html());
      krumo_type.push($(el).siblings('em').html().match(/\w*/)[0]);

      if ($(el).closest('.krumo-nest').length > 0) {
        krumo_traverse($(el).closest('.krumo-nest').prev().find('.krumo-name'));
      }
    }

    $('.krumo-child > div:first-child', context).dblclick(
      function(e) {
        if ($(this).find('> .krumo-php-path').length > 0) {
          // Remove path if shown.
          $(this).find('> .krumo-php-path').remove();
        }
        else {
          // Get elements.
          krumo_traverse($(this).find('> a.krumo-name'));

          // Create path.
          var krumo_path_string = '';
          for (var i = krumo_name.length - 1; i >= 0; --i) {
            // Start element.
            if ((krumo_name.length - 1) == i)
              krumo_path_string += '$' + krumo_name[i];

            if (typeof krumo_name[(i-1)] !== 'undefined') {
              if (krumo_type[i] == 'Array') {
                krumo_path_string += "[";
                if (!/^\d*$/.test(krumo_name[(i-1)]))
                  krumo_path_string += "'";
                krumo_path_string += krumo_name[(i-1)];
                if (!/^\d*$/.test(krumo_name[(i-1)]))
                  krumo_path_string += "'";
                krumo_path_string += "]";
              }
              if (krumo_type[i] == 'Object')
                krumo_path_string += '->' + krumo_name[(i-1)];
            }
          }
          $(this).append('<div class="krumo-php-path" style="font-family: Courier, monospace; font-weight: bold;">' + krumo_path_string + '</div>');

          // Reset arrays.
          krumo_name = [];
          krumo_type = [];
        }
      }
    );
  }
};

})(jQuery);
;
window.CKEDITOR_BASEPATH = '/sites/all/libraries/ckeditor/';;
