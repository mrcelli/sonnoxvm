/**
 * @file
 * Helper functions for memcache_admin module.
 */

// Global Killswitch
if (Drupal.jsEnabled) {
  $(document).ready(function() {
    $("body").append($("#memcache-devel"));
  });
}
;
(function ($) {

// Written by
// Bram ten Hove, bramth@goalgorilla.com
// Daniel Beeke, daniel@goalgorilla.com

// Explain link in query log
Drupal.behaviors.search_krumo_search = {
  attach: function() {

    // Define krumo root.
    var k = $('.krumo-root:not(".processed")');

    // Check if there is a krumo.
    if ($(k).addClass('processed').length > 0) {
      var form  = '<div class="search-krumo">';
      form     += '  <form id="search-krumo">';
      form     += '    <input class="form-text" type="text" name="search-query" />';
      // If there are more than one krumo's.
      if ($(k).length > 1) {
        form   += '    <select class="form-select" name="search-option">';
        form   += '      <option value="all">search all</option>';
        // For each krumo.
        $(k).each(function(i) {
          i++;
          form += '      <option value="'+ i +'">search krumo #'+ i +'</option>';
        });
        form   += '    </select>';
      }
      form     += '    <input class="form-submit" type="submit" value="submit" name="submit" />';
      form     += '  </form>';
      form     += '</div>';
      form     += '<div class="search-krumo-results"></div>';

      // Insert the form before the first krumo.
      k.eq(0).before(form);
    }

    // On submit execute the following.
    $('form#search-krumo').submit(function() {
      // Remove result and classes from previous query.
      $('.krumo-element.krumo-query-result').removeClass('krumo-query-result');
      $('.krumo-nest').hide().prev().removeClass('krumo-opened');
      $('.search-krumo-results').html('');

      // Get query value and option value as variables.
      var q = $('input[name=search-query]', this).val();
      var o = $('select[name=search-option]', this).val();
      // If the query is not empty, we can proceed.
      if (q) {
        var k;
        if (o && o != 'all') {
          k = $('.messages.status ul li:nth-child('+ o +') .krumo-root');
        }
        else {
          k = $('.krumo-root');
        }
        // Find all elements with the query.
        $('.krumo-element > a:contains('+ q +'), .krumo-element > strong:contains('+ q +')', k).each(function(i) {
          // Add result class.
          $(this).parent().addClass('krumo-query-result');

          // Expand parents until the query result is layed open before the user.
          $(this).parent().parents(".krumo-nest").show().prev().addClass('krumo-opened');
        });
        // Show result overview.
        $('.search-krumo-results').html('Found '+ $('.krumo-element > a:contains('+ q +'), .krumo-element > strong:contains('+ q +')', k).length +' elements');
      }
      else {
        $('.search-krumo-results').html('Empty query');
      }
      return false;
    });

  }
}

})(jQuery);
;
(function ($) {
Drupal.behaviors.search_krumo_trail = {
  attach: function() {

    $('.krumo-element:not(".processed")').addClass('processed').append(Drupal.t('<span class="krumo-get-path"><a href="#">Get path</a></span>'));

    // The function to return the path.
    $('.krumo-get-path').click( function(){
      // Function for getting a path to an element in PHP.
      var pathItems = [];
      var parent = $(this).parents('.krumo-root');
      var krumoIndex = parent.index('.krumo-root');

      // Array which will hold all the pieces of the trail.
      var currentItem = ['Trail', $(this).parent().children('.krumo-name').text()];
      pathItems.push(currentItem);

      // Filling the trail array.
      $(this).parents('.krumo-nest').each(function(i) {
        // Get the element type.
        var elementType = $(this).prev('.krumo-element').children('.krumo-type').text().toString().split(' ');
        // Objects.
        if (elementType[0] == 'Object') {
          var currentItem = ['Object', $(this).prev('.krumo-element').children('.krumo-name').text()];
        }
        // Arrays.
        else if (elementType[0] == 'Array,') {
          var currentItem = ['Array', $(this).prev('.krumo-element').children('.krumo-name').text()];
        }
        pathItems.push(currentItem);
      });

      // The string with the whole trail which will be returned at the end.
      var trail = '';
      // For each item in the trail array we are going to add it to the trail.
      $.each(pathItems, function(i) {
        // Fix the trail for arrays.
        if (pathItems[i +1] && pathItems[i +1][0] == 'Array') {
          // Integers should be returned as integers.
          if (parseInt($(this)[1]) == $(this)[1]) {
            trail = '[' + $(this)[1] + ']' + trail;
          }
          // Replace 'und' by the Drupal constant LANGUAGE_NONE.
          else if ($(this)[1] == 'und') {
            trail = '[LANGUAGE_NONE]' + trail;
          }
          // Else we return the item as a string in the trail.
          else {
            trail = "['" + $(this)[1] + "']" + trail;
          }
        }
        // Fix the trail for objects.
        else if (pathItems[i +1] && pathItems[i +1][0] == 'Object') {
          // Replace 'und' by the Drupal constant LANGUAGE_NONE.
          if ($(this)[1] == 'und') {
            trail = '->{LANGUAGE_NONE}' + trail;
          }
          // Else we add the item to the trail.
          else {
            trail = '->' + $(this)[1] + trail;
          }
        }
        else {
          // Add the variable name if it could be found.
          if (Drupal.settings.searchKrumo !== undefined && Drupal.settings.searchKrumo.variable[krumoIndex] !== undefined) {
            trail = Drupal.settings.searchKrumo.variable[krumoIndex] + trail;
          }
          // Otherwise we return the default variable name.
          else {
            trail = '$var' + trail;
          }
        }
      });

      $(this).addClass('hidden').hide().before('<input id="trail-input" value="' + trail + '" />');

      $('#trail-input').select().blur(function() {
        $(this).remove();
        $('.krumo-get-path.hidden').show();
      });

      return false;
    });
  }
}
})(jQuery);
;
(function ($) {

/**
 * Attaches double-click behavior to toggle full path of Krumo elements.
 */
Drupal.behaviors.devel = {
  attach: function (context, settings) {

    // Add hint to footnote
    $('.krumo-footnote .krumo-call').once().before('<img style="vertical-align: middle;" title="Click to expand. Double-click to show path." src="' + settings.basePath + 'misc/help.png"/>');

    var krumo_name = [];
    var krumo_type = [];

    function krumo_traverse(el) {
      krumo_name.push($(el).html());
      krumo_type.push($(el).siblings('em').html().match(/\w*/)[0]);

      if ($(el).closest('.krumo-nest').length > 0) {
        krumo_traverse($(el).closest('.krumo-nest').prev().find('.krumo-name'));
      }
    }

    $('.krumo-child > div:first-child', context).dblclick(
      function(e) {
        if ($(this).find('> .krumo-php-path').length > 0) {
          // Remove path if shown.
          $(this).find('> .krumo-php-path').remove();
        }
        else {
          // Get elements.
          krumo_traverse($(this).find('> a.krumo-name'));

          // Create path.
          var krumo_path_string = '';
          for (var i = krumo_name.length - 1; i >= 0; --i) {
            // Start element.
            if ((krumo_name.length - 1) == i)
              krumo_path_string += '$' + krumo_name[i];

            if (typeof krumo_name[(i-1)] !== 'undefined') {
              if (krumo_type[i] == 'Array') {
                krumo_path_string += "[";
                if (!/^\d*$/.test(krumo_name[(i-1)]))
                  krumo_path_string += "'";
                krumo_path_string += krumo_name[(i-1)];
                if (!/^\d*$/.test(krumo_name[(i-1)]))
                  krumo_path_string += "'";
                krumo_path_string += "]";
              }
              if (krumo_type[i] == 'Object')
                krumo_path_string += '->' + krumo_name[(i-1)];
            }
          }
          $(this).append('<div class="krumo-php-path" style="font-family: Courier, monospace; font-weight: bold;">' + krumo_path_string + '</div>');

          // Reset arrays.
          krumo_name = [];
          krumo_type = [];
        }
      }
    );
  }
};

})(jQuery);
;
/*
Plugin: jQuery Parallax
Version 1.1.3
Author: Ian Lunn
Twitter: @IanLunn
Author URL: http://www.ianlunn.co.uk/
Plugin URL: http://www.ianlunn.co.uk/plugins/jquery-parallax/

Dual licensed under the MIT and GPL licenses:
http://www.opensource.org/licenses/mit-license.php
http://www.gnu.org/licenses/gpl.html
*/

(function( $ ){
	var $window = $(window);
	var windowHeight = $window.height();

	$window.resize(function () {
		windowHeight = $window.height();
	});

	$.fn.parallax = function(xpos, speedFactor, outerHeight) {
		var $this = $(this);
		var getHeight;
		var firstTop;
		var paddingTop = 0;
		
		//get the starting position of each element to have parallax applied to it		
		$this.each(function(){
		    firstTop = $this.offset().top;
		});

		if (outerHeight) {
			getHeight = function(jqo) {
				return jqo.outerHeight(true);
			};
		} else {
			getHeight = function(jqo) {
				return jqo.height();
			};
		}
			
		// setup defaults if arguments aren't specified
		if (arguments.length < 1 || xpos === null) xpos = "50%";
		if (arguments.length < 2 || speedFactor === null) speedFactor = 0.8;
		if (arguments.length < 3 || outerHeight === null) outerHeight = true;
		
		// function to be called whenever the window is scrolled or resized
		function update(){
		  
			var pos = $window.scrollTop();				
            //alert("update");
			$this.each(function(){
				var $element = $(this);
				var top = $element.offset().top;
				var height = getHeight($element);

				// Check if totally above or totally below viewport
				if (top + height < pos || top > pos + windowHeight) {
					return;
				}

				$this.css('backgroundPosition', xpos + " " + Math.round((firstTop - pos) * speedFactor) + "px");
			});
		}		
        function ajaxscroll (){
          //  console.log('ajaxscroll');
              windowHeight = $window.height();
             $this.each(function(){
            firstTop = $this.offset().top;
             });
             if (arguments.length < 1 || xpos === null) xpos = "60%";
        if (arguments.length < 2 || speedFactor === null) speedFactor = 0.8;
        if (arguments.length < 3 || outerHeight === null) outerHeight = true;
        
          
        }
		$window.bind('scroll', update).resize(update);
		$window.bind('ajaxscroll', ajaxscroll).resize(ajaxscroll);
		update();
	};
})(jQuery);
;
